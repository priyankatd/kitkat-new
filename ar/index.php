<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="assets/images/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/slick.css?v1.5"/>
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css?v1.5"/>
	<link rel="stylesheet" type="text/css" href="assets/css/dcsns_wall.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css?v1.5"/>
    <link rel="stylesheet" type="text/css" href="assets/css/common.css?v1.6">
	<style>body{overflow-x:hidden;}.page-content{padding-top:0px;}</style>
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/jquery-ui-1.10.4.js"></script>
    <script src="assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>

    <![endif]-->
</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php 
            include("config.php");
            include("header.php");        
        ?>
    </header>

    <main class="page-content">
			<!-- <div class="breaker-couter animated hidden-sm hidden-xs bounceInDown">
                <div class="breaker-taken">
                    <p>عدد البريكات التي أخذتها هذا العام</p>
                    <h3>1,347,908,760</h3>
                </div>
                <div class="kitkat-consumed">
                    <p>مجموع الواح الكيت كات التي تم استهلاكها</p>
                    <h3>9,160,719,870</h3>
                </div>
                <div class="counter-bg">
                    <img src="assets/images/break-counter.png" class="img-responsive" alt="kitkat">
                </div>
            </div>	 -->
	<!-- Spotlight banner start-->
	<audio class="break-sound" preload="auto"><source src="assets/files/click.mp3"></audio>
       <div id="spotlight-home">
			<div class="spotlight-items mini-moments"></div>
			<div class="spotlight-items senses"></div>
	   </div>
	 <!-- Spotlight banner ends-->
	 
	 <!-- Usual Breaker start-->
       <div id="usual-breaker">
		<div class="clearfix outer-container">
			<div class="section-heading">
                        <h1>البريكرز المعتادين</h1>
			</div>
            <div class="left-scale">
				 <div class="right-arrow"></div>
            </div>
			<div class="overlay"></div>
            <div class="center-lines">
			
				<div class="left-arrow"></div>
				<div class="right-arrow"></div>
				
                <div class="usual-breaker">
					<div class="item">
					  <div class="ub-item-desc"><img src="assets/images/products/senses-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/kitkat-sense-hazelnut/">المزيد من المعلومات</a>
                       <a href="#" class="p-buynow" data-buynowid="5097">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-senses-hazelnut-2-finger.png"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/2-fingers-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/two-finger/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="4506">اشتري الآن</a></div>
                        <img src="assets/images/packshots/kitkat-2-fingers.png"  /></a>
                    </div>
					
					<div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/4-fingers-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/four-finger/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="4509">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-4-fingers.png"/>
                    </div>
					
                    <div class="item">
						 <div class="ub-item-desc"><img src="assets/images/products/4-fingers-dark-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/four-finger-dark/">المزيد من المعلومات</a>
                                            <a href="#" class="p-buynow" data-buynowid="4508">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-4-fingers-dark-chocolate.png"/>
                    </div>
					
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/chunky-caramel-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/chunky-caramel/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="4511">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-chunky-caramel.png"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/chunky-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/chunky/">المزيد من المعلومات</a>
                     <a href="#" class="p-buynow" data-buynowid="4510">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-chunky-milk-chocolate.png"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/chunky-mini-caramel-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/chunky-mini-caramel/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="4513">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-chunky-mini-caramel.png"/>
                    </div>
					
                    <div class="item">
					<div class="ub-item-desc"><img src="assets/images/products/mini-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/chunky-mini/">المزيد من المعلومات</a>
                                            <a href="#" class="p-buynow" data-buynowid="4514">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-chunky-mini-milk-chocolate.png"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/chunky-peanut-butter-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/chunky-peanut-butter/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-chunky-peanut-butter.png"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/mini-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/min-moments/">المزيد من المعلومات</a>
                     <a href="#" class="p-buynow" data-buynowid="5096">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-mini-moments.png" style="margin-right:-21px;"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/choc-milk-chocolate-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/pop-choc-milk-chocolate-bites/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="4516">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-pop-Choc-milk-chocolate-bites-36g.png"/>
                    </div>
					
                    <div class="item">
					 <div class="ub-item-desc"><img src="assets/images/products/choc-milk-chocolate-140-bg-ar.png" class="img-responsive"><a href="product-range/product-detail/pop-choc-milk-chocolate-bites/">المزيد من المعلومات</a>
                      <a href="#" class="p-buynow" data-buynowid="4515">اشتري الآن</a></div>
                      <img src="assets/images/packshots/kitkat-pop-choc-milk-chocolate-bites-140g.png" style="margin-right:-21px;"/>
                    </div>
                   
                </div>
            </div>
            <div class="right-scale">
				 <div class="left-arrow"></div>
            </div>
        </div>
	
	   </div>
	 <!-- Usual Breaker ends-->
	 
	 <!-- kitkat journey start -->
       <div id="kitkat-journey">
			<section class="time-line-all" id="journey">
                <div class="time-line-wrap time-line-bg clearfix">
                    <div class="time-header">
                        <h1><span></span>رحلة كيت كات</h1>
                    </div>
                    <div id="time-line" class="time-line">
                        <div class="time-line-item journey">
                            <div class="clearfix">
                                <p>بدأت رحلة <span>كيت كات</span> في لندن في أواخر القرن السابع عشر، عندما التقى نادي أدبي بمخبز فطائر يملكه شيف حلويات اسمه كريستوفر كاتلينج. وكان يسمى النادي بنادي الكيت كات مستوحاة من اختصار لاسم المالك.</p>
                            </div>
                        </div>
                        <div class="time-line-item year1935">
                            <div>
                                <h2>"نيويورك الهشة" من راونتري</h2>
                                <p>وفي 29 أغسطس تم تصنيع لوح شوكولاتة بأربعة أصابع ويفر في يورك. ومن ثم بدأت تباع ألواح الشوكولاتة في لندن وجنوب شرق إنجلترا باسم "الشوكولاته الهشة" من راونتري.</p>
                            </div>
                        </div>
                        <div class="time-line-item year1937">
                            <div>
                                <h2>"الشوكولاته الهشة" من راونتري</h2>
                                <p>جورج هاريس، مدير التسويق لدى راونتري، غير اسم المنتج الى  "شوكولاتة كيت كات الهشة". و في هذا العام أيضا ظهر أول استخدام لكلمة "استراحة" أو "بريك" في إعلانات كيت كات.</p>
                            </div>
                        </div>
                        <div class="time-line-item year1944">
                            <div>
                                 <h2>الحرب العالمية الثانية</h2>
                                <p>بسبب نقص المكونات بما في ذلك الحليب أثناء الحرب العالمية الثانية ، غيرت شركة راونتري وصفة "شوكولاتة كيت كات الهشة". كما تم تغيير لون االتغليف إلى الأزرق وتمت إزالة الشعار البيضاوي والكلمات "الشوكولاتة الهشة" وبقي الاسم المختصر كيت كات بخط عريض.</p>
                            </div>
                        </div>
                        <div class="time-line-item year1949">
                            <div>
                                <h2>مجد المغلف الأحمر</h2>
                                <p>مجد الغلاف الأحمر</p>
                            </div>
                        </div>
                        <div class="time-line-item year1950">
                            <div>
                                
                            </div>
                        </div>
                        <div class="time-line-item year1958">
                            <h2>شعارالإعلان الكلاسيكي</h2>
                            <div>
                                <p>دونالد جيليز ، موظف في وكالة إعلانات جي دبليو تي بلندن ، يبتدع شعارالإعلان الكلاسيكي: خدلك بريك، خدلك كيت كات</p>
                            </div>
                        </div>
                        <div class="time-line-item year1960">
                            <div>
                                <h2>الشكل الجديد للتغليف</h2>
                                <p>الستينات: ولدت الحزمة متعددة القطع من اصبعي الشوكولاتة. هذا الشكل الجديد من التعبئة والتغليف حرك المبيعات في المتاجر الكبرى الجديدة مع زيادة تخزين الواح الكيت كات من قبل المستهلكين في منازلهم.</p>
                                
                            </div>
                        </div>
                        <div class="time-line-item year1969">
                            <div>
                                <h2>ظهور أول إعلان تلفزيوني مصور بالألوان </h2>
                            </div>
                        </div>
                        <div class="time-line-item year1970">
                            <div>
                                
                            </div>
                        </div>
                        <a  href="https://www.youtube.com/embed/lnQKET83qKg?autoplay=1&amp;autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" class="time-line-video1" tabindex="-1" data-caption="PANDA TV Ad" data-options="width:1280, height:800">
							<div class="time-line-item year1987">
									<div><h2>انتاج الإعلان التلفزيوني الشهير مع الباندا</h2></div>
							</div>
						</a>
                        <div class="time-line-item year1988">
                            <div>
                                <h2>شركة نستله تشتري راونتري</h2>
                            </div>
                        </div>
                        <div class="time-line-item year1999">
                            <div>
                                <h2>اطلاق كيت كات تشنكي </h2>
                                <p>اطلاق كيت كات تشنكي في المملكة المتحدة لتنجح نجاحا باهرا، وانتقل النجاح الى الأسواق النامية الأخرى.</p>
                            </div>
                        </div>
                        <div class="time-line-item year2000">
                            <div>
                                
                            </div>
                        </div>
                        <div class="time-line-item year2005">
                            <div>
                                 <h2>اطلاق شوكولاتة كيت كات بوب شوك</h2>
                            </div>
                        </div>
                        <div class="time-line-item year2006">
                            <div>
                                <h2>إطلاق كيت كات تشنكي بنكهة الفول السوداني</h2>

                            </div>
                        </div>
						
						<a tabindex="-1" id="" href="assets/files/80-year-celebration.mp4" target="_blank" class="time-line-video2" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                        <div class="time-line-item year2015">
                            <div>
                                <h2>ميني مومنتس</h2>
                                <p>كيت كات تطلق سلسلة "ميني مومنتس" وهي مجموعة متنوعة من النكهات المختلفة.</p>
                            </div>
                        </div>
						 </a>
                        <div class="time-line-item year2016">
                            <div>
                                <h2>كيت كات سنسز بالبندق</h2>
                                <p>كيتكات تطلق سنسزالغنية بقطع البندق المقرمش</p>
                            </div>
                        </div>
                    </div>   
                </div>
            </section>
			
	   </div>
	 <!-- kitkat journey ends-->
	 
	 <!-- Social Sphere start -->
       <div id="social-sphere">
			<section id="social" class="kitkat-social">
                <div class="social-wrap">
                    <div class="section-heading social">
                        <h1><img src="assets/images/kitkat-main-logo.png" class="img-responsive hidden-lg hidden-md hidden-sm" alt="Kitkat Social Sphere"><img src="assets/images/kitkat-logo-social.png" class="img-responsive hidden-xs" alt="Kitkat Social Sphere">الدائرة الاحتماعية</h1>
                        <div class="before-after-line">
                            <div class="line-left"></div>
                            <div class="line-right"></div>
                        </div>
                    </div>
                    <div class="social-all">
                        <div id="social-stream" class="dcsns">

                            <div class="dcsns-content">
                                <ul class="stream">
                                    <li class="dcsns-li dcsns-facebook dcsns-feed-0" rel="295631">
                                        <div class="inner"><span class="section-thumb"><a href="https://www.facebook.com/kitkat.arabia/posts/1300643169965683:0" target="_blank"><img alt="" src="http://graph.facebook.com/1300643169965683/picture?type=normal"></a></span><span class="section-text"><a href="https://www.facebook.com/kitkat.arabia/posts/1300643169965683:0" target="_blank">فاجئ دروغبا وفن فور لويس أطفال مدارس خطة نستله للكاكاو مع ‫#‏بريك‬ كرة! <br>Fun For Louis and Didier Drogba surprising kids at a school built within the Nestlé Cocoa Plan</a></span><span class="section-user"><a href="https://www.facebook.com/126374857392526" target="_blank">126374857392526</a></span><span class="clear"></span></div><span class="section-intro"><a href="https://www.facebook.com/kitkat.arabia/posts/1300643169965683:0" target="_blank">Posted</a> <span>2 days + 4 hrs ago</span></span>
                                        <a href="https://www.facebook.com/126374857392526" target="_blank"><img class="icon" alt="" src="assets/images/dcsns-dark/facebook.png"></a>
                                    </li>
                                    <li class="dcsns-li dcsns-youtube dcsns-feed-0" rel="367999" style="left: 0px;">
                                        <div class="inner"><span class="section-icon"></span><span class="section-thumb"><a title="كيت كات ميني مومنتس " href="https://www.youtube.com/watch?v=of4JWg_hf9s" target="_blank"><img alt="" src="https://i.ytimg.com/vi/of4JWg_hf9s/mqdefault.jpg"></a></span><span class="section-title"><a title="كيت كات ميني مومنتس " href="https://www.youtube.com/watch?v=of4JWg_hf9s" target="_blank">جديد كيت كات سنسز بالبندق</a></span><span class="section-user"><a href="https://www.youtube.com/channel/UCSjcZqAllTOQEnfrNtG8gKA" target="_blank">KitKatArabia</a></span><span class="clear"></span></div><span class="section-intro"><a href="https://www.youtube.com/watch?v=of4JWg_hf9s" target="_blank">Uploaded</a> <span>1 month ago</span></span>
                                        <a href="https://www.youtube.com/channel/UCSjcZqAllTOQEnfrNtG8gKA" target="_blank"><img class="icon" alt="" src="assets/images/dcsns-dark/youtube.png"></a>
                                    </li>

                                    <li class="dcsns-li dcsns-instagram dcsns-feed-0" rel="896367">
                                        <div class="inner"><span class="section-icon"></span><span class="section-thumb"><a href="https://www.instagram.com/p/BDtHkJPl_WB/" target="_blank"><img alt="" src="https://igcdn-photos-a-a.akamaihd.net/hphotos-ak-xtp1/t51.2885-15/e35/11910477_1704682326433384_1240201010_n.jpg?ig_cache_key=MTEwOTk2OTAwMDQ5MjE5NDc2Mg%3D%3D.2"></a></span><span class="section-title"></span><span class="section-user"><a href="https://www.instagram.com/p/9nZuzSCFvK/" target="_blank">kitkatarabia</a></span><span class="clear"></span></div>
                                        <a href="https://www.instagram.com/kitkatarabia/" target="_blank"><img class="icon" alt="" src="assets/images/dcsns-dark/instagram.png"></a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <!-- <div class="load-more">
                        <h3>LOAD MORE</h3>
                    </div> -->
                    </div>
                </div>
            </section>	
	   </div>
	 <!-- Social Sphere ends-->
    </main>
    <?php include("footer.php"); ?>
        <script src="assets/js/custom.js?v1.0" type="application/javascript"></script>
        <script src="assets/js/owl.carousel.min.js" type="application/javascript"></script>
		<script type="text/javascript" src="assets/js/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="assets/js/slick.min.js"></script>
		<script src="assets/js/jquery.social.stream.wall.1.6.js" type="application/javascript"></script>
        <script src="assets/js/jquery.social.stream.1.5.12.min.js" type="application/javascript"></script>
        <script src="assets/js/ilightbox.min.js" type="application/javascript"></script>
		<script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
		
        <script type="text/javascript">
            $(document).ready(function () {
				
				 if(window.location.href.indexOf("#journey") > -1) {
					     $('html, body').animate({
								scrollTop: $('#journey').offset().top
							},
							800);
					}
				
				//var x = $('.usual-breaker .item img').width();
				//$('.usual-breaker .item img').css('width',Math.round(x / 60) * 60);
				//console.log();
				//$('.usual-breaker').css('width',$(window).innerWidth());
				// $('.usual-breaker .item img').each(function (i, el) {
	
				// });
				 $('.time-line-video1').iLightBox();
				 $('.time-line-video2').iLightBox();
				$('.spotlight-items').css('height',$(window).height());
				if($(window).width() < 751){
					$('.time-line .time-line-item ').css('width',$(window).width());
					}
				
                $('.usual-breaker').slick({
                    variableWidth: true,
                    infinite: false,slidesToScroll:3,
                    nav: false, speed:600,rtl:true,
					responsive: [{
						  breakpoint: 1024,
						  settings: {
							slidesToScroll:2,
							variableWidth: false
						  }

					}, {
						breakpoint:800,
						  settings: {
						 slidesToScroll:1,
						 variableWidth: false
						   }

						 }]
                  });
                $('.right-arrow').click(function(){
                   $('.usual-breaker').slick('slickNext');
                });
                $('.left-arrow').click(function(){
                     $('.usual-breaker').slick('slickPrev');
                });


                 $('.time-line').slick({
                  centerMode: true,variableWidth: true,
                  centerPadding: '0px',
                  infinite: false,rtl:true,
                  dots: true,
                  arrows: false,
                  onAfterChange: function paginationSet(slick,index){
                    paginationSet(index);  

                  },
                  customPaging: function(slick,index) {
                         switch(index){
                            case 0: return '<a>' + "INTRO" + '</a>';
                                    break;
                            case 1: return '<a>' + 1935 + '</a>';
                                    break;
                            case 2: return '<a>' + 1937 + '</a>';
                                    break;
                            case 3: return '<a>' + 1944 + '</a>';
                                    break;
                            case 4: return '<a>' + 1949 + '</a>';
                                    break;
                            case 5: return '<a>' + 1950 + '</a>';
                                    break;  
                            case 6: return '<a>' + 1958 + '</a>';
                                    break;
                            case 7: return '<a>' + 1960 + '</a>';
                                    break;
                            case 8: return '<a>' + 1969 + '</a>';
                                    break;
                            case 9: return '<a>' + 1970 + '</a>';
                                    break;
                            case 10: return '<a>' + 1987 + '</a>';
                                    break;
                            case 11: return '<a>' + 1988 + '</a>';
                                    break;
                            case 12: return '<a>' + 1999 + '</a>';
                                    break;
                            case 13: return '<a>' + 2000 + '</a>';
                                    break;
                            case 14: return '<a>' + 2005 + '</a>';
                                    break;
                            case 15: return '<a>' + 2006 + '</a>';
                                    break;
                            case 16: return '<a>' + 2015 + '</a>';
                                    break;
                            case 17: return '<a>' + 2016 + '</a>';
                                    break;   
                            }      
                 },
                   responsive: [
                     {
                      breakpoint: 1920,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0px',
                        slidesToShow: 1, slidesToScroll:1,
                      }
                    },
                     {
                      breakpoint: 1601,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '300px',
                        slidesToShow: 1
                      }
                    },
                    {
                      breakpoint: 1441,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '300px',
                        slidesToShow: 1
                      }
                    },
                    {
                      breakpoint: 1366,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '200px',
                        slidesToShow: 1
                      }
                    },
                    ,
                    {
                      breakpoint: 1025,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '175px',
                        slidesToShow: 1
                      }
                    },
                    {
                      breakpoint: 992,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '100px',
                        slidesToShow: 1
                      }
                    },
                    {
                      breakpoint: 801,
                      settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '30px',
                        slidesToShow: 1,variableWidth:false
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '30px',variableWidth:false,
                        slidesToShow: 1
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '00px',variableWidth:false,
                        slidesToShow: 1
                      }
                    }
                  ]
                });

				$('.breaker-couter').css('top',$('.header-wrap').height());
				//$('.breaker-couter').css('left',$(".header-wrap .container").offset().left);
				$('.breaker-couter').css('left',10);
				
            });
			  $(window).resize(function () {
				  $('.breaker-couter').css('top',$('.header-wrap').height());
				  $('.spotlight-items').css('height',$(window).height());
					/*if($(window).width() < 751){
						$('.time-line .time-line-item ').css('width',$(window).width());
						
					}*/
            });
        </script>

</body>

</html>