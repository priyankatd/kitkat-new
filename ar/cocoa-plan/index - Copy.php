<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="../assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/common.css">

    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="../assets/css/ie8.css">
    <script src="../assets/js/ie8.js"></script>

    <![endif]-->
</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php 
            include("../config.php");
            include("../header.php");        
        ?>
    </header>

    <main class="clearfix page-content">
	<!-- Video Container start-->
       <div id="video-container">
			<div class="video">
				
			</div>
			<div class="clearfix thumbnails-container">
				<div class="thumbnails">
				
				</div>
				<div class="thumbnails">
				
				</div>
				<div class="thumbnails">
				
				</div>
				<div class="thumbnails">
				
				</div>
			</div>
	   </div>
	 <!-- Video Container ends-->

    </main>
    <?php include("../footer.php"); ?>
        <script src="../assets/js/custom.js" type="application/javascript"></script>
        <script src="../assets/js/owl.carousel.min.js" type="application/javascript"></script>
        <script type="text/javascript" src="../assets/js/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../assets/js/slick.min.js"></script>
		
        <script type="text/javascript">
            $(document).ready(function () {
				$('#video-container').css('margin-top',$('.header-wrap').height());
                //$('.usual-breaker').css('width',$(window).innerWidth());
				$('.spotlight-items').css('height',$(window).height()-1);
                $('.usual-breaker').slick({
                    variableWidth: true,
                    infinite: false,
                    nav: false 
                    // prevArrow: $('.prev'),
                    // nextArrow: $('.next')
                  });

            });
        </script>

</body>

</html>