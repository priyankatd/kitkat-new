<footer class="clearfix kitkat-footer">
    <div class="footer-wrap">
        <div class="container">
            <div class="row">
                <div class="social">
                    <ul class="social-navs">
                        <span>تابعونا</span>
                        <li><a href="https://www.facebook.com/kitkat.arabia" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
                        <li><a href="https://twitter.com/kitkatarabia" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/user/KitKatArabia" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a href="https://www.instagram.com/kitkatarabia/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="http://breakerpage.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a></li>

                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="social">
                    <p>© 2016&nbsp;كافة حقوق الملكية الفكرية محفوظة لشركة سوسيتيه دي برودوي نستله إس. إيه. فيفي – سويسرا – مالكة العلامة التجارية&nbsp;</p>
                    <p><a href="<?php echo $root; ?>/en/consumer-privacy/">الأمن و الخصوصية</a> | <a href="<?php echo $root; ?>/en/terms-conditions/">الشروط  و الأحكام</a> | <a href="<?php echo $root; ?>/en/terms-conditions/">شروط و أحكام الحكلة</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>