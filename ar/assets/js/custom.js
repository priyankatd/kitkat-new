// Custom Javascripts



$(".menu-trigger").on("click", function () {
			$(this).toggleClass("active");
			if ($(".menu-trigger").hasClass("active")) {
				$(".main-menu-innner").addClass("active");
				$(".nav-wrap").addClass("active");
				$('.page-content').addClass("pushbody");
				$(".kitkat-footer").addClass("pushbody");

			} else if ($(".menu-trigger").not("active")) {
				$(".main-menu-innner").removeClass("active");
				$(".nav-wrap").removeClass("active");
				$(".page-content").removeClass("pushbody");
				$(".kitkat-footer").removeClass("pushbody");
			}
		});
		$(".page-content").on("click", function () {
			//alert();
			$(".menu-trigger").removeClass("active");
			$(".main-menu-innner").removeClass("active");
			$(".nav-wrap").removeClass("active");
			$(".page-content").removeClass("pushbody");
		});
		//$("ul.main-menu-innner").css("height", 400+"px");
		$(".main-menu-innner a").on("click", function () {
			$(".main-menu-innner").removeClass("active");
			$(".nav-wrap").removeClass("active");
			$(".page-content").removeClass("pushbody");
		});

function check_if_in_view() {

    var inViewElement = $('.scroll-in-view-element');
    var window_height = $(window).height();
    var window_top_position = $(window).scrollTop();
    var window_bottom_position = (window_top_position + window_height);
    var element_height = inViewElement.outerHeight();
    var element_top_position = inViewElement.offset().top;
    var element_bottom_position = (element_top_position + element_height);
    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {


        setTimeout(function () {
            $(".usual-breakers-item").addClass("active animated fadeInUp");
            $(".usual-breakers-item").addClass("active");

        }, 500);
    }
    // var inViewElementOdometre = $('.scroll-in-view-element-odometre');
    // var element_height_odometre = inViewElementOdometre.outerHeight();
    // var element_top_position_odometre = inViewElementOdometre.offset().top;
    // var element_bottom_position_odometre = (element_top_position_odometre + element_height_odometre);
    //   if ((element_bottom_position_odometre >= window_top_position) &&
    //       (element_top_position_odometre <= window_bottom_position)) {
    //         setTimeout(function(){
    //               odometer.innerHTML = 10000;

    //             }, 800);
    //     }
}

//var startPosition;
function productAccordian() {
    if ($(window).width() < 768) {
        var vHeight = $(".product-vertical-item").parent(".owl-item.active").height();
        var hHeight = $(".product-horizontal-item").parent(".owl-item.active").height();
        console.log(hHeight);
        console.log(vHeight);
        $(".product-inner-main-carousel").height(hHeight);
        $(".mini-moments-carousal").height(vHeight);
    }
}
// usual breakers section functions in homepage
function usualBreakerActions() {


    if ($(".usual-breakers-item").first().parent(".owl-item").hasClass("active") && $(".usual-breakers-item").last().parent(".owl-item").hasClass("active")) {
        $('.prev-trigger').hide();
        $('.next-trigger').hide();
    } else {
        $('.prev-trigger').hide();
        $('.next-trigger').show();
    }
    $(".usual-breakers-carousal").bind("touchstart", function () {
        $('.prev-trigger').show();
        $('.next-trigger').show();
    });
    $('.prev-trigger').click(function () {
        if ($(".usual-breakers-item").first().parent(".owl-item").hasClass("active")) {
            $('.prev-trigger').hide();
            $('.next-trigger').show();
        } else {
            $('.prev-trigger').fadeIn();
            $('.next-trigger').fadeIn();
        }
    });
    $('.next-trigger').click(function () {
        // $(".usual-breakers-item").each(function(){

        //$(".usual-breakers-item:nth-child(4)").css("margin-left" ,"120")

        if ($(".usual-breakers-item").last().parent(".owl-item").hasClass("active")) {
            $('.prev-trigger').fadeIn();
            $('.next-trigger').fadeOut();
        } else {
            $('.prev-trigger').fadeIn();
            $('.next-trigger').fadeIn();
        }
    });

    if ($(window).width() > 1280) {
        $(".usual-breaker-item-inner").mouseenter(function () {
            $(this).parent(".usual-breakers-item").animate({
                bottom: "45"
            }, 300);
            $(".usual-breakers-item").removeClass("hover-active");
            $(this).parent(".usual-breakers-item").addClass("hover-active");
        });
        $(".usual-breaker-item-inner").mouseleave(function () {
            $(".usual-breakers-item").removeClass("hover-active");
            $(this).parent(".usual-breakers-item").animate({
                bottom: "0"
            }, 300);
        });

        $(".usual-breakers-item").on("click", function () {
            $(this).removeClass("hover-active");
            var startPosition = (this).getAttribute("current-item");
        });
    }
}

function productInnerOwlTriggerAnimation() {
    var bgCounter = 1;
    $(".mini-moments-carousal .owl-prev").click(function () {
        $(".nutri-info").hide();
        minimomentsOwl.trigger('next.minimomentsOwl.carousel');
        console.log(bgCounter);
        switch (bgCounter) {
        case 1:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#a9774a"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#ebaf7b" }, "500");
            bgCounter = bgCounter;
            break;

        case 2:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#a9774a"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#ebaf7b" }, "500");
            bgCounter = bgCounter - 1;
            break;
        case 3:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#298240"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#298240" }, "500");
            bgCounter = bgCounter - 1;
            break;
        case 4:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#de2127"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#de2127" }, "500");
            bgCounter = bgCounter - 1;
            break;
        case 5:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#92d4ef"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#92d4ef" }, "500");
            bgCounter = bgCounter - 1;
            break;
        }
    });
    $(".mini-moments-carousal .owl-next").click(function () {
        $(".nutri-info").hide();
        console.log(bgCounter);
        switch (bgCounter) {

        case 1:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#298240"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#298240" }, "500");
            bgCounter = bgCounter + 1;
            break;
        case 2:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#de2127"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#de2127" }, "500");
            bgCounter = bgCounter + 1;
            break;
        case 3:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#92d4ef"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#92d4ef" }, "500");
            bgCounter = bgCounter + 1;
            break;
        case 4:
            $(".mini-moments-carousal .owl-controls").animate({
                backgroundColor: "#f4c069"
            }, "500");
            // $(".back-to-product-main i").animate({ color: "#f4c069" }, "500");
            bgCounter = bgCounter + 1;
            break;


        }
        minimomentsOwl.trigger('prev.minimomentsOwl.carousel', [300]);
    });
}

function triggerColorAnimation() {
    productMainOwl.on('initialized.owl.carousel', function (property) {
        var current = property.item.index;
        var color = $(property.target).find(".owl-item").eq(current).find(".product-horizontal-item").attr('main-nav-color');
        console.log(color);
        $(".product-main-prev-trigger").css('border', '2px solid' + color);
        $(".product-main-next-trigger").css('border', '2px solid ' + color);
        $(".product-popup .back-to-product-main").css('border', '2px solid ' + color);
        $(".product-popup .product-main-triggers i").css("color", color);
        $(".back-to-product-main i").css("color", color);
    });

    productMainOwl.on('changed.owl.carousel', function (property) {
        var current = property.item.index;
        var color = $(property.target).find(".owl-item").eq(current).find(".product-horizontal-item").attr('main-nav-color');
        console.log(color);
        $(".product-main-prev-trigger").css('border', '2px solid' + color);
        $(".product-main-next-trigger").css('border', '2px solid ' + color);
        $(".product-popup .back-to-product-main").css('border', '2px solid ' + color);
        $(".product-popup .product-main-triggers i").css("color", color);
        $(".back-to-product-main i").css("color", color);
    });
}
var productLandingOwl = $(".product");

function callProductLandingOwl() {

    setTimeout(function () {
        var productLandingOwl = $(".product");
        productLandingOwl.trigger('destroy.owl.carousel');
        productLandingOwl.find('.owl-stage-outer').children().unwrap();
        productLandingOwl.owlCarousel({
            loop: false,
            nav: true,
            margin: 0,
            mouseDrag: true,
            lazyLoad: true,
            navSpeed: 800,
            items: 3,
            mouseDrag: false,
            touchDrag: false,
            dots: true,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1
                },
                768: {
                    items: 2,
                    slideBy: 2
                },
                991: {
                    items: 2,
                    slideBy: 2
                },
                1281: {
                    items: 3,
                    slideBy: 3
                }
            }
        });
    }, 50);
}

function minimomentsResize() {
    product - vertical - item
}

function callproductpopup() {
    $(".product-popup").addClass("active");
    $(".product-range").removeClass("active");
    // product main owl initialisation

    productMainOwl.trigger('destroy.owl.carousel');
    productMainOwl.find('.owl-stage-outer').children().unwrap();
    productMainOwl.owlCarousel(productMainOwlSettings);
    triggerColorAnimation();

    $('.product-main-prev-trigger').click(function () {
        productAccordian();
        productMainOwl.trigger('prev.owl.carousel');
        // productMainOwl.on('changed.owl.carousel', function (property) {
        //     current = property.item.index;
        //     triggerColorAnimation(current);
        // });
    });
    $('.product-main-next-trigger').click(function () {
        productAccordian();
        productMainOwl.trigger('next.owl.carousel');
        productMainOwl.on('changed.owl.carousel', function (property) {
            current = property.item.index;
            triggerColorAnimation(current);
        });
    });

    minimomentsOwl.owlCarousel(minimomentsOwlSettings);
    productAccordian();
}

function productHorizontalItemInnerCounting() {
    if ($(".product-horizontal-item").parents().hasClass("owl-item.active")) {
        console.log("edfds");
    }
}

function productHorizontalItemInnerCalling() {
    if ($(window).width() <= 1281) {

        var productBottomOwl = $(".horizontal-item-left-bottom");

        var productBottomOwlSettings = {
            loop: false,
            items: 2,
            nav: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 0,
            navSpeed: 800,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            touchDrag: false,
            dots: false,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            responsive: {
                0: {
                    nav: true,
                    dots: true,
                    items: 1,
                },
                767: {
                    dots: false,
                    nav: true,
                    items: 1,
                },
                1025: {
                    items: 2,
                    nav: true,
                },

                1281: {
                    nav: false,
                }
            }
        };
        productBottomOwl.trigger('destroy.owl.carousel');
        productBottomOwl.find('.owl-stage-outer').children().unwrap();
        productBottomOwl.owlCarousel(productBottomOwlSettings);
        $(".horizontal-item-left-bottom-single.item1").on("click", function () {
            $(this).parent().parent().find(".owl-item").children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).addClass("active");
            $(this).parents().find(".horizontal-item-left").children().children(".horizontal-item-left-inner").removeClass("active");
            $(this).parents().find(".horizontal-item-left").children().children(".horizontal-item-left-inner.item1").addClass("active");
        });
        $(".horizontal-item-left-bottom-single.item2").on("click", function () {
            $(this).parent().parent().find(".owl-item").children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).addClass("active");
            $(this).parents().find(".horizontal-item-left").children().children(".horizontal-item-left-inner").removeClass("active");
            $(this).parents().find(".horizontal-item-left").children().children(".horizontal-item-left-inner.item2").addClass("active");
        });
        $(".horizontal-item-left-bottom-single.item3").on("click", function () {
            $(this).parent().parent().find(".owl-item").children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).addClass("active");
            $(this).parents().find(".horizontal-item-left").children().children(".horizontal-item-left-inner").removeClass("active");
            $(this).parents().find(".horizontal-item-left").children().children(".horizontal-item-left-inner.item3").addClass("active");
        });
        var currentBottomOwl = 0;
        productBottomOwl.on('changed.owl.carousel', function (property) {
            currentBottomOwl = property.item.index;
            currentBottomOwl = currentBottomOwl + 1;
            console.log(currentBottomOwl);
            $(this).parents(".horizontal-item-left").children(".horizontal-item-left-inner-wrap").find(".horizontal-item-left-inner").hide();
            $(this).parents(".horizontal-item-left").children(".horizontal-item-left-inner-wrap").find(".horizontal-item-left-inner:nth-child(" + currentBottomOwl + ")").show();
            $(this).children(".owl-stage-outer").find(".owl-item").children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).children(".owl-stage-outer").find(".owl-item.active").children(".horizontal-item-left-bottom-single").addClass("active");
        });

    } else {
        $(".horizontal-item-left-bottom-single.item1").on("click", function () {
            $(this).parent().children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).addClass("active");
            $(this).parent().parent().children().children(".horizontal-item-left-inner").removeClass("active");
            $(this).parent().parent().children().children(".horizontal-item-left-inner.item1").addClass("active");
        });
        $(".horizontal-item-left-bottom-single.item2").on("click", function () {
            $(this).parent().children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).addClass("active");
            $(this).parent().parent().children().children(".horizontal-item-left-inner").removeClass("active");
            $(this).parent().parent().children().children(".horizontal-item-left-inner.item2").addClass("active");
        });
        $(".horizontal-item-left-bottom-single.item3").on("click", function () {
            $(this).parent().children(".horizontal-item-left-bottom-single").removeClass("active");
            $(this).addClass("active");
            $(this).parent().parent().children().children(".horizontal-item-left-inner").removeClass("active");
            $(this).parent().parent().children().children(".horizontal-item-left-inner.item3").addClass("active");
        });
    }
}

//+++++++++contact us functions++++++++++++//
function contactFunctions() {
    if ($(window).width() > 991) {
        $(".contact-us-popup").removeClass("mCustomScrollbar");
        $(".contact-us").height($(window).height());
        var tabs = $('.tabs-titles li.click-act');
        var contents = $('.tabs-contents li.tab-act');
        tabs.bind('click', function () {
            contents.hide();
            tabs.removeClass('current');
            $(contents[$(this).index()]).show();
            $(this).addClass('current');
            // alert($(this).index());
        });
    }

    else{
        var accordianClick = 1;
        $(".mob-toggle-btn").click(function () {
            if (accordianClick == 1) {
            $(".tabs-contents li.tab-act .content").hide();
            $(this).parent('li.tab-act').children(".content").slideToggle("slow");
            $(this).parent('li.tab-act').children(".content").removeClass("info-active");
            // $(this).toggleClass("mob-activeTab");
            if ($(".tabs-contents li.tab-act .content:hidden")) {
                $(".mob-toggle-btn").removeClass("mob-activeTab");
            }
            if ($(this).parent('li.tab-act').children(".content:visible")) {
                $(this).addClass("mob-activeTab");
            }
             accordianClick = 2;
        }
        else {
          $(".tabs-contents li.tab-act .content").hide();
          accordianClick = 1;
          $(this).removeClass("mob-activeTab");  
        }

        });
    }
    $("a.privacy-detailed").on("click", function () {
        $(".privacy-statement-details").addClass("active");
    });
    $(".privacy-statement-details .statement-closer").on("click", function () {
        $(".privacy-statement-details").removeClass("active");
    });
    $(".tab-inner-section-item.comment").on("click", function () {
        $(".tabs-inner-section").removeClass("active", 300);
        $(".tab-inner-section-items-popups.comment").addClass("active", 200);
        $(".tab-inner-section-items-popups.comment").addClass("active", 200);
    });
    $(".tab-inner-section-item.claim").on("click", function () {
        $(".tab-inner-section-items-popups.claim").addClass("active", 200);
    });
    $(".tab-inner-section-item.question").on("click", function () {
        $(".tab-inner-section-items-popups.question").addClass("active", 200);
    });
    $(".tab-inner-section-items-popups .form-closer").on("click", function () {
        $(".tab-inner-section-items-popups.comment").removeClass("active");
        $(".tab-inner-section-items-popups.claim").removeClass("active");
        $(".tab-inner-section-items-popups.question").removeClass("active");

    });
    $(".tab-inner-section-item").hover(function () {
        $(".tab-inner-section-item").removeClass("m-hover");
        $(this).addClass("m-hover");
    });
    $(".tab-inner-section-item").mouseleave(function () {
        $(".tab-inner-section-item").removeClass("m-hover");
    });
    //Initialising Accordion
    $(".accordion").tabs({
        tabs: '> h2',
        effect: 'slide',
        initialIndex: null
    });

    //The click to hide function
    $(".accordion > h2").on("click", function () {
        if ($(this).hasClass("current") && $(this).next().queue().length === 0) {
            $(this).next().slideToggle("slow");
            $(this).removeClass("current");
        } else if (!$(this).hasClass("current") && $(this).next().queue().length === 0) {
            $(this).next().slideToggle("slow");
            $(this).addClass("current");
        }
    });
    // image change
    $(".contact-us-trigger .trig-button").on("click", function () {
        contactUsFileupload();
        $(".tabs-inner-section").addClass("active", 300);
        $(".contact-us").toggleClass("active", "slow");
        $(this).children(".triggerer").toggleClass("active");
        $(this).children(".closer").toggleClass("active");
        $(".tab-inner-section-items-popups").removeClass("active", "slow");
        $(".privacy-statement-details").removeClass("active", "slow");
    });

}

function contactUs() {
	$('#iframe_1').iLightBox();
    /*$(".trig-button").trigger('click');
    return false;*/
}

function click(el) {
    var evt = document.createEvent('Event');
    evt.initEvent('click', true, true);
    el.dispatchEvent(evt);
    return false;
}

function contactUsFileupload() {

    // document.querySelector('.file-upload.file-upload1').addEventListener('click', function (e) {
    //     var fileInput = document.querySelector('.fileElem');
    //     //click(fileInput);
    //     fileInput.click();
    // });
    // document.querySelector('.file-upload.file-upload2').addEventListener('click', function (e) {
    //     var fileInput = document.querySelector('.fileElem');
    //     //click(fileInput);
    //     fileInput.click();
    // });
    // document.querySelector('.file-upload.file-upload3').addEventListener('click', function (e) {
    //     var fileInput = document.querySelector('.fileElem');
    //     //click(fileInput);
    //     fileInput.click();
    // });
}






$(document).ready(function () {
    // var isFirefox = typeof InstallTrigger !== 'undefined';
    // console.log(isFirefox);
    //  if(isFirefox ==true){
    //   $(".usual-breakers-carousal").css("bottom",-5)
    //  }
    var productMainOwl = $('.product-inner-main-carousel');
    var minimomentsOwl = $('.mini-moments-carousal');

    var productMainOwlSettings = {
        loop: false,
        items: 1,
        nav: false,
        navSpeed: 800,
        margin: 0,
        touchDrag: false,
        mouseDrag: false,
        dots: false,
        navSpeed: 1000,
        URLhashListener: true,
        startPosition: 'URLHash',
        animateOut: 'fadeOut',
        responsive: {
            0: {
                dots: true,
            },
            992: {
                dots: false,
            }
        }
    };
    var minimomentsOwlSettings = {
        loop: false,
        items: 1,
        nav: true,
        mouseDrag: false,
        margin: 0,
        navSpeed: 800,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        startPosition: 'startPosition',
        touchDrag: false,
        dots: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {

                dots: true,
            },
            992: {

            }
        }
    };

    minimomentsOwl.on('resized.owl.carousel', function (event) {
        $(this).children(".owl-controls").css("top", $(".product-vertical-item.mocha .vertical-item-left").height() - 70);
    });

    minimomentsOwl.on('initialized.owl.carousel', function (event) {
        $(this).children(".owl-controls").css("top", $(".product-vertical-item.mocha .vertical-item-left").height() - 70);
    });
    callProductLandingOwl();
    contactFunctions();
    $(".kitkat-banner").on("click", function () {
        $(".triggerer").addClass("active");
        $(".closer").removeClass("active");
        $(".contact-us").removeClass("active", "slow");
        $(".tab-inner-section-items-popups").removeClass("active", "slow");
        $(".privacy-statement-details").removeClass("active", "slow");
        // var x=document.getElementsByClassName("contact-us-form").reset();
        // console.log(x);
        //$(".main-menu-innner").removeClass("active","slow");

    });
    $(".product-range").on("click", function () {
        $(".triggerer").addClass("active");
        $(".closer").removeClass("active");
        $(".contact-us").removeClass("active", "slow");
        $(".tab-inner-section-items-popups").removeClass("active", "slow");
        $(".privacy-statement-details").removeClass("active", "slow");
        // var x=document.getElementsByClassName("contact-us-form").reset();
        // console.log(x);
        //$(".main-menu-innner").removeClass("active","slow");

    });
	$('.spotlight-items.senses').css('display','block');
    // banner owl initialisation
    $("#spotlight-home").owlCarousel({
        autoplay: true,
        autoplaySpeed: 750,
        loop: true,
        margin: 0,
        nav: true,
        mouseDrag: false,
        lazyLoad: false,
        items: 1,
        touchDrag: true,
        dots: true,rtl:true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        responsive: {
            0: {
                items: 1,
                touchDrag: true,
            },
            1280: {
                touchDrag: true,
            }
        }
    });

    $(".push-down").click(function () {
        $('html, body').animate({
                scrollTop: $('#journey').offset().top - 150
            },
            800);
        return false;
    });

    // ++++++++++++++++++contact us++++++++++++++++++
    // var numb[];
    // var i=0;
    // $(".call-us-item").each(function(){
    //   var numb[i] = $('.call-us-item h5').val();
    //   i++;
    // });


    // home+++++++=usal breakers area++++++++++++++
    var usualBreakersOwl = $(".usual-breakers-carousal");
    usualBreakersOwl.owlCarousel({
        loop: false,
        nav: true,
        mouseDrag: false,
        navSpeed: 800,
        responsive: true,
        lazyLoad: true,
        items: 4,
        touchDrag: false,
        slideBy: 2,
        navSpeed: 800,
        dots: false,
        nav: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1,
                touchDrag: true,
            },
            767: {
                items: 2,
                touchDrag: true,
            },
            1025: {
                items: 3,
                touchDrag: false,
            },
            1600: {
                items: 4,
            },
        }
    });
    usualBreakerActions();
    $('.prev-trigger').click(function () {
        usualBreakersOwl.trigger('prev.owl.carousel');
    });
    $('.next-trigger').click(function () {
        usualBreakersOwl.trigger('next.owl.carousel', [800]);

    });


    //+++++++++++product page popups++++++++++++++++++++
    // product main owl initialisation
    productMainOwl.on('initialized.owl.carousel', function (property) {
        current = property.item.index;
        triggerColorAnimation(current);
    });
    productMainOwl.on('changed.owl.carousel', function (property) {
        current = property.item.index;
        triggerColorAnimation(current);
    });
    $(".product .item a").on("click", function () {
        callproductpopup();
        productInnerOwlTriggerAnimation();
        $(".back-to-product-main").show();
    });

    $(".back-to-product-main").on("click", function () {
        $(this).hide();
        $(".product-popup").removeClass("active");
        $(".product-range").addClass("active");
        callProductLandingOwl();
        // $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 1});
        // $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({default_offset_pct: 0, orientation: 'vertical'});
    });
    $(".tear-here").on("drag", function () {
        $(".item-image").removeClass("teared");
        $(".product-normal").removeClass("animated shake rotateOutUpLeft");
        $(this).parent(".item-tear-inner").children().children(".product-normal").addClass("animated shake");
        $(this).parent(".item-tear-inner").children().children(".product-normal").addClass("animated bounceOutLeft", 800);
        $(this).parent().parent(".item-image").addClass("teared", 800);
        $(".product-range .item .item-image .tear-here").removeClass("teared");
        $(this).addClass("teared");
    });
    $(".product-range .item").on("click", function () {
        $(".item-image").removeClass("teared");
        $(".product-normal").removeClass("animated shake rotateOutUpLeft");
        $(".product-range .item .item-image .tear-here").removeClass("teared");
    });
    //+++++++++++product page nutrient table popup++++++
    $(".nutritional-info").on("click", function () {
        var nutritionalPopup = $(this).attr("href");
        $(".nutri-popup-overlay").fadeIn();
        $(nutritionalPopup).fadeIn();
        $(nutritionalPopup).position({
            my: "center",
            at: "center",
            of: window
        });
        return false;
    });

    $(".nutri-popup-trigger").click(function (event) {
        $(this).toggleClass("active");
        $(this).parents(".mobile-nutri-pop-up").children(".nutri-info").slideToggle();
        var owlactiveHeight = $(this).parents(".owl-item.active").height();
    });
    $(".nutri-closer, .nutri-popup-overlay").click(function (event) {
        $(".nutri-popup-overlay, .nutri-popup-outer").fadeOut();
    });
    

    var audio = $(".break-sound")[0];
    $("#spotlight-home .owl-dots .owl-dot").click(function () {
        audio.play();
    });


    // +++++++++++++++buy now button i light box popup++++++++++++++++
    //lightbox = new fusepump.lightbox.buynow(4506);
    $(".p-buynow").on("click", function () {
        var buyId = $(this).attr('data-buynowid');
        if (buyId) {
            lightbox = new fusepump.lightbox.buynow(buyId);
            lightbox.show(); // Show the lightbox
        }
        return false;
    });
});




$(window).scroll(function () {
    //tag show and hidden 
    if ($(window).width() > 1280) {
        if ($(window).scrollTop() > 600) {
            $(".breaker-couter").removeClass("bounceInDown");
            $(".breaker-couter").addClass("fadeOutUp");
        } else {

            if ($(".breaker-couter").hasClass("bounceInDown")) {
                $(".breaker-couter").removeClass("fadeOutUp");
            } else {
                $(".breaker-couter").addClass("bounceInDown");
            }
        }
    } else {
        if ($(window).scrollTop() > 300) {
            $(".breaker-couter").removeClass("bounceInDown");
            $(".breaker-couter").addClass("fadeOutUp");
        } else {

            if ($(".breaker-couter").hasClass("bounceInDown")) {
                $(".breaker-couter").removeClass("fadeOutUp");
            } else {
                $(".breaker-couter").addClass("bounceInDown");
            }
        }
    }

});
$(window).load(function () {});
$(window).resize(function () {
    // contactFunctions();
});