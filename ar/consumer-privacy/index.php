<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Products - KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="../assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom-new.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/footer-styles.css">
	<style>.slick-next{width:63px;height:63px;right:50px;background:url('../assets/images/r-arrow.png') no-repeat;}.slick-prev{width:63px;height:63px;left:50px;background:url('../assets/images/l-arrow.png') no-repeat;    z-index: 9;}.slick-prev:before,.slick-next:before{content:''}body{overflow-x:hidden;}</style>
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="../assets/js/ie8.js"></script>

    <![endif]-->
<!--     <script>
        if (top != self) {
            top.location = self.location;
        }
        if (top.location != self.location) {
            top.location = self.location.href;
        }
        (function (window) {
            if (window.location !== window.top.location) {
                window.top.location = window.location;
            }
        })(this);
    </script> -->

</head>
<body id="products">
    <header class="kitkat-header">
        <?php 
            include("../config.php");
            include("../header.php");        
        ?>
    </header>

    <main class="page-content">
        <section class="tools-content">
            <div class="container">
                <div class="row">
                    <div class="main-content">
                        <div class="page-style frame">
                            <section>
                                <h2>Consumer Privacy Notice </h2>
                                <p>Effective on [January 2015]; last updated on [March 1, 2016] Nestlé Middle East FZE ("Nestlé") is committed to safeguarding your privacy and ensuring that you continue to trust Nestlé with your personal data. When you interact with us, you may share personal information with us which allows identification of you as an individual (e.g. name, email, address, telephone number). This is known as "personal data".
                                </p>
                                <p>This notice "<strong>("Privacy Notice")</strong> sets out:</p>
                                <ol>
                                    <li><a href="#scope-n-acceptance" class="smooth-scroll">Scope and acceptance</a></li>
                                    <li><a href="#personal-data" class="smooth-scroll">Personal data collected by Nestlé</a></li>
                                    <li><a href="#children-data" class="smooth-scroll">Children’s personal data</a></li>
                                    <li><a href="#nestle-collect" class="smooth-scroll">Why Nestlé collects personal data and how it uses it</a></li>
                                    <li><a href="#sharing" class="smooth-scroll">Sharing of personal data by Nestlé</a></li>
                                    <li><a href="#your-right" class="smooth-scroll">Your rights </a></li><a href="#your-right" class="smooth-scroll">
                        </a><li><a href="#your-right" class="smooth-scroll"></a><a href="#cookies" class="smooth-scroll">[Cookies and other technologies]</a></li>
                                    <li><a href="#data-security" class="smooth-scroll">Data security and retention</a></li>
                                    <li><a href="#contact-us" class="smooth-scroll">How to contact us</a></li>
                                </ol>
                                <h4 id="scope-n-acceptance">1 Scope and acceptance of this Privacy Notice</h4>
                                <p>This Privacy Notice applies to the personal data that we collect about you for the purposes of providing you with our products and services we offer.</p>
                                <p>By using Nestlé Sites (as defined below) or by giving us your personal data, you accept the practices described in this Privacy Notice. If you do not agree to this Privacy Notice, please do not use Nestlé Sites (as defined below) or give us any personal data.</p>
                                <p>Nestlé reserves the right to make changes to this Privacy Notice at any time. We encourage you to regularly review this Privacy Notice to make sure you are aware of any changes and how your personal data may be used.</p>
                                <h4 id="personal-data">2 Data collected by Nestlé</h4>
                                <p>Nestlé may collect personal data about you from a variety of sources, including through: </p>
                                <ul>
                                    <li>Online and electronic interactions with us, including via Nestlé websites, mobile applications, text messaging programs or through Nestlé branded pages or applications on third party social networks (e.g. Facebook) (Nestlé Sites);</li>
                                    <li>Offline interactions with us, including via direct marketing campaigns, hard copy registration cards, competition entries and contacts through Nestlé consumer services call centres; and</li>
                                    <li>Your interaction with online targeted content (such as advertisements) that Nestlé, or service providers on our behalf, provide to you via third party websites or applications.</li>
                                </ul>
                                <h5>2.1   Data that you provide to us directly</h5>
                                <p>This is data that you provide to us with your consent for a specified purpose, including:</p>
                                <ul>
                                    <li>Personal contact information, including any information allowing Nestlé to contact you in person (e.g. name, home or (e)mail address, and phone number);</li>
                                    <li>Demographic information, including date of birth, age, gender, location (e.g. zip code, city and state and geo-location), favourite products, hobbies, interests, and household or lifestyle information;</li>
                                    <li>Payment information, including to make purchases (e.g. credit card number, expiration date, billing address);</li>
                                    <li>Account login information, including any information that is required for you to establish a user account with Nestlé (e.g. login ID/email, user name, password and security question/answer);</li>
                                    <li>Consumer feedback, including information that you share with Nestlé about your experience in using Nestlé products and services (e.g. your comments and suggestions, testimonials and other feedback related to Nestlé products); and</li>
                                    <li>Consumer-generated content, including any content (e.g. photos, videos and personal stories) that you create and then share with Nestlé (and perhaps others) by uploading it to a Nestlé Site. </li>
                                </ul>
                                <h5>2.2   Data that we collect when you interact with Nestlé Sites</h5>
                                <p>We use cookies and other tracking technology which collect certain kinds of information when you interact with Nestlé Sites. Click <a href="#">here</a> </p>
                                <h5>2.3   Data collected from other sources </h5>
                                <p>We may collect information about you from other legitimate sources for the purpose of providing you with our products and services. Such sources include third party data aggregators, Nestlé promotional partners, public sources and third party social networking sites. Such information may include:</p>
                                <ul>
                                    <li>personal contact information; and</li>
                                    <li>any personal data that is part of your profile on a third party social network (e.g. Facebook) and that you allow that third party social network to share with us (e.g. name, email address, gender, birthday, city, profile picture, user ID, friend list). You can learn more about the data that we may obtain about you by visiting the website of the relevant third party social network.</li>
                                </ul>
                                <p>We may also receive personal data about individuals when we acquire other companies.</p>
                                <h4 id="children-data">3 Children’s personal data</h4>
                                <p>Nestlé does not knowingly solicit or collect personal data from children below the age of 12. If Nestlé discovers that it has accidentally collected personal data from a child below 12, it will remove that child’s personal data from its records as soon as reasonably possible. However, Nestlé may collect personal data about children below the age of 12 years of age from the parent or custodian directly and therefore with their explicit consent.</p>
                                <h4 id="nestle-collect">4  Why Nestlé collects personal data and how it uses it</h4>
                                <p>Nestlé collects and uses personal data only as necessary for the purposes for which it was obtained. Nestlé may use your personal data for some or all of the following purposes:</p>
                                <ul>
                                    <li><u>Orders</u> - to process and ship your orders and to inform you about the status of your orders. Please note that there are many e-commerce websites that sell Nestlé products but that are not controlled or operated by Nestlé. We recommend that you read their policies, including on privacy, before making any purchases on those websites.</li>
                                    <li><u>Account maintenance</u> - to create and maintain your accounts with us, including administering any consumer loyalty or rewards programs that are associated with your account.</li>
                                    <li><u>Consumer service</u> - to provide you with consumer service, including responses to your inquiries, complaints and general feedback about our products. Consumer service may be provided through various forms of communication, including via email, letter, telephone and online chat features.</li>
                                    <li><u>Consumer engagement</u> - to get you more actively engaged with our products and services. This may involve the use or publication of consumer-generated content.</li>
                                    <li><u>Personalisation</u> - Nestlé may combine personal data about you collected from one source (e.g. a website) with data collected from another source (e.g. an offline event). This provides Nestlé with a more complete view of you as a consumer, which, in turn, allows Nestlé to serve you better and with greater personalisation, including in respect of the following:</li>
                                    <ul>
                                        <li>Websites - to improve and personalise your experience on websites, using data such as account login information, technical computer information, and/or previous website usage information;</li>
                                        <li>Products - to improve Nestlé’s products, tailor them to your needs and come up with new product ideas. This includes the use of demographic information, consumer profiling information and consumer feedback; and</li>
                                        <li>Interest-based advertising - to serve you advertisements tailored to your interests. One way Nestlé does this is to match activities or information collected on Nestlé Sites with data collected about you on third party sites (i.e. data-matching). This type of advertising is also known as "online behavioural advertising" or "targeted advertising". Such personalisation is typically performed via cookies or similar technologies.</li>
                                    </ul>
                                    <li><u>Marketing communications</u> - to provide you with marketing communications where you have opted-in to receiving such communications (including information about Nestlé, its products and services, competitions and promotions). These can be shared via electronic means (e.g. SMS, emails and online advertising) and via post. If you opt-in to receiving SMS, your mobile service provider’s policy for receiving SMS will apply, which may be at a fee.</li>
                                    <li><u>Social features</u> – to offer you a number of social features, including the following:</li>
                                    <ul>
                                        <li>Website community features on a Nestlé Site
                                            <br> When you visit a Nestlé Site with a community feature and upload or share recipes, pictures, videos, artwork or other content, Nestlé may use and display the personal data that you share on such sites.
                                        </li>
                                        <li>
                                            Website viral features
                                            <br> Nestlé may use your personal data to offer you website viral features, such as a tell-a-friend program, where you can share certain news, product information, promotions or other content with family and friends.
                                            <br> This typically requires the collection and use of personal contact information (e.g. names and emails) so that the selected one-time message/content can be delivered to their recipients.
                                        </li>
                                        <li>Third party social networking
                                            <br> Nestlé may use your personal data from when you interact with third party social networking features such as "Facebook Connect" or "Facebook Like". These features may be integrated on Nestlé Sites including for purposes such as running contests and allowing you to share content with friends.
                                            <br> If you use these features, Nestlé may have the ability to obtain certain personal data about you from your social networking information. You can learn more about how these features work, and the profile data Nestlé may obtain about you, by visiting the website of the relevant third party social network.

                                        </li>
                                        <li>
                                            Other specific purposes
                                            <br> We may use your personal data for other specific business purposes, including to maintain the day-to-day operation and security of Nestlé Sites, to conduct demographic studies or audits, and to contact you for consumer research.
                                        </li>
                                    </ul>
                                </ul>
                                <h4 id="sharing">5   Sharing of personal data by Nestlé</h4>
                                <p>Nestlé does not share your personal data with any third party that intends to use it for direct marketing purposes, unless you have provided specific consent in relation to this. </p>
                                <p>Nestlé may share your personal data with third parties for other purposes, but only in the following circumstances:</p>
                                <h5>5.1 Affiliates</h5>
                                <p>Nestlé may provide your personal data to its affiliates or related companies for legitimate business purposes.</p>
                                <h5>5.2 Service providers </h5>
                                <p>Nestlé may engage service providers, agents or contractors to provide services on its behalf, including to administer Nestlé Sites and services available to you. These third parties may come to access or otherwise process your personal data in the course of providing these services.</p>
                                <p>Nestlé requires such third parties, who may be based outside the country from which you have accessed the Nestlé Site or service, to comply with all relevant data protection laws and security requirements in relation to your personal data, usually by way of a written agreement. </p>
                                <h5>5.3 Partners and joint promotions</h5>
                                <p>Nestlé may run a joint or co-sponsored program or promotion with another company and, as part of your involvement in the activity, collect and use your personal data. </p>

                                <p>Your personal data will only be shared with another company if you have opted in to receive information directly from that company. Nestlé encourages you to read the privacy notice of any such company before sharing personal data. If you do not want your personal data to be collected by or shared with a company other than Nestlé, you can always choose not to participate in such activity. If you do opt-in to communications from such a company, remember that you always have the right to opt-out and you would need to contact that company directly to do so.</p>
                                <h5>5.4 Legal requirements and business transfer </h5>
                                <p>Nestlé may disclose your personal data if it is required to do so by law or if, in Nestlé’s good faith judgment, such legal disclosure is reasonably necessary to comply with legal processes or respond to any claims.</p>
                                <p>In the event of a full or partial merger with, or acquisition of all or part of Nestlé by another company, the acquirer would have access to the information maintained by that Nestlé business, which could include personal data.</p>
                                <h4 id="your-right">6   Your rights</h4>
                                <h5>6.1 Right to opt-out of marketing communications</h5>
                                <p>You have the right to opt-out of receiving marketing communications about Nestlé and can do so by:</p>
                                <ul>
                                    <li>(a) following the instructions for opt-out in the relevant marketing communication;</li>
                                    <li>(b) if you have an account with Nestlé, you may have the option to change your opt-in/opt-out preferences under the relevant edit-account section of the account; or</li>
                                    <li>(c) contacting us.</li>
                                </ul>
                                <p>Please note that even if you opt-out from receiving marketing communications, you may still receive administrative communications from Nestlé, such as order confirmations and notifications about your account activities (e.g. account confirmations and password changes).</p>
                                <h5>6.2 Access and rectification</h5>
                                <p>You have a right to request access to your personal data. You may send us a request for access. If Nestlé cannot provide access to your personal data, it will provide you with the reasons why. </p>
                                <p>You also have the right to request that Nestlé correct any inaccuracies in your personal data. If you have an account with Nestlé for a Nestlé Site, this can usually be done through the appropriate "your account" or "your profile" section(s) on the Nestlé Site (if available). Otherwise, you can send us a request to rectify your data. </p>
                                <h4 id="cookies">7   Cookies and other tracking technologies</h4>
                                <h5>7.1.1   What are cookies?</h5>
                                <p>Cookies are small text files that are placed on your computer by websites that you visit. They are widely used in order to make websites work, or work more efficiently, as well as to provide information to the owners of the website. </p>
                                <h5>7.1.2   How and why do we use cookies? </h5>
                                <p>We use cookies to improve the use and functionality of Nestlé Sites and to gain a better understanding of how visitors use Nestlé Sites and the tools and services offered on them. Cookies help us tailor Nestlé Sites to your personal needs, to improve their user-friendliness, gain customer satisfaction feedback and to communicate to you elsewhere on the internet. </p>
                                <h5>7.1.3   What types of cookies may be used on Nestlé Sites?</h5>
                                <p>We may use the following types of cookies on Nestlé Sites: </p>
                                <p><strong>Session cookies</strong> - These are temporary cookie files which are erased when you close your browser. When you restart your browser and go back to the site that created that cookie, the website will treat you as a new visitor.</p>
                                <p><strong>Persistent cookies</strong> – These cookies stay on your browser until you delete them manually or until your browser deletes them based on the duration period set within the cookie. These cookies will recognize you as a return visitor.</p>
                                <p><strong>Necessary cookies</strong> - Necessary cookies are strictly necessary for the operation of a Nestlé Site. They enable you to navigate around the site and use our features. </p>
                                <p><strong>Cookies that send information to us</strong> - These are the cookies that we set on a Nestlé Site and they can only be read by that site. This is known as a "First Party" cookie.</p>
                                <p>We also place cookies on Nestlé advertisements which are placed on other websites owned by third parties (e.g. Facebook). We obtain information via those cookies when you click on or interact with the advertisement. In this situation Nestlé is placing a "Third Party" cookie. Nestlé may use the information obtained by these cookies to serve you with advertising that is relevant and of interest to you based on your past online behaviour.</p>
                                <p>Cookies that send information to other companies - These are cookies that are set on a Nestlé Site by our partner companies (e.g. Facebook or advertisers). They may use the data collected from these cookies to anonymously target advertising to you on other websites, based on your visit to the Nestlé Site. For example, if you use a social widget (e.g. the Facebook icon) on a Nestlé Site, it will record your "share" or "like". Facebook (as the company setting the cookie) will collect the data. </p>
                                <h5>7.1.4   Examples of cookies used on this Website</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>
                                                        <b>Cookie used
                                    type and category</b>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><b>Purpose</b></p>
                                                </td>
                                                <td>
                                                    <p><b>What data is collected?</b></p>
                                                </td>
                                                <td>
                                                    <p><b>Details</b></p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td valign="bottom">
                                                    <p><b>Performance Cookies</b></p>
                                                </td>
                                                <td valign="bottom"> </td>
                                                <td valign="bottom"> </td>
                                                <td rowspan="2" valign="top">
                                                    <p>Google Analytics;
                                                        <br><a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage">https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage</a></p>

                                                    <p>DoubleClick;
                                                        <br>
                                                        <a href="https://support.google.com/adsense/answer/2839090?hl=en">https://support.google.com/adsense/answer/2839090?hl=en</a></p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <p>Type: Analytics</p>

                                                    <p>Category:</p>

                                                    <p>Persistent</p>

                                                    <p>Session</p>

                                                    <p>Third party</p>

                                                </td>
                                                <td>
                                                    <p>Help us understand how visitors interact with our Website by providing information about the areas visited, time spent and any issues encountered, such as error messages. This helps us improve the performance of our websites. </p>
                                                </td>
                                                <td valign="bottom">
                                                    <p>All data is collected and aggregated anonymously.</p>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td valign="top">
                                                    <p><b>Social Sharing Cookies</b></p>

                                                    <p><b>(sometimes called "Third Party" cookies, or "Social Widgets") </b></p>
                                                </td>
                                                <td rowspan="2">
                                                    <p>Social sharing offered on the Website is run by third parties. These third parties may drop cookies on your computer when you use social sharing features on the Website, or if you are already logged into them. These cookies help improve your experience on the Website. </p>

                                                    <p>They allow you to share comments/ratings/pages/bookmarks and help to provide access to social networks and social online tools more easily.</p>
                                                </td>
                                                <td rowspan="2">
                                                    <p>These cookies may collect personal data that you have voluntarily disclosed, such as your username. </p>
                                                </td>
                                                <td rowspan="2">
                                                    <p>Facebook; </p>

                                                    <p><a href="https://www.ghostery.com/en/apps/facebook_social_plugins">https://www.ghostery.com/en/apps/facebook_social_plugins</a></p>

                                                    <p>Twitter;
                                                        <br><a href="https://www.ghostery.com/en/apps/twitter_button">https://www.ghostery.com/en/apps/twitter_button</a></p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td valign="top">
                                                    <p>Type:</p>

                                                    <p>Social Media/Sharing</p>

                                                    <p>Category:</p>

                                                    <p>Third Party</p>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td valign="bottom">
                                                    <p><b>Targeting/Advertising</b></p>
                                                </td>
                                                <td rowspan="3">
                                                    <p>These cookies are used to deliver content via targeted advertising that is relevant to you and your interests or to limit the number of times you see a particular advertisement. </p>

                                                    <p>These cookies also help us measure the effectiveness of advertising campaigns on Nestlé and Non-Nestlé websites. We may share this information with other parties, including our agencies.</p>
                                                </td>
                                                <td rowspan="3">
                                                    <p>These cookies track users via their IP address.</p>
                                                </td>
                                                <td valign="bottom"> </td>
                                            </tr>

                                            <tr>
                                                <td valign="bottom"> </td>
                                                <td valign="bottom"> </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <p>Type: </p>

                                                    <p>Cross Site Tracking</p>

                                                    <p>Category:</p>

                                                    <p>Persistent</p>

                                                    <p>Session</p>

                                                    <p>Third party</p>

                                                </td>
                                                <td>
                                                    <p>DoubleClick;
                                                        <br><a href="https://www.ghostery.com/en/apps/doubleclick">https://www.ghostery.com/en/apps/doubleclick</a></p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td valign="bottom">
                                                    <p><b>Miscellaneous</b></p>
                                                </td>
                                                <td rowspan="2" valign="bottom">
                                                    <p>Flash cookies may store your preferences such as volume control or high game score, or display content based upon what you view on the Website in order to personalize your visit. Our third party partners provide certain features on the Website such as promotions, and games and use Flash Cookies to collect and store your information.</p>
                                                </td>
                                                <td valign="bottom"> </td>
                                                <td valign="bottom"> </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <p>Type: Flash Cookies</p>

                                                    <p>Category:</p>

                                                    <p>Persistent</p>

                                                    <p>Third Party</p>

                                                </td>
                                                <td>
                                                    <p>These cookies may collect anonymous and personal data</p>
                                                </td>
                                                <td>
                                                    <p>To learn how to manage privacy and storage settings for Flash Cookies please click here: </p>

                                                    <p><a href="http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager.html#117118">http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager.html#117118</a></p>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <h5>7.1.5   Other similar technologies</h5>
                                <p>Nestlé Sites also make use of other tracking technologies including IP addresses, log files and web beacons, which also help us tailor Nestlé Sites to your personal needs. </p>
                                <ul>
                                    <li>IP addresses
                                        <br> An IP address is a number that is used by computers on the network to identify your computer every time you log on to the Internet. We may record IP Addresses for the following purposes: (i) troubleshoot technical concerns, (ii) maintain website safety and security (iii) better understand how our websites are utilized, and (iv) to better tailor content to your needs depending on the country you are in.
                                    </li>
                                    <li>Log Files
                                        <br> We (or a third party on our behalf) may collect information in the form of logs files that record website activity and gather statistics about a user’s browsing habits. These entries are generated anonymously, and help us gather (among other things) (i) a user’s browser type and operating system, (ii) information about a user’s session (such as the URL they came from, the date and time they visited our website, and which pages they've viewed on our website and for how long), and, (iii) other similar navigational or click-stream data (e.g. site traffic reporting and unique visitor counts).
                                    </li>
                                    <li>
                                        Web Beacons
                                        <br> We may use web beacons (or clear GIFs) on the Nestlé Sites. Web beacons (also known as "web bugs") are small strings of code that provide a method of delivering a graphic image on a web page for the purpose of transferring data back to us. We use web beacon information for a variety of purposes, including information as to how a user responds to email campaigns (e.g. the time the email is opened, where the user links to from the email), site traffic reporting, unique visitor counts, advertising and email auditing and reporting, and personalization.
                                    </li>
                                </ul>
                                <h5>7.1.6   Manage your cookies/preferences</h5>
                                <p>You should ensure that your computer setting reflects whether you are happy to accept cookies or not. You can set your browser to warn you before accepting cookies, or you can simply set it to refuse them. You do not need to have cookies on to use or navigate through many parts of Nestlé Sites although you may not have access to all the features on Nestlé Sites if you do so. See the 'help' button on your browser (e.g. Internet Explorer, Firefox) for how you can do this. Remember that if you use different computers in different locations, you will need to ensure that each browser is adjusted to suit your cookie preferences. </p>
                                <p>As a web beacon forms part of a webpage, it is not possible to ‘opt-out’ but you can render them ineffective by opting out of the cookies they set.</p>
                                <p>Additionally, where available, you can exercise your choice in allowing for cookies to be placed on your computer or to opt-out of cookies by visiting the following sites and selecting which company’s cookies you would like to opt out of: http://www.aboutads.info/choices/#completed or http://www.youronlinechoices.eu/ </p>
                                <h4 id="data-security">8   Data security and retention</h4>
                                <h5>8.1 Data security</h5>
                                <p>In order to keep your personal data secure, Nestlé has implemented a number of security measures, including: </p>
                                <ul>
                                    <li><strong>Secure operating environments -</strong> Nestlé stores your data in secure operating environments and only accessible to Nestlé employees, agents and contractors on a need-to-know basis. Nestlé also follows generally accepted industry standards in this respect. </li>
                                    <li><strong>Encryption for payment information -</strong> Nestlé uses industry-standard encryption to provide protection for sensitive financial information, such as credit card information sent over the Internet (e.g. when you make payments through Nestlé’s online stores).</li>
                                    <li><strong>Prior authentication for account access -</strong> Nestlé requires its registered consumers to verify their identity (e.g. login ID and password) before they can access or make changes to their account. This is aimed to prevent unauthorized accesses.</li>
                                </ul>
                                <p>Please note that these protections do not apply to personal data you choose to share in public areas such as on community websites.</p>
                                <h5>8.2 Retention</h5>
                                <p>Nestlé will only retain your personal data for as long as it is necessary for the stated purpose, taking into account also our need to answer queries or resolve problems, provide improved and new services, and comply with legal requirements under applicable laws.</p>
                                <p>This means that we may retain your personal data for a reasonable period after your last interaction with us. When the personal data that we collect is no longer required in this way, we destroy or delete it in a secure manner.</p>

                                <h4 id="contact-us">9    Contact us</h4>
                                <p>Nestlé acts as "data controller" for the personal data it processes in the framework of this Privacy Notice. If you have any questions or comments regarding this Privacy Notice or Nestlé’s personal data collection practices, please contact us by:</p>
                                <h4>10   Phone: <br>
                        For KSA and UAE toll free: 8008-971971/<br>
                        For other countries: +971-4-8100000<br>
                    </h4>
                                <h4>11  Email:<span> carecenter@nestle-family.com</span></h4>
                                <h4>12  Mail: Nestlé Middle East FZE, P.O. Box: 17327 Dubai, United Arab Emirates</h4>
                                <p><strong>Copyright</strong> © March 2016 Nestlé Middle East FZE</p>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

        
     <?php include("../footer.php"); ?>
        <script src="../assets/js/slick.min.js" type="application/javascript"></script>
        <!--<script src="../assets/js/custom.js" type="text/javascript"></script>-->
        <script src="../assets/js/jquery.event.move.js" type="application/javascript"></script>
        <script src="../assets/js/jquery.twentytwenty.js" type="application/javascript"></script>
        <script type="text/javascript" src="../assets/js/slick.min.js"></script>
       
</body>

</html>