<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive-tab.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.custom.css">
        
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="assets/css/contact-us.css?v1.4">
	<style>body{background: transparent;}</style>

    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/jquery-ui-1.10.4.js"></script>
    <script src="assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>

    <![endif]-->
</head>

    <body id="">

        <section class="contact-us">
            <div id="responsiveTabsDemo" class="left-panel">
                <ul class="tab-heading">
                    <li><a href="#tab-1">ارسلوا بريد الكتروني</a></li>
                    <li><a href="#tab-2">اتصلوا بنا</a></li>
                    <!--<li><a href="#tab-3">تحدثوا معنا</a></li>-->
                    <li><a href="#tab-4">الأسئلة الشائعة</a></li>
                </ul>

                <div id="tab-1" class="item">
                    <div class="left-content" >
                        <p><span>جميل أن نراكم هنا!</span> يبدو وكأنه لديك شيء للمشاركة معنا. الرجاء اختيار أحد الخيارات أدناه للوصول إلينا وسنكون أكثر من سعداء لمساعدتكم!</p>
                        <div class="email-section">
                            <h3>ما الذي أتى بك الى هنا؟</h3>
                            <div class="comment">
                                <img src="assets/images/contact-assets/comment.png">
                            </div>
                            <div class="advice">
                                <img src="assets/images/contact-assets/advice.png">
                            </div>
                            <div class="question">
                                <img src="assets/images/contact-assets/question.png">
                            </div>
                        </div>
                    </div>
                    <div class="left-content-comment-container" style="display: none;">
                        <div class="">
                                        <p>Fill in your personal information and indicate your comment, advice needed or question and we will reply with the shortest possible time.</p>
                                        <div class="popup-tabs">
                                            <div class="form-closer">
                                                <img src="assets/images/close-red.png">
                                            </div>
                                            <h3>شاركونا تعليقكم</h3>
                                            <div class="form-area">
                                                <form class="contact-us-form2" method="post">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الاسم الاول"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الاسم العائلي"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="البريد الالكتروني"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الجوال"></input>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 ">
                                                            <select class="form-area-select">
                                                                <option class="form-area-option" selected>اختر الدولة</option>
                                                                <option value="uae">الامارات العربية المتحدة</option>
                                                                <option value="bahrain">البحرين</option>
                                                                <option value="kuwait">الكويت</option>
                                                                <option value="oman">عمان</option>
                                                                <option value="saudi">Saudi Arabia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="message-row-1 col-md-7">
                                                            <textarea placeholder="رسالتكم هنا"></textarea>
                                                        </div>
                                                        
                                                        <div class="col-md-5">
                                                         <p>HOW SHALL WE GET IN TOUCH WITH YOU?</p>
                                                            <div class="email-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>البريد الالكتروني</p>
                                                            </div>
                                                            <div class="mobile-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>الجوال</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 attach-file-box">
                                                            <img src="assets/images/contact-assets/attach-file.png"><span class="attach-file">الملف المرفق (2 ميجا بايت)</span>
                                                        </div>
                                                        <div class="offers col-md-12">
                                                            <div class="offer-statement">
                                                                <input class="checkboxes" type="checkbox" name="offers"></input>
                                                                <p> أود أن أتلقى العروض من نستلة أو أن يتصل بي بالنيابة عن شركة نستلة بخصوص منتجات نستلة و عروضها أو بحوث المستهلك.</p>
                                                            </div>
                                                        </div>
                                                        <div class="message-row-2 col-md-12">
                                                           <div class="privacy-statement">
                                                                <input class="checkboxes" type="checkbox" name="privacy-check"></input><p> I accept the use and privacy of my data, more information on <a href="#" class="privacy-detailed">الخصوصية</a></p>

                                                            </div>
                                                            <button class="contact-us-send" type="sbumit" name="submit" value="SEND">ارسال</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                    <div class="left-content-advice-container" style="display: none;">
                                                <div class="">
                                        <p>Fill in your personal information and indicate your comment, advice needed or question and we will reply with the shortest possible time.</p>
                                        <div class="popup-tabs">
                                            <div class="form-closer">
                                                <img src="assets/images/close-red.png">
                                            </div>
                                            <h3>ما هي النصيحة التي تحتاجها منا؟</h3>
                                            <div class="form-area">
                                                <form class="contact-us-form2" method="post">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الاسم الاول"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الاسم العائلي"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="البريد الالكتروني"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الجوال"></input>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 ">
                                                            <select class="form-area-select">
                                                                <option class="form-area-option" selected>اختر الدولة</option>
                                                                <option value="uae">الامارات العربية المتحدة</option>
                                                                <option value="bahrain">البحرين</option>
                                                                <option value="kuwait">الكويت</option>
                                                                <option value="oman">عمان</option>
                                                                <option value="saudi">Saudi Arabia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="message-row-1 col-md-7">
                                                            <textarea placeholder="رسالتكم هنا"></textarea>
                                                        </div>
                                                        
                                                        <div class="col-md-5">
                                                         <p>HOW SHALL WE GET IN TOUCH WITH YOU?</p>
                                                            <div class="email-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>البريد الالكتروني</p>
                                                            </div>
                                                            <div class="mobile-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>الجوال</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 attach-file-box">
                                                            <img src="assets/images/contact-assets/attach-file.png"><span class="attach-file">الملف المرفق (2 ميجا بايت)</span>
                                                        </div>
                                                        <div class="offers col-md-12">
                                                            <div class="offer-statement">
                                                                <input class="checkboxes" type="checkbox" name="offers"></input>
                                                                <p> أود أن أتلقى العروض من نستلة أو أن يتصل بي بالنيابة عن شركة نستلة بخصوص منتجات نستلة و عروضها أو بحوث المستهلك.</p>
                                                            </div>
                                                        </div>
                                                        <div class="message-row-2 col-md-12">
                                                           <div class="privacy-statement">
                                                                <input class="checkboxes" type="checkbox" name="privacy-check"></input><p> I accept the use and privacy of my data, more information on <a href="#" class="privacy-detailed">الخصوصية</a></p>

                                                            </div>
                                                            <button class="contact-us-send" type="sbumit" name="submit" value="SEND">ارسال</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                    <div class="left-content-question-container" style="display: none;">
                                                <div class="">
                                        <p>Fill in your personal information and indicate your comment, advice needed or question and we will reply with the shortest possible time.</p>
                                        <div class="popup-tabs">
                                            <div class="form-closer">
                                                <img src="assets/images/close-red.png">
                                            </div>
                                            <h3>أخبرنا عن السؤال الخاص بك</h3>
                                            <div class="form-area">
                                                <form class="contact-us-form2" method="post">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الاسم الاول"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الاسم العائلي"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="البريد الالكتروني"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="الجوال"></input>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 ">
                                                            <select class="form-area-select">
                                                                <option class="form-area-option" selected>اختر الدولة</option>
                                                                <option value="uae">الامارات العربية المتحدة</option>
                                                                <option value="bahrain">البحرين</option>
                                                                <option value="kuwait">الكويت</option>
                                                                <option value="oman">عمان</option>
                                                                <option value="saudi">Saudi Arabia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="message-row-1 col-md-7">
                                                            <textarea placeholder="رسالتكم هنا"></textarea>
                                                        </div>
                                                        
                                                        <div class="col-md-5">
                                                         <p>HOW SHALL WE GET IN TOUCH WITH YOU?</p>
                                                            <div class="email-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>البريد الالكتروني</p>
                                                            </div>
                                                            <div class="mobile-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>الجوال</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 attach-file-box">
                                                            <img src="assets/images/contact-assets/attach-file.png"><span class="attach-file">الملف المرفق (2 ميجا بايت)</span>
                                                        </div>
                                                        <div class="offers col-md-12">
                                                            <div class="offer-statement">
                                                                <input class="checkboxes" type="checkbox" name="offers"></input>
                                                                <p>أود أن أتلقى العروض من نستلة أو أن يتصل بي بالنيابة عن شركة نستلة بخصوص منتجات نستلة و عروضها أو بحوث المستهلك.</p>
                                                            </div>
                                                        </div>
                                                        <div class="message-row-2 col-md-12">
                                                           <div class="privacy-statement">
                                                                <input class="checkboxes" type="checkbox" name="privacy-check"></input><p> I accept the use and privacy of my data, more information on <a href="#" class="privacy-detailed">الخصوصية</a></p>

                                                            </div>
                                                            <button class="contact-us-send" type="sbumit" name="submit" value="SEND">ارسال</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
                <div id="tab-2" class="item">
                    <div class="tabs-common-inner-section">
                        <h3>رقم مركز نستلة لرعاية
العملاء في جميع الدول</h3>
                        <div class="">
                            <ul class="contact-countries-list">
                                <li class="call-us-item">
                                    <p>المملكة العربية السعودية</p>
                                    <img src="assets/images/contact-us/ksa.png">
                                    <p class="calling-number">8008971971</p>
                                    <span>الرقم المجاني</span>
                                </li>
                                <li class="call-us-item">
                                    <p>الامارات العربية المتحدة</p>
                                    <img src="assets/images/contact-us/uae.png">
                                    <p class="calling-number">8008971971</p>
                                    <span>الرقم المجاني</span>
                                <li class="call-us-item">
                                    <p>البحرين</p>
                                    <img src="assets/images/contact-us/bahrain.png">
                                    <p class="calling-number">+97444587688</p>
                                    <span>الرقم المجاني</span>
                                </li>
                                <li class="call-us-item">
                                    <p>الكويت</p>
                                    <img src="assets/images/contact-us/kuwait.png">
                                    <p class="calling-number">+96522286847</p>
                                    <span>الرقم المجاني</span>
                                </li>
                                <li class="call-us-item">
                                    <p>عمان</p>
                                    <img src="assets/images/contact-us/oman.png">
                                    <p class="calling-number">+96822033446</p>
                                    <span>الرقم المجاني</span>
                                </li>
                                <li class="call-us-item">
                                    <p>الأردن</p>
                                    <img src="assets/images/contact-us/jordan.png">
                                    <p class="calling-number">+96265902998</p>
                                    <span>الرقم المجاني</span>
                                </li>
                                <li class="call-us-item">
                                    <p>لبنان</p>
                                    <img src="assets/images/contact-us/lebanon.png">
                                    <p class="calling-number">+9614548595</p>
                                    <span>الرقم المجاني</span>
                                </li>
                                <li class="call-us-item">
                                    <p>قطر</p>
                                    <img src="assets/images/contact-us/qatar.png">
                                    <p class="calling-number">+97444587688</p>
                                    <span>الرقم المجاني</span>
                                </li>
                            </ul>
                            <h3 class="text-center">دول أخرى</h3>
                            <p class="text-center"><strong>0097148100000</strong></p>
                        </div>
                    </div>
                </div>
                <!--<div id="tab-3" class="item">
                    <div class="tabs-common-inner-section">
                        <h3>المساعدة عبر الشات</h3>
                        <p>Start a chat session with Nestlé Chat Support now.</p>
                        <div class="form-area row">
                            <form class="contact-us-form1" method="post">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input name="" type="text" placeholder="الاسم الاول"></input>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input name="" type="text" placeholder="الاسم العائلي"></input>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input name="" type="text" placeholder="البريد الالكتروني"></input>
                                </div>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <textarea placeholder="رسالتكم هنا"></textarea>
                                </div>
                                <div class="col-md-5 col-sm-6  col-xs-12">
                                    <div class="chat-now-container">
                                        <div class="chat-now text-center">
                                            <img src="assets/images/chat.png" >
                                            <h4>تحدث الآن</h4>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->
                <div id="tab-4" class="item">
                    <div class="tabs-common-inner-section">
                        <h3>هل تبحث عن المزيد؟</h3>
                        <p>هنا ستجد إجابات على الأسئلة الأكثر شيوعاً.</p>
                        <div class="faq-inner">
                            <div class="accordions">
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="accordion">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </h3>
                                    <div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                    </div>
                                </div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-content">
                <div class="contact-us-popup-inner-right">
                    <div class="new-letter">
                        <input type="text" name="user" placeholder="اشترك بنشرتنا الإخبارية"></input>
                        <button type="submit"><i class="fa fa-caret-right"></i></button>
                    </div>
                    <!--<div class="help-line">
                        <h3>خط المساعدة</h3>
                        <img src="assets/images/contact-assets/call-icon.gif" alt="">
                        <p>ممثلي خدمة المستهلك متوفرون لمساعدتك 24/7.</p>
                        <h3 class="helpline-number">1 800 123-4567</h3>
                    </div>-->
                    <div class="address-box">
                        <p class="company-name"> نستله الشرق الأوسط<br>
                        ص.ب. 17327<br>
                        الطابق الثالث - مبنى مطار دبي ورلد سنترال الرئيسي<br>
                        بالقرب من مطار آل مكتوم الدولي الجديد<br>
                        مدينة دبي اللوجستية، جبل علي - دبي</p>
                    </div>
                    <div class="social">
                        <h4>تابعونا</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/kitkat.arabia" target="_blank"><img src="assets/images/contact-assets/facebook.png"></a></li>
                            <li><a href="https://twitter.com/kitkatarabia" target="_blank"><img src="assets/images/contact-assets/twitter.png"></a></li>
                            <li><a href="https://www.youtube.com/watch?v=of4JWg_hf9s"><img src="assets/images/contact-assets/youtube.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </section>
        <script src="assets/js/jquery.responsiveTabs.js"></script>
        <script>
            $('#responsiveTabsDemo').responsiveTabs({
                startCollapsed: 'accordion'
            });
            $(function() {
                $( ".accordions" ).accordion();
                $(".accordion").accordion({ header: "h3", collapsible: true, active: false });
              });
             $('.comment').click(function(){
                $('.left-content-comment-container').fadeIn(500);
                $('.left-content').fadeOut();
            });
             $('.advice').click(function(){
                $('.left-content-advice-container').fadeIn(500);
                $('.left-content').fadeOut();
            });
             $('.question').click(function(){
                $('.left-content-question-container').fadeIn(500);
                $('.left-content').fadeOut();
            });
            $('.form-closer').click(function(){
                $('.left-content-comment-container').fadeOut();
                $('.left-content').fadeIn(500);
            });
            $('.form-closer').click(function(){
                $('.left-content-question-container').fadeOut();
                $('.left-content').fadeIn(500);
            });
            $('.form-closer').click(function(){
                $('.left-content-advice-container').fadeOut();
                $('.left-content').fadeIn(500);
            });
        </script>
    </body>
</html>