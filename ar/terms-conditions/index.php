<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Products - KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="../assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom-new.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/footer-styles.css">
	<style>.slick-next{width:63px;height:63px;right:50px;background:url('../assets/images/r-arrow.png') no-repeat;}.slick-prev{width:63px;height:63px;left:50px;background:url('../assets/images/l-arrow.png') no-repeat;    z-index: 9;}.slick-prev:before,.slick-next:before{content:''}body{overflow-x:hidden;}</style>
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="../assets/js/ie8.js"></script>

    <![endif]-->
<!--     <script>
        if (top != self) {
            top.location = self.location;
        }
        if (top.location != self.location) {
            top.location = self.location.href;
        }
        (function (window) {
            if (window.location !== window.top.location) {
                window.top.location = window.location;
            }
        })(this);
    </script> -->

</head>
<body id="products">
    <header class="kitkat-header">
        <?php 
            include("../config.php");
            include("../header.php");        
        ?>
    </header>

    <main class="page-content">
        <section class="tools-content">
            <div class="container">
                <div class="row">
                    <div class="main-content">
                        <div class="page-style frame">
                            <section>
                                <h2>Terms and Conditions of Use</h2>
                                <p>
                                    Welcome to our Website! We hope that you will enjoy your online experience. Nestlé Middle East FZE (“Nestlé”) is committed to maintaining trust with users of our Website. The terms below govern your use of the Website.
                                </p>
                                <h4>1.    Acceptable use</h4>
                                <p>
                                    Please feel free to explore our Website and, where available, contribute material to it, such as questions, posts, ratings and reviews and multimedia content (e.g. pictures, videos).
                                </p>
                                <p> However, use of the Website and materials posted to it should not be illegal or offensive in any way. You should be mindful not to:</p>
                                <ul>
                                    <li>(a) breach another person’s right to privacy;</li>
                                    <li>(b) infringe any intellectual property rights;</li>
                                    <li>(c) make statements that are defamatory (including towards Nestlé), relate to pornography, are of a racist or xenophobic nature, promote hatred or incite to violence or disorder; </li>
                                    <li>(d) upload files that contain viruses or may lead to security issues; or </li>
                                    <li>(e) otherwise jeopardize the integrity of the Website.</li>
                                </ul>
                                <p>Please note that Nestlé may remove any content from the Website that it believes maybe illegal or offensive. Nestlé reserves the right, but not the obligation, to monitor or review the content you have submitted as well as to edit, remove or refuse to post any content that Nestlé deems in its sole discretion to be inappropriate. </p>
                                <h4>2.  Data protection</h4>
                                <p>Our Privacy Notice applies to any personal data or material shared on this Website. Find out more
                                    <a href="/en/consumer-privacy"></a>here </p>
                                <h4>3.  Intellectual property</h4>
                                <h5>3.1. Content provided by Nestle </h5>
                                <p>All intellectual property rights, including copyright and trademarks, in materials published by or on behalf of Nestlé on the Website (e.g. text and images) are owned by Nestlé or its licensors. </p>
                                <p>You may reproduce extracts of the Website for your own private use (i.e. non-commercial use) provided that you keep intact and respect all intellectual property rights, including any copyright notice which may appear on such content (e.g. © 2016 Nestlé).</p>
                                <h5>3.2. Content provided by You</h5>
                                <p>You represent to Nestlé that you are either the author of the content that you contribute to this Website, or that you have the rights (ie: have been given permission by the rights holder) and are able to contribute such content (e.g. pictures, videos, music) to the Website.</p>
                                <p>You agree that such content will be treated as non-confidential and you grant Nestlé a royalty free, perpetual, worldwide licence to use (including to disclose, reproduce, transmit, publish, adapt or broadcast) the content you provide for purposes relating to its business.</p>
                                <p>Nestlé may use the content for promotional, advertising and other purposes and you will not be entitled to any compensation for such use.</p>
                                <p>Please note that Nestlé is free to decide whether or not to use this content and that Nestlé may already have developed similar content or have obtained such content from other sources, in which case all intellectual property rights in this content remains with Nestlé and its licensors.</p>
                                <h5>3.3.    Rules governing consumer ratings and review service </h5>
                                <p>In addition to these terms the following additional rules apply for ratings and review services. In order to submit any content to any rating and review service available on this Website (such as text, photo, video, likeness or other material or information), you must be 21 years of age or older, or have received the consent of the person/s exercising parental authority. If you have received prior payment or promise of payment in return for your proposed submission; or if you have received an incentive such as free product, discounts, gifts, sweepstakes entries you will disclose it in your submission. If you are a Nestlé employee or work for a company or agency hired by Nestlé, you have disclosed that relationship. </p>
                                <p>All content that you submit is accurate and is based upon your actual experience with the product being reviewed. It shall not include any information that references other websites, addresses, email addresses, contact information or phone numbers. You are responsible for the content of your submission, not Nestlé.</p>
                                <h4>4.  Liability</h4>
                                <p>While Nestlé uses all reasonable efforts to ensure the accuracy of materials on our Website and to avoid disruptions, we are not responsible for inaccurate information, disruptions, discontinuance or other events which may cause you damage, either direct (e.g. computer failure) or indirect (e.g. loss of profit). Any reliance upon materials on this Website shall be at your own risk.</p>
                                <p>This Website may contain links to websites outside of Nestlé. Nestlé has no control over such third party websites, does not necessarily endorse them and accepts no responsibility for them, including as to their content, accuracy or function. As a result, we invite you to carefully review the legal notices of such third party websites, including keeping yourself informed of any changes to them.</p>
                                <p>You may operate a third party website and wish to link to this Website. In this case, Nestlé does not object to such linking provided that you do not suggest in any way that you are affiliated with or endorsed by Nestlé. You must not use “framing” or similar practices, and must ensure that the link to the Website opens in a new window.</p>
                                <h4>5.  Contact us</h4>
                                <p>This Website is operated by Nestlé Middle East FZE, P.O. Box: 17327 Dubai, United Arab Emirates.</p>
                                <p>If you have any question or comment regarding the Website, please feel free to contact us by (i) email at carecenter@nestle-family.com or (ii) phone at For KSA and UAE toll free: 8008-971971 For other countries: +971-4-8100000
                                </p>
                                <h4>6.  Changes</h4>
                                <p>Nestlé reserves the right to make changes to these terms of use. Please refer to this page from time to time to review these terms of use and any new information.</p>

                                <h4>7.  Governing law and jurisdiction</h4>
                                <p>The Website is intended for users from United Arab Emirates, Saudi Arabia, Oman, Bahrain, Kuwait, Qatar, Lebanon, Jordan, Palestine, Syria, Yemen, and Iraq only. Nestlé makes no representation that the products and the content of this Website are appropriate or available in locations other than United Arab Emirates, Saudi Arabia, Oman, Bahrain, Kuwait, Qatar, Lebanon, Jordan, Palestine, Syria, Yemen, and Iraq.</p>
                                <p>You and Nestlé agree that any claim or dispute relating to the Website shall be governed by the law of the United Arab Emirates and brought before the courts of Dubai, UAE.</p>

                                <p>Copyright © March 2016 [Nestlé Middle East FZE].</p>


                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

        
     <?php include("../footer.php"); ?>
        <script src="../assets/js/slick.min.js" type="application/javascript"></script>
        <!--<script src="../assets/js/custom.js" type="text/javascript"></script>-->
        <script src="../assets/js/jquery.event.move.js" type="application/javascript"></script>
        <script src="../assets/js/jquery.twentytwenty.js" type="application/javascript"></script>
        <script type="text/javascript" src="../assets/js/slick.min.js"></script>
       
</body>

</html>