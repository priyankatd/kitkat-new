<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <!--<meta http-equiv="X-Frame-Options" content="SAMEORIGIN">-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/mm.css">
    <!--<link rel="stylesheet" type="text/css" href="../../assets/css/mini-moments.css">-->

    <script src="../../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->
</head>

<body id="home">
    <div class="nutri-popup-overlay"></div>
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../../config.php");
            include("../../../header.php");
        ?>
    </header>

    <main class="clearfix page-content mm-slider">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper dreamy-breaker-bg" data-color="#000">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <!-- <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name dreamy-breaker-color"><img src="../../../assets/images/products/Cookies-&amp;-Cream-text.png" alt="Cookies &amp; Cream" class="img-responsive img-center"></h3>
                  <p class="product-text db-p-color">المزيج الرائع من قطع الكوكيز والكريمة البيضاء الغنيّة .</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#cream-ninfo" class="nutritional-info"><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> ميني مومنتس</h3>
                  <div class="videoposter">
                    <a href="https://www.youtube.com/embed/of4JWg_hf9s?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video1" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper live-breaker-bg" data-color="grey">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <!-- <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/live-breaker/live-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/live-breaker/live-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/live-breaker/live-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name live-breaker-color"><img src="../../../assets/images/products/mocha-text.png" alt="Mocha" class="img-responsive img-center"></h3>
                  <p class="product-text db-p-color">
                   مزيج الموكا الأصيل من البن المحمص و شوكولاته الحليب الغنية بالكريمة.
                  </p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#macha-ninfo" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> ميني مومنتس</h3>
                  <div class="videoposter">
                    <a id="" href="https://www.youtube.com/embed/of4JWg_hf9s?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video2" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                  </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
              <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper rr-breaker-bg" data-color="red">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                 <!--  <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/rr-breaker/rr-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/rr-breaker/rr-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/rr-breaker/rr-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name db-p-color"><img src="../../../assets/images/products/hazelnut-text.png" alt="Hazelnut" class="img-responsive img-center"></h3>
                  <p class="product-text db-p-color">تجربة رائعة بنكهة البندق المقرمش والغنية بالكريمة.</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#hazelnut-ninfo" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> ميني مومنتس</h3>
                  <div class="videoposter">
                     <a id="" href="https://www.youtube.com/embed/of4JWg_hf9s?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video3" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                  </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper thrill-breaker-bg">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <!-- <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/thrill-breaker/thrill-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/thrill-breaker/thrill-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/thrill-breaker/thrill-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name thrill-breaker-color"><img src="../../../assets/images/products/milk-chocolate-text.png" alt="Milk Chocolate" class="img-responsive img-center"></h3>
                  <p class="product-text db-p-color">الأصلية برقائق الويفر الهش وشوكولاته الحليب الغنية بالكريمة.</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#milk-ninfo" class="nutritional-info"><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> ميني مومنتس</h3>
                  <div class="videoposter">
                  <a id="" href="https://www.youtube.com/embed/of4JWg_hf9s?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video4" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                  </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper love-breaker-bg">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <!-- <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/love-breaker/love-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/love-breaker/love-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/love-breaker/love-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name love-breaker-color"><img src="../../../assets/images/products/caramel-text.png" alt="Caramel" class="img-responsive img-center"></h3>
                  <p class="product-text db-p-color">مغرية مع طبقة خفيفة مخملية من الكرميل الذهبية.</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#caramel-ninfo" class="nutritional-info"><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> ميني مومنتس</h3> 
                  <div class="videoposter">              
                    <a id="" href="https://www.youtube.com/embed/f4JWg_hf9s?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video5" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                  </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->
    </main>

    <!-- Nutritional info modals-->
    <div class="nutri-popup-outer" id="macha-ninfo">
            <div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
            <h2>ميني مومنتس</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <thead>
                        <tr>
                          <th>المعلومات الغذائية</th>
                          <th>بالمائة غرام</th>
                          <th>لكل حلوى</th>
                          <th></th>
                          <th>%GDA</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>طاقة (كيلوكالوري)</td>
                          <td>500</td>
                          <td>80</td>
                          <td>2000</td>
                          <td>4</td>
                        </tr>
                        <tr>
                          <td>بروتين (غ)</td>
                          <td>6.5</td>
                          <td>1</td>
                          <td>50</td>
                          <td>2</td>
                        </tr>
                        <tr>
                          <td>كاربوهيدرات (غ)</td>
                          <td>60.6</td>
                          <td>9.7</td>
                          <td>260</td>
                          <td>4</td>
                        </tr>
                        <tr>
                          <td>منها سكريات (غ)</td>
                          <td>49</td>
                          <td>7.8</td>
                          <td>90</td>
                          <td>9</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>40.3</td>
                          <td>6.4</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>0</td>
                          <td>0</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>دسم (غ)</td>
                          <td>25.2</td>
                          <td>4</td>
                          <td>70</td>
                          <td>6</td>
                        </tr>
                        <tr>
                          <td>منها مشبع (غ)</td>
                          <td>15.8</td>
                          <td>2.7</td>
                          <td>20</td>
                          <td>14</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>0.2</td>
                          <td>0</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>2.4</td>
                          <td>0.4</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>صوديوم (غ)</td>
                          <td>0.08</td>
                          <td>0.01</td>
                          <td>2.4</td>
                          <td>1</td>
                        </tr>

                      </tbody>
                    </table>
                    <p>إرشادات الاستهلاك اليومي للبالغين (2000 كيلوكالوري) يجب تعديل الحصص للأطفال وفقاً لمختلف الأعمار.</p>
                </div>
                
            </div>
        </div>


    <?php include("../../../footer.php"); ?>
        <script src="../../../assets/js/custom.js" type="application/javascript"></script>
        <script src="../../../assets/js/owl.carousel.min.js" type="application/javascript"></script>
        <script src="../../../assets/js/ilightbox.min.js" type="application/javascript"></script>
        <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".mm-slider .owl-next").empty();
                $(".mm-slider .owl-prev").empty();
                $('.minimoments-video1').iLightBox();
                $('.minimoments-video2').iLightBox();
                $('.minimoments-video3').iLightBox();
                $('.minimoments-video4').iLightBox();
                $('.minimoments-video5').iLightBox();
              /*  if( currentIndex == 0 ) {
                  $(".owl-controls").addClass("mm-slide-one");
                } else if (currentIndex == 1) {
                  $(".owl-controls").addClass("mm-slide-two");
                } else if (currentIndex == 2) {
                }*/
                $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                    console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                    $(this).children('div').find('h1').html(main_title);
                    $(this).children('div').find('img').attr('src',main_img);
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
        </script>
        <script>
        var mmowl = $('.mm-slider');
        var mmowl_settings = {
          margin: 0,rtl:true,
          dots: true,
          nav:true,
          //loop: true,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 1
            },
            1000: {
              items: 1
            }
          }
        }
        mmowl.owlCarousel(mmowl_settings);

        mmowl.on('changed.owl.carousel', function(event) {
           var currentItemOwl = event.item.index + 1;
           //alert(currentItemOwl);
           if(currentItemOwl == 1) {
             $(".red-bg-bottom").css("background","#6fbae6");
           } else if (currentItemOwl == 2) {
             $(".red-bg-bottom").css("background","#cb8f5c");
           } else if (currentItemOwl == 3) {
             $(".red-bg-bottom").css("background","#4e9551");
           } else if (currentItemOwl == 4) {
             $(".red-bg-bottom").css("background","#f03439");
           } else if(currentItemOwl == 5) {
             $(".red-bg-bottom").css("background","#fac062");
           }
        });
		
		if( $(window).width() < 1200 )  {
          mmowl.on('changed.owl.carousel', function(event) {
           var currentItemOwl = event.item.index + 1;
           if(currentItemOwl == 1) {
             $(".mm-slider .owl-controls").css("background-color","#6fbae6");
           } else if (currentItemOwl == 2) {
             $(".mm-slider .owl-controls").css("background-color","#cb8f5c");
           } else if (currentItemOwl == 3) {
             $(".mm-slider .owl-controls").css("background-color","#4e9551");
           } else if (currentItemOwl == 4) {
             $(".mm-slider .owl-controls").css("background-color","#f03439");
           } else if(currentItemOwl == 5) {
             $(".mm-slider .owl-controls").css("background-color","#fac062");
           }
        });
        }

        $(window).resize(function(){
            if( $(window).width() < 1200 )  {
          mmowl.on('changed.owl.carousel', function(event) {
           var currentItemOwl = event.item.index + 1;
           if(currentItemOwl == 1) {
             $(".mm-slider .owl-controls").css("background-color","#6fbae6");
           } else if (currentItemOwl == 2) {
             $(".mm-slider .owl-controls").css("background-color","#cb8f5c");
           } else if (currentItemOwl == 3) {
             $(".mm-slider .owl-controls").css("background-color","#4e9551");
           } else if (currentItemOwl == 4) {
             $(".mm-slider .owl-controls").css("background-color","#f03439");
           } else if(currentItemOwl == 5) {
             $(".mm-slider .owl-controls").css("background-color","#fac062");
           }
        });
        }
        });

       </script>
</body>

</html>
