<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/product-detail.css">
    <script src="../../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->

</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../../config.php");
            include("../../../header.php");
        ?>
    </header>

    <main class="clearfix page-content">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper">
        <div class="red-bg-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-up-arrow">
                    <a href="../min-moments/"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-down-arrow">
                    <a href="../four-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
            
				<div class="main-product">
                    <div>
                        <h1 class="main-title text-center">نستله كيت كات<br>إصبعين شوكولاته <br>20.5غ</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/kitkat-2-finger-milk-c-bar-20.5g.png">
                    </div>
                </div>
				
				<!-- for mobile -->
         <div class="clearfix main-container">   
        <div class="main-product one">
                    <div>
                        <h1 class="main-title text-center">نستله كيت كات<br>إصبعين شوكولاته <br>20.5غ</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-2-finger-milk-c-bar-20.5g.png">
                    </div>
                </div>
				<div class="main-product two">
                    <div>
                        <h1 class="main-title text-center">نستله كيت كات<br>إصبعين شوكولاته <br>20.5غ، علبة من 36</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-2-finger-multipack.png">
                    </div>
                </div>
				
				<div class="main-product three">
                    <div>
                        <h1 class="main-title text-center">نستله كيت كات<br>إصبعين شوكولاته <br>20.5غ، عبوة من 12</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-pack-of-12.png">
                    </div>
                </div>
				
				<!-- for mobile -->
				
			</div>		
                <div class="thumbnail-product" id="thumb-1">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-2-finger-multipack.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">نستله كيت كات<br>إصبعين شوكولاته <br>20.5غ، علبة من 36</h1>
                    </div>
                </div>
                <div class="thumbnail-product" id="thumb-2">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-pack-of-12.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">نستله كيت كات<br>إصبعين شوكولاته <br>20.5غ، عبوة من 12</h1>
                    </div>
                </div>
			<!-- 
                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
 -->

            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/product-detail/right-panel-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name">كيت كات 2 إصبع</h3>
                  <p class="product-text">نستله كيت كات 2 إصبع هو الوجبة الكاملة عند حلول وقت الاستراحة. يمكن الاستمتاع بها مع العائلة والأصدقاء في المنزل، أو مع الزملاء أثناء النهار. وهي الوجبة الخفيفة المثالية التي يمكن الاستمتاع بها كجزء من نظام غذائي متوازن حيث تحتوي على 105 سعرة حرارية فقط لكل حصة غذائية (1 حصة = 1 لوح مغلف). عليك فقط فتح الغلاف، وفصل الإصبع، اقسمه إلى نصفين واستمتع بمذاق أصابع الويفراللذيذة والمغطاة بشوكولاتة الحليب السلسة.</p>
                  <hr>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="1213"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#nutritionalpopup" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img class="img-responsive" src="../../../assets/images/product-range/product-detail/2fingers/2-finger-inner.png">
                </div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->

    </main>
        <div class="nutri-popup-outer" id="nutritionalpopup">
    		<div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
    		<h2>كيت كات 2 إصبع</h2>
            <div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
    				<table border="0" cellpadding="0" cellspacing="0">
					  <thead>
					    <tr>      
					      <th>المعلومات الغذائية</th>
					      <th>بالقطعة (غ 17،7)</th>
					      <th>%GDA</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td>طاقة (كيلوكالوري)</td>
					      <td>91</td>
					      <td>5</td>
					   </tr>
					    <tr>
					      <td>كاربوهيدرات (غ)</td>
					      <td>1.2</td>
					      <td>2</td>
					    </tr>
					    <tr>
					      <td>منها سكريات (غ)</td>
					      <td>10.9</td>
					      <td>4</td>
					    </tr>
					    <tr>
					      <td>منها سكريات (غ)</td>
					      <td>8.9</td>
					      <td>10</td>
					    </tr>
					    <tr>
					      <td></td>
					      <td>7.5</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td></td>
					      <td>0</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td>دسم (غ)</td>
					      <td>4.6</td>
					      <td>7</td>
					    </tr>
					    <tr>
					      <td>منها مشبع (غ)</td>
					      <td>2.8</td>
					      <td>14</td>
					    </tr>
					    <tr>
					      <td>صوديوم (غ)</td>
					      <td>0.02</td>
					      <td>1</td>
					    </tr>
					    
					  </tbody>
					</table>
                        <p>إرشادات الاستهلاك اليومي للبالغين (2000 كيلوكالوري) يجب تعديل الحصص للأطفال وفقاً لمختلف الأعمار.</p>
    			</div>
    			
    		</div>
    	</div>
    <div class="nutri-popup-overlay"></div>
    
    <?php include("../../../footer.php"); ?>
        <!--<script src="../../assets/js/custom.js" type="application/javascript"></script>-->
        <script type="text/javascript" src="../../../assets/js/slick.min.js"></script>
         <script src="../../../assets/js/ilightbox.min.js" type="application/javascript"></script>
         <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
         <script src="../../../assets/js/product-detail.js" type="application/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
				
	
				if($(window).width() < 768 ){
					$('.main-container').slick({
						infinite: false,
						nav: false,dots:true,
						easing: 'linear',rtl:true,
						draggable: true,
						slidesToShow: 1
                  });
				}
				
				//+++++++++++product page nutrient table popup++++++
    $(".nutritional-info").on("click", function () {
        var nutritionalPopup = $(this).attr("href");
        $(".nutri-popup-overlay").show();
        $(nutritionalPopup).show();
        $(nutritionalPopup).position({
            my: "center",
            at: "center",
            of: window
        });
        return false;
    });

    $(".nutri-popup-trigger").click(function (event) {
        $(this).toggleClass("active");
        $(this).parents(".mobile-nutri-pop-up").children(".nutri-info").slideToggle();
        var owlactiveHeight = $(this).parents(".owl-item.active").height();
    });
    $(".nutri-closer, .nutri-popup-overlay").click(function (event) {
        $(".nutri-popup-overlay, .nutri-popup-outer").hide();
    });
    
              
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                    //console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                   // var lastSlash = main_img.lastIndexOf("/");
                   var lastSlash = main_img.split("/").pop();
                   
                   var text = main_img.substring(0, main_img.lastIndexOf('/'));
                   
                   main_img = text+'/thumbnails/'+lastSlash; 
                   console.log(" last slash "+lastSlash);
                   $(this).children('div').find('h1').html(main_title);
                                  $(this).children('div').find('img').attr('src',main_img);
     
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
			
			  $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
        </script>
</body>

</html>
