<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Products - KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="../assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom-new.css">
	<style>.slick-prev{width:63px;height:63px;right:50px;background:url('../assets/images/r-arrow.png') no-repeat;}.slick-next{width:63px;height:63px;left:50px;background:url('../assets/images/l-arrow.png') no-repeat;z-index: 9;}.slick-next:before,.slick-prev:before{content:''}body{overflow-x:hidden;}</style>
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="../assets/js/ie8.js"></script>

    <![endif]-->
<!--     <script>
        if (top != self) {
            top.location = self.location;
        }
        if (top.location != self.location) {
            top.location = self.location.href;
        }
        (function (window) {
            if (window.location !== window.top.location) {
                window.top.location = window.location;
            }
        })(this);
    </script> -->

</head>
<body id="products">
    <header class="kitkat-header">
        <?php 
            include("../config.php");
            include("../header.php");        
        ?>
    </header>
        <main class="clearfix page-content">
            <div class="products">
                <div class="container-fluid row">
                    <div class="product-wrap">
                        <div class="text-center">
                            <h1>منتجات كيت كات</h1>
                        </div>
                        <!-- <div class="left-custom-nav pull-left"></div>  -->
                        <div class="product-list">
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
										 <img src="../assets/images/product-range/kitkat-mini-moments.png">
                                        <img src="../assets/images/product-range/inner/kitkat-mini-moments.png">
                                       
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                        <h4>كيت كات ميني مومنتس</h4>
                                           <a href="#" class="p-buynow" data-buynowid="5096"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/min-moments" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center buy-mini">5 نكهات فريدة من نوعها في كيس واحد.</p>
                                        </div>
                                    </div>
                                    <div class="tear-container tear-large"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
									<img src="../assets/images/product-range/kitkat-senses.png" >
                                    <img src="../assets/images/product-range/inner/kitkat-senses.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات سنسز بالبندق إصبعين</h4>
                                            <a href="#" class="p-buynow" data-buynowid="5097"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/kitkat-sense-hazelnut" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">تجربة رائعة بنكهة البندق المقرمش والغنية بالكريمة</p>
                                        </div>
                                    </div>
									<div class="tear-container tear-medium"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
										<img src="../assets/images/product-range/kitkat-2-fingers.png">
                                        <img src="../assets/images/product-range/inner/kitkat-2-finger.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات إصبعين</h4>
                                            <a href="#" class="p-buynow" data-buynowid="4506"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/two-finger" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">لوح شوكولاتة لذيذ يتكون من 2 أصابع من الويفر الهش مغطاة بشوكولاتة الحليب السلسة.</p>
                                        </div>
                                    </div>   
										 <div class="tear-container tear-medium"></div>	
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
										<img src="../assets/images/product-range/kitkat-4-fingers.png">
                                        <img src="../assets/images/product-range/inner/kitkat-4-finger.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات لوح أربع أصابع</h4>
                                             <a href="#" class="p-buynow" data-buynowid="4509"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/four-finger" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">شوكولاتة لذيذة مكونة من 4 أصابع من الويفر الهشة المغطاة بشوكولاتة الحليب السلسة.</p>
                                        </div>
                                    </div>  
										 <div class="tear-container tear-medium"></div>	
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">      
										<img src="../assets/images/product-range/kitakt-4-finger-dark-chocolate.png">      <img src="../assets/images/product-range/inner/kitkat-4-finger-dark-chocolate.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات بأربع أصابع بالشوكولاته الداكنة </h4>
                                            <a href="#" class="p-buynow" data-buynowid="4508"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/four-finger-dark" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">شوكولاتة لذيذة مكونة من 4 أصابع من الويفر الهشة المغطاة بشوكولاتة سوداء</p>
                                        </div>
                                    </div>
									 <div class="tear-container tear-medium"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical"> 
										<img src="../assets/images/product-range/chunky-mini.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-mini.png">   
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4> كيت كات شانكي ميني</h4>
                                              <a href="#" class="p-buynow" data-buynowid="4514"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-mini" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">لوح شوكولاتة لذيذ ، يضم رقائق الويفر الهشة المغطاة بشوكولاتة الحليب السلسة.</p>
                                        </div>
                                    </div>
									 <div class="tear-container tear-small"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">  
										<img src="../assets/images/product-range/kitkat-chunky.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky.png">             
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات شنكي</h4>
                                             <a href="#" class="p-buynow" data-buynowid="4510"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center kitkat-chunky">لوح شوكولاتة لذيذ ، يضم رقائق الويفر الهشة المغطاة بشوكولاتة الحليب السلسة.</p>
                                        </div>
                                    </div>
									 <div class="tear-container"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">  
										<img src="../assets/images/product-range/kitkat-chumky-mini-caramel.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-mini-caramel.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات شنكي بالكراميل</h4>
                                             <a href="#" class="p-buynow" data-buynowid="1279"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-mini-caramel" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">لوح شوكولاتة لذيذ ، يضم رقائق الويفر الهشة المغطاة بطبقة لذيذة من الكراميل وشوكولاتة الحليب.</p>
                                        </div>
                                    </div>
									 <div class="tear-container tear-small"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">   
										<img src="../assets/images/product-range/kitkat-chunky-caramel.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-caramel.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات شنكي بالفول السوداني</h4>
                                            <a href="#" class="p-buynow" data-buynowid="4511"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-caramel" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center buy-mini">لوح شوكولاتة لذيذ ، يضم رقائق الويفر الهشة المغطاة بطبقة لذيذة من معجون الفول السوداني وشوكولاتة الحليب.</p>
                                        </div>
                                    </div>  
										 <div class="tear-container"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">    
										<img src="../assets/images/product-range/kitkat-chunky-peanut-butter.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-caramel.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>كيت كات شنكي بالفول السوداني</h4>
                                            <a href="#" class="p-buynow" data-buynowid=""><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-peanut-butter" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center kitkat-chunky">لوح شوكولاتة لذيذ ، يضم رقائق الويفر الهشة المغطاة بطبقة لذيذة من معجون الفول السوداني وشوكولاتة الحليب.</p>
                                        </div>
                                    </div>
										 <div class="tear-container"></div>	
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>    
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture">                                        
                                        <img src="../assets/images/product-range/kitkat-pop-choc-mini.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>نستلة كيت كات بوب شوك – كيس من حبات الشوكولاتة بالحليب 36 غ</h4>
                                            <a href="#" class="p-buynow" data-buynowid="1216"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/pop-choc-milk-chocolate-bites" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center info-pop">يحتوي الكيس على كرات صغيرة ولذيذة من بسكويت ويفر مغلف بشوكولاته الحليب للاستمتاع بطعم الشوكولاته المثالي لا يحتوي على ألوان صناعية أو منكهات أو مواد حافظة ملائم للنباتيين</p>
                                        </div>
                                    </div>
									 <!--<div class="tear-container"></div>-->
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" >                                        
                                        <img src="../assets/images/product-range/kitkat-pop-choc-140.png">   
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>نستلة كيت كات بوب شوك – كيس من حبات الشوكولاتة بالحليب 140 غ</h4>
                                            <a href="#" class="p-buynow" data-buynowid="1217"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/pop-choc-milk-chocolate-bites" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center info-pop">يحتوي الكيس على كرات صغيرة ولذيذة من بسكويت ويفر مغلف بشوكولاته الحليب شوكولاته مثالية للمشاركة مع العائلة والأصدقاء كيس مدعّم يمكن إعادة إغلاقه لا يحتوي على ألوان صناعية أو منكهات أو مواد حافظة ملائم للنباتيين</p>
                                        </div>
                                    </div>
										<!-- <div class="tear-container"></div>-->
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                        </div> <!-- product list -->
                        <!-- <div class="right-custom-nav pull-right"></div>  -->
                        <div class="product-r-gradient"></div>
                    </div>
                </div>
           </div>
    </main>
     <?php include("../footer.php"); ?>
        <!--<script src="../assets/js/custom.js" type="text/javascript"></script>-->
        <script src="../assets/js/jquery.event.move.js" type="application/javascript"></script>
        <script src="../assets/js/jquery.twentytwenty.js" type="application/javascript"></script>
		<script src="../assets/js/ilightbox.min.js" type="application/javascript"></script>
        <script src="../assets/js/slick.min.js" type="text/javascript"></script>
		 <script src="../assets/js/product-detail.js" type="text/javascript"></script>
		 <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
        <script>
			$(document).ready(function () {
				$('.products').css('margin-top',$('.header-wrap').height() - 30);
					function contactUs() {$('#iframe_1').iLightBox();}
            });
            $(window).resize(function(){
                $('.products').css('margin-top',$('.header-wrap').height() - 30);
            });
            $('.product-list').slick({
                    variableWidth: true,
                    infinite: false,
                    nav: false,rtl:true,
                    centerMode: false,
                    margin:0,
                    easing: 'linear',
                    draggable: true,
                    slidesToScroll: 5,
                   responsive: [
                        {
                          breakpoint: 1700,
                          settings: {
                            slidesToScroll: 3,
                            margin: 0
                          }
                        },
                        {
                          breakpoint: 1390,
                          settings: {
                            /*slidesToShow: 3,*/
                            slidesToScroll: 3
                          }
                        },
                        {
                          breakpoint: 1070,
                          settings: {
                            /*slidesToShow: 2,*/
                            slidesToScroll: 2
                          }
                        },
                        {
                          breakpoint: 750,
                          settings: {
                           /* slidesToShow: 1,*/
                            slidesToScroll: 1
                          }
                        },
                        {
                          breakpoint: 430,
                          settings: {
                            /*slidesToShow: 1,*/
                            slidesToScroll: 1
                          }
                        }
                    ]
                  });

        </script>
        <script type="text/javascript">
            $(window).load(function () {
                $(".product-picture[data-orientation='vertical']").twentytwenty({
                    default_offset_pct:1, orientation: 'vertical'
                });
            });
        </script>
</body>

</html>