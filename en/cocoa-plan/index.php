
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cocoa Plan - KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="../assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/jkit.css">
    <!-- <link rel="stylesheet" type="text/css" href="../assets/sequence/css/sequence-theme.intro.css"> -->
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/layer-slider/layerslider.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/common.css?v1.0">
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="../assets/css/ie8.css">
    <script src="../assets/js/ie8.js"></script>
    <![endif]-->
</head>

<body id="cocoa">
      <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php 
            include("../config.php");
            include("../header.php");        
        ?>
    </header>
 
    <main id="product-page-content" class="page-content">
         <!-- intro videos -->
        <div class="cocoa-intro-vid">
            <!-- blur element -->
            <img class="vid-blur-elem-1" src="../assets/images/cocoa/element-blur.png" alt="">
            <img class="vid-blur-elem-2" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
            <img class="vid-blur-elem-3" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 hidden-xs hidden-sm"></div>
                    <div id="video-container">
                        <div class="main-vid-play">
                            <!-- <img src="../assets/images/cocoa/main_video.png" class="img-responsive" alt="video"> -->
                            <div id="homeBannervideo" class="banner-image home-banner-video"></div>
                        </div>
                        <!-- sub vids -->
                        <div class="cocoa-help-text"><h4>Nestlé Cocoa Plan Helps:</h4></div>
                        <div class="thumbnails-container clearfix">
                           
                                <div class="vid-slide thumbnails">
                                    <img src="../assets/images/cocoa/videos/better-sust.jpg" class="img-responsive thumb-vid" alt="better farming">
                                    <a class="thumb-play" data-play="1" href="#">
                                        <img src="../assets/images/cocoa/videos/ply-btn.png" class="img-responsive" alt="play">
                                    </a>
                                    <p>On the road to sustainability</p>
                                </div>
                                <div class="vid-slide thumbnails">
                                    <img src="../assets/images/cocoa/videos/better-farmer.jpg" class="img-responsive thumb-vid" alt="better farming">
                                    <a class="thumb-play" data-play="2" href="#">
                                        <img src="../assets/images/cocoa/videos/ply-btn.png" class="img-responsive" alt="play">
                                    </a>
                                    <p>Better Farming</p>
                                </div>
                                <div class="vid-slide thumbnails">
                                    <img src="../assets/images/cocoa/videos/better-cocoa.jpg" class="img-responsive thumb-vid" alt="better farming">
                                    <a class="thumb-play" data-play="3" href="#">
                                        <img src="../assets/images/cocoa/videos/ply-btn.png" class="img-responsive" alt="play">
                                    </a>
                                    <p>Better Cocoa</p>
                                </div>
                                <div class="vid-slide thumbnails">
                                    <img src="../assets/images/cocoa/videos/better-lives.jpg" class="img-responsive thumb-vid" alt="better farming">
                                    <a class="thumb-play" data-play="4" href="#">
                                        <img src="../assets/images/cocoa/videos/ply-btn.png" class="img-responsive" alt="play">
                                    </a>
                                    <p>Better Lives</p>
                                </div>
                            
                        </div>
                    </div>
                    </div>
                    <div class="view-galery">
                        <div class="page-mores">
                            <a href="javascript:void(0)" class="cocoa-anims kingLayer"><img src="../assets/images/cocoa/videos/cocoa_plan_button.png" class="img-responsive" alt="cocoa slides"></a>
                            <a href="javascript:void(0)" class="gal-slides"><img src="../assets/images/cocoa/videos/view_gallery.png" class="img-responsive" alt="cocoa slides"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ++++++++++++++++++++++++++++++complete slide items start+++++++++++++++++++++++++++++ -->
        <div class="cocoa-wrap">
            <div class="cocoa-close-btn"><a href="#">X</a></div>
            <!-- owl start -->
            <div id="layerslider" class="cocoa-carousel cocoa-owl">
                <!-- item 1 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <!-- content -->
                    <h2 class="ls-l" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetxin:300;offsetxout:-300;delayin:200;durationout:1000;">The Nestlé Cocoa Plan</h2>

                    <img class="ls-l cocoa-slide-1-img" style="top:50px;left:35%;white-space: nowrap;" data-ls="offsetxin:-300;offsetxout:-300;offsetxout:-300;delayin:500;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/cocoa-big.png" alt="">

                    <span class="ls-l" style="top:100px;left:47.2%;white-space: nowrap;" data-ls="offsetxin:300;delayin:600;durationout:1000;">10</span>
                    <h1 class="ls-l" style="top:210px;left:57.2%;white-space: nowrap;" data-ls="offsetxin:300;delayin:800;durationout:1000;">THINGS YOU NEED TO<br/>KNOW ABOUT COCOA</h1>
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:10%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:20%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:80%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>
                <!-- item 2 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <!-- contnet -->
                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetxin:200;delayin:500;durationout:1300;"><span class="cocoa-slide-count">01 </span>COCOA GROWS...</h2>

                    <img class="ls-l" style="top:50px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:200;delayin:800;durationout:1300; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-tree-shade-1.png" alt="tree-bg">
                    <img class="ls-l" style="top:50px;left:49%;white-space: nowrap;" data-ls="offsetyin:200;offsetyout:200;delayin:1000;durationout:1300; parallaxlevel:1;" src="../assets/images/cocoa/point-1-tree.png" alt="tree-bg">

                    <h4 class="ls-l" style="top:350px;left:50%;white-space: nowrap;" data-ls="offsetyin:100;delayin:1200;durationout:1300;">On a Tree</h4>
                    <p class="ls-l" style="top:370px;left:50%;white-space: nowrap;" data-ls="offsetyin:100;delayin:1300;durationout:1300;">Cocoa beans come from pods picked from trees before
                        <br>becoming delicious chocolate.</p>
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:210px;left:30%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-1.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:80%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:200px;left:75%;white-space: nowrap;" data-ls="offsetxin:200;delayin:600;durationout:1300;scalexin:0.5; parallaxlevel:-1.5;" src="../assets/images/cocoa/point-1-cocoa-blur-2.png" alt="">
                </div>
                <!-- item 3 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">

                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">02 </span>THE COCOA TREE'S<br/>SCIENTIFIC NAME IS</h2>

                    <h4 class="ls-l point2" style="top:60px;left:50%;white-space: nowrap;" data-ls="offsetyin:-400;offsetyout:-400;delayin:600;durationout:1300;">Theobroma cocoa</h4>

                    <h5 class="ls-l point2" style="top:100px;left:50%;white-space: nowrap;" data-ls="offsetyin:-600;offsetyout:-600;delayin:900;durationout:1300;">WHICH IS GREAT FOR</h5>

                    <img src="../assets/images/cocoa/point-2-cocoa.png" class="ls-l" style="top:130px;left:50%;white-space: nowrap;" data-ls="offsetyin:400;offsetyout:400;delayin:1200;durationout:1300;parallaxlevel:1;" alt="cocoa">

                    <p class="ls-l point2" style="top:320px;left:50%;white-space: nowrap;" data-ls="offsetyin:200;offsetyout:200;delayin:1400;durationout:1300;">Food of the Gods</p>

                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:210px;left:25%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-2-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:75%;white-space: nowrap;" data-ls="offsetxin:200;delayin:600;durationout:1300;scalexin:0.5; parallaxlevel:-1.5;" src="../assets/images/cocoa/point-2-cocoa-blur.png" alt="">
                </div>
                <!-- item 4 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">

                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">03 </span>THE FIRST KITKAT BAR<br/>WAS SOLD IN 1937...</h2>

                    <img src="../assets/images/cocoa/point-3-map.png" class="ls-l" style="top:60px;left:40%;white-space: nowrap;" data-ls="offsetxin:-400;offsetxout:-400;delayin:500;durationout:1300;parallaxlevel:-1;" alt="cocoa">

                    <img src="../assets/images/cocoa/point-3-uk.jpg" class="ls-l" style="top:80px;left:59%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:700;durationout:1300;parallaxlevel:1;" alt="uk">

                    <h1 class="ls-l point3" style="top:160px;left:60%;white-space: nowrap;" data-ls="offsetxin:200;offsetxout:200;delayin:900;durationout:1300;">IN UNITED KINGDOM</h1>

                    <img src="../assets/images/cocoa/point-3-packet.png" class="ls-l" style="top:200px;left:60%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1000;durationout:1300;parallaxlevel:-1;" alt="kitkat">
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:240px;left:20%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-1.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:80%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:200px;left:75%;white-space: nowrap;" data-ls="offsetxin:200;delayin:600;durationout:1300;scalexin:0.5; parallaxlevel:-1.5;" src="../assets/images/cocoa/point-1-cocoa-blur-2.png" alt="">
                </div>
                <!-- item 5 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">

                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">04 </span>MAXIMUM CHOCOLATE IS<br/>CONSUMED IN UK</h2>

                    <img class="ls-l" src="../assets/images/cocoa/point-4-map.png" style="top:100px;left:50%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:700;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-3-uk.jpg" style="top:70px;left:35%;white-space: nowrap;" data-ls="offsetxin:-400;offsetxout:-400;delayin:900;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/uk-10kg.png" style="top:60px;left:49%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:900;durationout:1300;parallaxlevel:-1.5;" alt="uk">
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:10%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:20%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:80%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>
                <!-- item 6 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">

                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">05 </span>MOST OF WORLD'S COCOA<br/>COMES FROM...</h2>

                    <img class="ls-l" src="../assets/images/cocoa/processed/point-5-ivory-cost-map.png" style="top:60px;left:15%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:200;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/iv-cost-val.png" style="top:100px;left:30%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:500;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-5-basket.png" style="top:100px;left:50%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:800;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/ghana-val.png" style="top:100px;left:71%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1200;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/processed/point-5-ghana-map.png" style="top:100px;left:85%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1500;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <p class="ls-l from-sec point6" style="top:310px;left:50%;white-space: nowrap;" data-ls="offsetyin:200;offsetyout:-200;delayin:1800;durationout:1300;">More than half the world's total cocoa supply
                        <br>is grown in Ivory Coast and Ghana</p>

                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:5%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:90%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>
                <!-- item 7 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">06 </span>ALL MOST ALL COCOA IS GROWN ON<br/>SMALL FAMILY FARMS WITH NO MORE<br/>THAN 4 HECTARES</h2>

                    <img class="ls-l" src="../assets/images/cocoa/point-6-farmhouse.png" style="top:100px;left:50%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:700;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <p class="ls-l from-sec point6" style="top:350px;left:50%;white-space: nowrap;" data-ls="offsetyin:200;offsetyout:-200;delayin:1000;durationout:1300;">These farms operate in much the same way as they
                        <br/>did 100 years ago.</p>
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:5%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:90%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>
                <!-- item 8 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">07 </span>TO CREATE CHOCOLATE BARS,<br/>COCOA BEAN MUST BE</h2>

                    <img class="ls-l" src="../assets/images/cocoa/point-7-tree.png" style="top:130px;left:21%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:200;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-line-1.png" style="top:250px;left:29%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:500;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-harvested.png" style="top:150px;left:38%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:800;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-line-2.png" style="top:190px;left:47%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1100;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-fer.png" style="top:80px;left:58%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1400;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-line-3.png" style="top:120px;left:69%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1700;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-dry.png" style="top:150px;left:76%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:2000;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-line-4.png" style="top:270px;left:70%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:2300;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-7-turn.png" style="top:290px;left:53%;white-space: nowrap;" data-ls="offsetxin:-400;offsetxout:-400;delayin:2500;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:240px;left:5%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:90%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:250px;left:95%;white-space: nowrap;" data-ls="offsetxin:200;delayin:600;durationout:1300;scalexin:0.5; parallaxlevel:-1.5;" src="../assets/images/cocoa/point-1-cocoa-blur-2.png" alt="">
                </div>
                <!-- item 9 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">08 </span>IN A SINGLE YEAR...</h2>

                    <img class="ls-l" src="../assets/images/cocoa/point-8-tree-cocoa.png" style="top:90px;left:27%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:200;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-8-cocoa.png" style="top:100px;left:50%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:500;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-8-kikat.png" style="top:100px;left:75%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:800;durationout:1300;parallaxlevel:-1;" alt="uk">
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:5%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:90%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>
                <!-- item 9 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">09 </span>HOWEVER...<br/>COOCA CORPS ARE DEPLETING</h2>

                    <img class="ls-l" src="../assets/images/cocoa/point-9-farm.png" style="top:60px;left:50%;white-space: nowrap;" data-ls="offsetyin:400;offsetyout:400;delayin:300;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-9-tree-bug.png" style="top:120px;left:52%;white-space: nowrap;" data-ls="offsetyin:400;offsetyout:400;delayin:600;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-9-buggi.png" style="top:160px;left:42%;white-space: nowrap;" data-ls="offsetxin:-400;offsetxout:-400;delayin:1000;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-9-buggi-1.png" style="top:120px;left:62%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:1300;durationout:1300;parallaxlevel:1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-9-buggi-3.png" style="top:220px;left:65%;white-space: nowrap;" data-ls="offsetxin:800;offsetxout:800;delayin:1600;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <p class="ls-l from-sec point6" style="top:350px;left:50%;white-space: nowrap;" data-ls="offsetyin:200;offsetyout:-200;delayin:1900;durationout:1300;">Around 30% are lost due to plant disease.</p>

                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:5%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:90%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>
                <!-- item 10 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">
                    <h2 class="ls-l from-sec" style="top:0px;left:50%;white-space: nowrap;" data-ls="offsetyin:-200;offsetyout:-200;delayin:300;durationout:1300;"><span class="cocoa-slide-count bigDig">10 </span>OFFICIAL LAUNCH OF THE NESTLE<br/>COCOA PLANT IN 2009</h2>

                    <img class="ls-l" src="../assets/images/cocoa/point-10-sweep.png" style="top:70px;left:20%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:300;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-10-house.png" style="top:70px;left:50%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:600;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <img class="ls-l" src="../assets/images/cocoa/point-10-plant.png" style="top:70px;left:80%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:400;delayin:900;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <p class="ls-l from-sec point6" style="top:350px;left:50%;white-space: nowrap;" data-ls="offsetyin:200;offsetyout:-200;delayin:1200;durationout:1300;">Nestlé is investing CHF 110 million over the
                        <br>next 10 years to help farmers.</p>
                    <!-- blur element -->
                    <img class="ls-l cocoa-slide-1-img" style="top:180px;left:5%;white-space: nowrap;" data-ls="offsetxin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/element-blur.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:10%;white-space: nowrap;" data-ls="offsetyin:-300;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:1;" src="../assets/images/cocoa/point-1-cocoa-blur-3.png" alt="">
                    <img class="ls-l cocoa-slide-1-img" style="top:0px;left:90%;white-space: nowrap;" data-ls="offsetxin:200;delayin:200;durationout:1300;scalexin:0.5; parallaxlevel:-1;" src="../assets/images/cocoa/point-1-cocoa-blur-4.png" alt="">
                </div>

                <!-- item 10 -->
                <div class="ls-slide coc-slide" data-ls="transition2d:1;timeshift:-1000;">

                    <img class="ls-l" src="../assets/images/cocoa/end-slide-cocoa.png" style="top:20px;left:50%;white-space: nowrap;" data-ls="offsetxin:-400;offsetxout:-400;delayin:300;durationout:1300;parallaxlevel:-1;" alt="uk">

                    <p class="ls-l from-sec point6" style="top:200px;left:50%;white-space: nowrap;" data-ls="offsetxin:400;offsetxout:-400;delayin:300;durationout:1300;">It's the right thing to do for cocoa farmers.
                        <br>And it helps ensure you can enjoy your favourite
                        <br/>chocolate for years to come.</p>

                    <p class="ls-l from-sec lastslider" style="top:280px;left:50%;white-space: nowrap;" data-ls="offsetxin:-400;offsetxout:-400;delayin:300;durationout:1300;">Learn more at <a target="_blank" href="http://www.nestlecocoaplan.com/">nestlecocoaplan.com</a></p>
                </div>
            </div>
            <div class="cocoa-banner-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-2">
                            <!-- <div class="cocoa-slide-btns animater">
                        <a class="cocoa-prev animBt" href="javascript:void(0)"><i class='fa fa-angle-left'></i></a>
                        <a class="cocoa-next animBt" href="javascript:void(0)"><i class='fa fa-angle-right'></i></a>
                    </div> -->
                            <div class="Presentedby">Presented By
                                <p>Nestle Cocoa Plan</p>
                            </div>

                        </div>
                    </div>
                </div>
                <img src="../assets/images/cocoa/cocoa-banner-footer.png" class="img-responsive" alt="cocoa-footer.png">
            </div>
        </div>
        <!-- cocoa wrap mobile -->
        <div class="cocoa-wrap-mobile">
            <div class="cocoa-close-btn"><a href="#">X</a></div>
            <!-- owl carousel area -->
            <div class="cocoa-mob-slide">
                <!-- slide 1 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <h2 class="introhead">The Nestlé Cocoa Plan</h2></div>
                    <div class="mob-slide-img">
                        <img src="../assets/images/cocoa/cocoa-big.png" class="img-responisve" alt="cocoa">
                    </div>
                    <div class="mob-slide-text">
                        <h1 class="intro-digit">10</h1>
                        <h2 class="intro-text">THINGS YOU NEED TO<br>KNOW ABOUT COCOA</h2>
                    </div>
                </div>
                <!-- slide 2 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <h2 class="pointheads"><span>01</span> COCOA GROWS...</h2></div>
                    <div class="mob-slide-point-img">
                        <img src="../assets/images/cocoa/point-1-tree-shade.png" class="img-responisve" alt="trees">
                    </div>
                    <div class="mob-slide-text">
                        <h4 class="pointtext">On a Tree</h4>
                        <h4 class="pointtext-sub">Cocoa beans come from pods picked<br/>from trees becoming delicious<br/>chocolate.</h4>
                    </div>
                </div>
                <!-- slide 3 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">02</span>
                        <h3 class="pointheads point-head-text"> THE COCOA TREE'S<br/>SCIENTIFIC NAME IS</h3>
                    </div>
                    <div class="mobile-exp-3">
                        <h2 class="exp-3-head">Theobroma Cacao</h2>
                        <p class="exp-3-text">WHICH IS GREEK FOR</p>
                    </div>
                    <div class="mob-slide-point-img point3">
                        <img src="../assets/images/cocoa/mobile/point-3-mob-img.png" class="img-responisve" alt="cocoa">
                    </div>
                    <div class="mobile-exp-3-down">
                        <h2 class="exp-3-down-head">Food of the Gods</h2>
                    </div>
                </div>
                <!-- slide 4 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">03</span>
                        <h3 class="pointheads point-head-text"> THE FIRST KIKAT BAR<br/>WAS SOLD IN 1937...</h3>
                    </div>
                    <div class="mob-slide-point-img point-4">
                        <img src="../assets/images/cocoa/mobile/04-img-mobile.png" class="img-responisve" alt="map">
                        <br/>
                        <img src="../assets/images/cocoa/mobile/04-text.png" class="img-responisve" alt="uk">
                    </div>
                </div>
                <!-- slide 5 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">04</span>
                        <h3 class="pointheads point-head-text"> MAXIMUM CHOCOLATE IS<br/>CONSUMED IN THE UK</h3>
                    </div>
                    <div class="ext-5-imgs">
                        <div class="uk-badge"><img src="../assets/images/cocoa/point-3-uk.jpg" class="img-responisve" alt="uk flag"></div>
                        <div class="uk-val"><img src="../assets/images/cocoa/uk-10kg.png" class="img-responisve" alt="uk value"></div>
                    </div>
                    <div class="mob-slide-point-img point-5">
                        <img src="../assets/images/cocoa/point-4-map.png" class="img-responisve" alt="map">
                    </div>
                </div>
                <!-- slide 6 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">05</span>
                        <h3 class="pointheads point-head-text"> MOST OF THE WORLD'S<br/>COCOA COMES FROM...</h3>
                    </div>
                    <div class="ext-6-text">
                        <h3>WEST AFRICA</h3></div>
                    <div class="ext-6-imgs">
                        <div class="iv-mob"><img src="../assets/images/cocoa/mobile/iv-mobile.png" class="img-responisve" alt="ivory cost"></div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="ghana-mob"><img src="../assets/images/cocoa/mobile/ghana-mobile.png" class="img-responisve" alt="ghana"></div>
                    </div>
                    <div class="mob-slide-point-img point-6">
                        <img src="../assets/images/cocoa/point-5-basket.png" class="img-responisve" alt="map">
                    </div>
                    <div class="ext-6-text-down">
                        <p>More than half the world's total cocoa supply is grown in Ivory Coast and Ghana</p>
                    </div>
                </div>
                <!-- slide 7 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits v-top-mob-align">06</span>
                        <h3 class="pointheads point-head-text"> ALMOST ALL COCOA IS<br/>GROWN ON SMALL FAMILY<br/>FARMS WITH NO MORE<br/>THAN 4 HECTARES.</h3>
                    </div>
                    <div class="mob-slide-point-img point-7">
                        <br/>
                        <br/>
                        <img src="../assets/images/cocoa/point-6-farmhouse.png" class="img-responisve" alt="farms">
                    </div>
                    <div class="ext-6-text-down">
                        <p>These farms operate in much the same way as they did 100 years ago.</p>
                    </div>
                </div>
                <!-- slide 8 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">07</span>
                        <h3 class="pointheads point-head-text"> TO CREATE CHOCOLATE<br/>BARS, COCOA BEANS<br/>MUST BE</h3>
                    </div>
                    <div class="mob-slide-point-img">
                        <br/>
                        <br/>
                        <img src="../assets/images/cocoa/mobile/point-7-all.png" class="img-responisve" alt="farms">
                    </div>
                </div>
                <!-- slide 9 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">08</span>
                        <h3 class="pointheads point-head-text"> IN A SINGLE<br/>YEAR...</h3>
                    </div>
                    <div class="mob-slide-point-img">
                        <br/>
                        <br/>
                        <img src="../assets/images/cocoa/mobile/point-8-all.png" class="img-responisve" alt="farms">
                    </div>
                </div>
                <!-- slide 10 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">09</span>
                        <h3 class="pointheads point-head-text"> HOWEVER...<br/>COCOA CROPS<br/>ARE DEPLETING</h3>
                    </div>
                    <div class="mob-slide-point-img point-10">
                        <br/>
                        <br/>
                        <img src="../assets/images/cocoa/point-9-all.png" class="img-responisve" alt="farms">
                    </div>
                </div>
                <!-- slide 11 -->
                <div class="mob-story-slide">
                    <div class="mob-slide-head">
                        <span class="points-out-digits">10</span>
                        <h3 class="pointheads point-head-text"> OFFICIAL LAUNCH OF THE<br/>NESTLE COCOA PLAN IN 2009</h3>
                    </div>
                    <!-- GRIDING -->
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 point-11-img"><img src="../assets/images/cocoa/mobile/point-10-clean.png" class="img-responisve" alt="cleaning"></div>
                            <div class="col-sm-7 col-xs-12 point-11-content">
                                <h4>Trained 27,000 cocoa farmers in 2012</h4>
                                <p>In the areas of good agricultural practices, farm management, social and environmental issues</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7 point-11-content hidden-xs">
                                <h4>Built 42 schools by 2016</h4>
                            </div>
                            <div class="col-sm-5 col-xs-12 point-11-img"><img src="../assets/images/cocoa/mobile/point-10-school.png" class="img-responisve" alt="school"></div>

                            <div class="col-sm-7 point-11-content hidden-sm">
                                <br/>
                                <h4>Built 42 schools by 2016</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 point-11-img"><img src="../assets/images/cocoa/mobile/point-10-bud.png" class="img-responisve" alt="cleaning"></div>
                            <div class="col-sm-7 col-xs-12 point-11-content">
                                <h4>Distribute 12 million higher - yielding, disease resistant cocoa plants by 2020</h4>
                            </div>
                        </div>
                    </div>
                    <div class="ext-11-down">
                        <p>Nestlé is investing CHF 110 million over the next 10 years to help farmers</p>
                    </div>
                </div>
                <!-- end slide-->
                <div class="mob-story-slide">
                    <!-- <div class="mob-slide-head"><h2 class="introhead">The Nestlé Cocoa Plan</h2></div> -->
                    <div class="mob-slide-img">
                        <img src="../assets/images/cocoa/cocoa-big.png" class="img-responisve" alt="cocoa">
                    </div>
                    <div class="mob-slide-text end-slide">
                        <p class="mob-end-text">It's the right thing to do for cocoa farmers. And it helps ensure you can enjoy your favourite chocolate for years to come.</p>
                        <p class="mob-end-text-learn">Learn more at <a target="_blank" href="http://www.nestlecocoaplan.com/">nestlecocoaplan.com</a></p>
                    </div>
                </div>

            </div>
        </div>
        <!-- ++++++++++++++++++++++++++++++complete slide items finish+++++++++++++++++++++++++++++ -->
		<?php include("../footer.php"); ?>
    </main>
   
   
    <script type="text/javascript" src="../assets/jwplayer-7/jwplayer.js"></script>
    <script type="text/javascript" src="../assets/jwplayer-7/polyfills.base64.js"></script>
    <script type="text/javascript" src="../assets/jwplayer-7/polyfills.promise.js"></script>
    <script type="text/javascript" src="../assets/jwplayer-7/provider.cast.js"></script>
    <script type="text/javascript" src="../assets/jwplayer-7/provider.shaka.js"></script>
    <script type="text/javascript" src="../assets/jwplayer-7/provider.youtube.js"></script>
    <script>
        jwplayer.key = "vFIxAzsLZ5slokYHaYI3mCT/v1WLPHQdhjWpng==";
    </script>
    <script src="../assets/js/owl.carousel.min.js" type="application/javascript"></script>
    <script src="../assets/js/ilightbox.min.js" type="application/javascript"></script>
    <script src="../assets/js/custom.js" type="application/javascript"></script>
    <!-- layer slider -->
    <script src="../assets/js/layer-slider/greensock.js" type="application/javascript"></script>
    <script src="../assets/js/layer-slider/layerslider.kreaturamedia.jquery.js" type="application/javascript"></script>
    <script src="../assets/js/layer-slider/layerslider.transitions.js" type="application/javascript"></script>
    <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
			$('#video-container').css('margin-top',$('.header-wrap').height()+50);
            // jw inits
            // main vid
            var playerInstance = jwplayer("homeBannervideo");
            var playerLinkVar = "https://www.youtube.com/watch?v=ATdg_JKh4ls";
            // jw inits
            playerInstance.setup({
                file: playerLinkVar,
                controls: true,
                image: "../assets/images/cocoa/videos/vid-banner.jpg",
                aspectratio: "16:9",
                autostart: false,
                mute: false,
                width: "100%",
                height: "100%"
            });

            // cocoa slide anim close
            $(".cocoa-close-btn").click(function () {
                playerInstance.stop();
                $(".cocoa-intro-vid").show();
                $(".cocoa-wrap").hide();
                $(".cocoa-wrap-mobile").hide();
            });
            // owl caros
            var vidSlide = $('.vidOwl');
            var vidSlideSettings = {
                loop: false,
                margin: 10,
                items: 4,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true,
                        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
                    },
                    400: {
                        items: 2,
                        nav: true,
                        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
                    },
                    768: {
                        items: 4,
                        nav: false
                    }
                }
            }
            vidSlide.owlCarousel(vidSlideSettings);
        });
        $(".thumb-play").click(function () {
            var playerInstance = jwplayer("homeBannervideo");
            var playerLinkVar = "https://www.youtube.com/watch?v=ATdg_JKh4ls";
            var clickGet = $(this).attr("data-play");
            if (clickGet == 1) {
                var playerLinkVar = "https://www.youtube.com/watch?v=ATdg_JKh4ls";
            };
            if (clickGet == 2) {
                var playerLinkVar = "https://www.youtube.com/watch?v=dwg1_WPbm64";
            };
            if (clickGet == 3) {
                var playerLinkVar = "https://www.youtube.com/watch?v=kGRnf72V460";
            };
            if (clickGet == 4) {
                var playerLinkVar = "https://www.youtube.com/watch?v=usQoSYJvkqQ";
            };
            playerInstance.setup({
                file: playerLinkVar,
                // file: "http://impact.zayedfutureenergyprize.com/videos/zayed.mp4",
                controls: true,
                image: "../assets/images/cocoa/videos/better-farmer.jpg",
                aspectratio: "16:9",
                autostart: true,
                mute: false,
                width: "100%",
                height: "100%"
            });
        });
        // gallery
        $('.gal-slides').click(function () {
            $.iLightBox(
        [
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-1.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-2.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-3.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-4.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-5.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-6.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-7.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-8.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-9.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-10.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-11.jpg"
                        },
                    {
                        URL: "../assets/images/cocoa/videos/gallery/gl-12.jpg"
                        }
        ], {
                    skin: 'dark',
                    path: 'horizontal',
                    smartRecognition: true,
                    maxScale: 1,
                    overlay: {
                        opacity: .5
                    },
                    controls: {
                        arrows: true,
                        thumbnail: false
                    },
                    styles: {
                        nextOffsetX: 300,
                        nextOpacity: .75,
                        prevOffsetX: 300,
                        prevOpacity: .55
                    },
                    thumbnails: {
                        normalOpacity: .6,
                        activeOpacity: 1
                    }
                }
            );
            return false;
        });

        // cocoa wrap mobile only
        function cocoaMobileStory() {
            var mobileCocoa = $(".cocoa-mob-slide");
            var mobileCocoa_settings = {
                loop: false,
                autoHeight: true,
                margin: 10,
                items: 1,
                nav: true,
                navText: ["<img src='../assets/images/cocoa/mobile/cocoa-mob-left.png'>", "<img src='../assets/images/cocoa/mobile/cocoa-mob-right.png'>"]
            }
            mobileCocoa.owlCarousel(mobileCocoa_settings);
            if ($(window).width() <= 990) {
                // removing desktop nature
                $(".cocoa-anims").click(function () {
                    // $(this).removeClass("kingLayer");
                    $(".cocoa-wrap").hide();
                    $(".cocoa-intro-vid").hide();
                    $(".cocoa-wrap-mobile").show();
                    // creatoing mobile carousel
                    mobileCocoa.trigger('destroy.owl.carousel');
                    mobileCocoa.find('.owl-stage-outer').children().unwrap();
                    mobileCocoa.owlCarousel(mobileCocoa_settings);
                });

            } else {

                $(".cocoa-anims").click(function () {
                    var playerInstance = jwplayer("homeBannervideo");
                    playerInstance.stop();
                    $(".cocoa-wrap").show();
                    $(".cocoa-intro-vid").hide();
                    // cocoa slide close
                    if ($(".cocoa-anims").hasClass("kingLayer")) {
                        $('#layerslider').layerSlider({
                            autoStart: false,
                            navButtons: false,
                            showCircleTimer: false,
                            responsiveUnder: 1170,
                            layersContainer: 1170,
                            skin: 'v5',
                            skinsPath: '../assets/css/layer-slider/skins/'
                        });
                    }
                    $(this).removeClass("kingLayer");
                });

                $(".cocoa-wrap-mobile").hide();
                mobileCocoa.trigger('destroy.owl.carousel');
                mobileCocoa.find('.owl-stage-outer').children().unwrap();
            }
        }
        cocoaMobileStory();
        $(window).resize(function () {
            cocoaMobileStory();
			$('#video-container').css('margin-top',$('.header-wrap').height()+50);
        })
    </script>
  </body>

</html>