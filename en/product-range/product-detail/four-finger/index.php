<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <!--<meta http-equiv="X-Frame-Options" content="SAMEORIGIN">-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/product-detail.css">
    <script src="../../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->

</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../../config.php");
            include("../../../header.php");
        ?>
    </header>

    <main class="clearfix page-content">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper">
        <div class="red-bg-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-up-arrow">
                    <a href="../two-finger/"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-down-arrow">
                    <a href="../four-finger-dark/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
    
				<div class="main-product">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>FOUR FINGER MILK CHOCOLATE BAR</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/4fingers/kitkat-4-finger-milk-c-bar-20.5g.png">
                    </div>
                </div>
				
				<!-- for mobile -->
         <div class="clearfix main-container">    
				<div class="main-product one">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>FOUR FINGER MILK CHOCOLATE BAR</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/4fingers/thumbnails/kitkat-4-finger-milk-c-bar-20.5g-small.png">
                    </div>
                </div>
				<div class="main-product two">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>FOUR FINGER MILK CHOCOLATE BAR<br>41.5G</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/4fingers/thumbnails/kitkat-4-finger-not-final-copy.png">
                    </div>
                </div>
				
				<div class="main-product three">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>FOUR FINGER MILK CHOCOLATE BAR  41.5G,<br>PACK OF 6</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/4fingers/thumbnails/kitkat-4-finger-pack-of-12.png">
                    </div>
                </div>
				
			</div>	
<!-- for mobile -->			
                <div class="thumbnail-product" id="thumb-1">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/4fingers/thumbnails/kitkat-4-finger-not-final-copy.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">KITKAT<br>FOUR FINGER MILK CHOCOLATE BAR<br>41.5G</h1>
                    </div>
                </div>
                <div class="thumbnail-product" id="thumb-2">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/4fingers/thumbnails/kitkat-4-finger-pack-of-12.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">KITKAT<br>FOUR FINGER MILK CHOCOLATE BAR  41.5G, <br>PACK OF 6</h1>
                    </div>
                </div>



            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/product-detail/right-panel-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name">KITKAT<br>FOUR FINGER</h3>
                  <p class="product-text">NESTLÉ KITKAT 4 Finger Chocolate Bar is the ideal snack for those who really value their break. Just unwrap the bar and break off one of the 4 wafer fingers, snap it into two and savour the deliciously smooth milk chocolate. It’s the ideal snack to have as part of a balanced diet with only 105k per portion (1 portion = 2 fingers).</p>
                  <hr>
                  <div class="buttons">
                      <a href="#"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#nutritionalpopup" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
				  <a id="" class="ipad-playvideo minimoments-video"  href="https://www.youtube.com/embed/jukISGago1c?autoplay=1&amp;autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/product-detail/4fingers/video-4_finger.jpg" class="img-responsive img-center">
                    </a>
                </div>
                <div class="right-panel-img">
                <div class="videoposter">
                    <a id="" class="ipad-hide minimoments-video1" href="https://www.youtube.com/embed/jukISGago1c?autoplay=1&amp;autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                    <img src="../../../assets/images/product-range/product-detail/4fingers/video-4_finger.jpg" class="img-responsive img-center">
                    </a>
                </div>
                &nbsp;<br>
                  <img class="img-responsive" src="../../../assets/images/product-range/product-detail/4fingers/kitkat-4-fingers-inner.png" width="85%">
                </div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->

    </main>
    <div class="nutri-popup-outer" id="nutritionalpopup">
            <div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
            <h2>Kitkat<br>FOUR FINGER</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <thead>
                        <tr>      
                          <th>Nutrient</th>
                          <th>Per 100g</th>
                          <th>Per serving**</th>
                          <th>Reference Intake*</th>
                          <th>%RI*</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td rowspan="2">Energy</td>
                          <td>510.84 KJ</td>
                          <td>212 KJ</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>122.09 kcal</td>
                          <td>50.67 kcal</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td rowspan="2">Fat <br/>of which: saturates</td>
                          <td>25.78g</td>
                          <td>10.7g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>16.14g</td>
                          <td>6.7g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
                          <td>61.68g</td>
                          <td>25.6g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>40.10g</td>
                          <td>20.7g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Fibre</td>
                          <td>2.0g</td>
                          <td>0.9g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Protein</td>
                          <td>6.02g</td>
                          <td>2.8g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Salt</td>
                          <td>0.072g</td>
                          <td>0.03g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        
                      </tbody>
                    </table>
                    <p>*Reference Intake of an average adult (__ KJ/__ kcal).</p>
                    <p>**Per bar.</p>
                    <p>Portions should be adjusted for childern of different ages.</p>
                </div>
                
            </div>
        </div>
    <div class="nutri-popup-overlay"></div>
    <?php include("../../../footer.php"); ?>
        <!--<script src="../../assets/js/custom.js" type="application/javascript"></script>-->
        <script type="text/javascript" src="../../../assets/js/slick.min.js"></script>
         <script src="../../../assets/js/ilightbox.min.js" type="application/javascript"></script>
         <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
         <script src="../../../assets/js/product-detail.js" type="application/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
              $('.minimoments-video').iLightBox();
              $('.minimoments-video1').iLightBox();
				if($(window).width() < 768 ){
					
					$('.main-container').slick({
                    infinite: false,
                    nav: false,dots:true,
                    easing: 'linear',
                    draggable: true,
					slidesToShow: 1

                  });
				}
				
				//+++++++++++product page nutrient table popup++++++
    $(".nutritional-info").on("click", function () {
        var nutritionalPopup = $(this).attr("href");
        $(".nutri-popup-overlay").show();
        $(nutritionalPopup).show();
        $(nutritionalPopup).position({
            my: "center",
            at: "center",
            of: window
        });
        return false;
    });

    $(".nutri-popup-trigger").click(function (event) {
        $(this).toggleClass("active");
        $(this).parents(".mobile-nutri-pop-up").children(".nutri-info").slideToggle();
        var owlactiveHeight = $(this).parents(".owl-item.active").height();
    });
    $(".nutri-closer, .nutri-popup-overlay").click(function (event) {
        $(".nutri-popup-overlay, .nutri-popup-outer").hide();
    });
    
              
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                    //console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                   // var lastSlash = main_img.lastIndexOf("/");
                   var lastSlash = main_img.split("/").pop();
                   
                   var text = main_img.substring(0, main_img.lastIndexOf('/'));
                   
                   main_img = text+'/thumbnails/'+lastSlash; 
                   console.log(" last slash "+lastSlash);
                   $(this).children('div').find('h1').html(main_title);
                                  $(this).children('div').find('img').attr('src',main_img);
     
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
			
			  $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
        </script>
</body>

</html>
