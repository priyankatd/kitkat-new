<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/product-detail.css">
	<style>@media (max-width: 767px){.product-image {top: 87%;}}</style>
    <script src="../../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->

</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../../config.php");
            include("../../../header.php");
        ?>
    </header>

    <main class="clearfix page-content">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper">
        <div class="red-bg-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-up-arrow">
                    <a href="../min-moments/"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-down-arrow">
                    <a href="../four-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
            
				<div class="main-product">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>TWO FINGER MILK CHOCOLATE BAR<br>20.5G</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/kitkat-2-finger-milk-c-bar-20.5g.png">
                    </div>
                </div>
				
				<!-- for mobile -->
         <div class="clearfix main-container">   
        <div class="main-product one">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>TWO FINGER MILK CHOCOLATE BAR<br>20.5G</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-2-finger-milk-c-bar-20.5g.png">
                    </div>
                </div>
				<div class="main-product two">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>TWO FINGER CHOCOLATE BAR 20.5G, BOX OF 36</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-2-finger-multipack.png">
                    </div>
                </div>
				
				<div class="main-product three">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>TWO FINGER MILK CHOCOLATE BAR 20.5G, MULTIPACK, PACK OF 12</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-pack-of-12.png">
                    </div>
                </div>
				
				<!-- for mobile -->
				
			</div>		
                <div class="thumbnail-product" id="thumb-1">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-2-finger-multipack.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">KITKAT<br>TWO FINGER CHOCOLATE BAR 20.5G, BOX OF 36</h1>
                    </div>
                </div>
                <div class="thumbnail-product" id="thumb-2">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/2fingers/thumbnails/kitkat-pack-of-12.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">KITKAT<br>TWO FINGER MILK CHOCOLATE BAR 20.5G, MULTIPACK, PACK OF 12</h1>
                    </div>
                </div>
			<!-- 
                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
 -->

            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/product-detail/right-panel-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name">KITKAT<br>TWO FINGER</h3>
                  <p class="product-text">NESTLÉ KITKAT 2 Finger is the perfect snack for any break time. It can be enjoyed with family and friends at home, or with colleagues during the day. It’s the ideal snack to enjoy as part of a balanced diet with only 105k calories per portion (1 portion = 1 individually wrapped bar). Just unwrap, break off a finger, snap it in two and savour the great tasting crispy wafer fingers covered with smooth milk chocolate.</p>
                  <hr>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="1213"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#nutritionalpopup" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img class="img-responsive" src="../../../assets/images/product-range/product-detail/2fingers/2-finger-inner.png">
                </div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->

    </main>
        <div class="nutri-popup-outer" id="nutritionalpopup">
            <div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
            <h2>Kitkat<br>TWO FINGER</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <thead>
                        <tr>      
                          <th>Nutrient</th>
                          <th>Per 100g</th>
                          <th>Per serving**</th>
                          <th>Reference Intake*</th>
                          <th>%RI*</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td rowspan="2">Energy</td>
                          <td>514.12 KJ</td>
                          <td>91 KJ</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>122.87 kcal</td>
                          <td>21.7495 kcal</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td rowspan="2">Fat <br/>of which: saturates</td>
                          <td>25.98g</td>
                          <td>4.6g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>15.81g</td>
                          <td>2.8g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
                          <td>61.58g</td>
                          <td>10.9g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>50.28g</td>
                          <td>8.9g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Fibre</td>
                          <td>2.0g</td>
                          <td>0.9g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Protein</td>
                          <td>6.77g</td>
                          <td>1.2g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Salt</td>
                          <td>0.113g</td>
                          <td>0.02g</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        
                      </tbody>
                    </table>
                    <p>*Reference Intake of an average adult (__kJ/__kcal).</p>
                    <p>**Per bar.</p>
                    <p>Portions should be adjusted for childern of different ages.</p>
                </div>
                
            </div>
        </div>
    <div class="nutri-popup-overlay"></div>
    
    <?php include("../../../footer.php"); ?>
        <!--<script src="../../assets/js/custom.js" type="application/javascript"></script>-->
        <script type="text/javascript" src="../../../assets/js/slick.min.js"></script>
         <script src="../../../assets/js/ilightbox.min.js" type="application/javascript"></script>
         <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
         <script src="../../../assets/js/product-detail.js" type="application/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
				
	
				if($(window).width() < 768 ){
					$('.main-container').slick({
						infinite: false,
						nav: false,dots:true,
						easing: 'linear',
						draggable: true,
						slidesToShow: 1
                  });
				}
				
				//+++++++++++product page nutrient table popup++++++
    $(".nutritional-info").on("click", function () {
        var nutritionalPopup = $(this).attr("href");
        $(".nutri-popup-overlay").show();
        $(nutritionalPopup).show();
        $(nutritionalPopup).position({
            my: "center",
            at: "center",
            of: window
        });
        return false;
    });

    $(".nutri-popup-trigger").click(function (event) {
        $(this).toggleClass("active");
        $(this).parents(".mobile-nutri-pop-up").children(".nutri-info").slideToggle();
        var owlactiveHeight = $(this).parents(".owl-item.active").height();
    });
    $(".nutri-closer, .nutri-popup-overlay").click(function (event) {
        $(".nutri-popup-overlay, .nutri-popup-outer").hide();
    });
    
              
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                    //console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                   // var lastSlash = main_img.lastIndexOf("/");
                   var lastSlash = main_img.split("/").pop();
                   
                   var text = main_img.substring(0, main_img.lastIndexOf('/'));
                   
                   main_img = text+'/thumbnails/'+lastSlash; 
                   console.log(" last slash "+lastSlash);
                   $(this).children('div').find('h1').html(main_title);
                                  $(this).children('div').find('img').attr('src',main_img);
     
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
			
			  $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
        </script>
</body>

</html>
