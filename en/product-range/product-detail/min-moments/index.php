<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <!--<meta http-equiv="X-Frame-Options" content="SAMEORIGIN">-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/mm.css?v1.4">
    <!--<link rel="stylesheet" type="text/css" href="../../assets/css/mini-moments.css">-->

    <script src="../../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->
</head>

<body id="home">
    <div class="nutri-popup-overlay"></div>
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../../config.php");
            include("../../../header.php");
        ?>
    </header>

    <main class="clearfix page-content mm-slider">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper dreamy-breaker-bg" data-color="#000">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                 <!--  <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name dreamy-breaker-color"> COOKIES & CREAM</h3>
                  <p class="product-text db-p-color">The marvelous balance of cookies bits and rich white cream</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#cream-ninfo" class="nutritional-info"><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> MINI MOMENTS</h3>
                  <div class="videoposter">
                    <a href="https://www.youtube.com/embed/qrBam8INpGQ?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video1 mm-vid-box-con" data-caption="New KitKat mini moments" data-type="iframe" data-options="width:1920, height:1080">
                      <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper live-breaker-bg" data-color="grey">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                 <!--  <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/live-breaker/live-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/live-breaker/live-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/live-breaker/live-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name live-breaker-color">MOCHA</h3>
                  <p class="product-text db-p-color">
                    An authentic mocha blend of roasted coffee and creamy milk chocolate
                  </p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#macha-ninfo" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> MINI MOMENTS</h3>
                  <div class="videoposter">
                    <a href="https://www.youtube.com/embed/qrBam8INpGQ?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video2 mm-vid-box-con" data-caption="New KitKat mini moments" data-type="iframe" data-options="width:1920, height:1080">
                      <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
              <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper rr-breaker-bg" data-color="red">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <!-- <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/rr-breaker/rr-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/rr-breaker/rr-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/rr-breaker/rr-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name color-hazel">HAZELNUT</h3>
                  <p class="product-text db-p-color">Fascinating with the creamy and crunch hazelnut experience.</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#hazelnut-ninfo" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> MINI MOMENTS</h3>
                  <div class="videoposter">
                    <a href="https://www.youtube.com/embed/qrBam8INpGQ?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video3 mm-vid-box-con" data-caption="New KitKat mini moments" data-type="iframe" data-options="width:1920, height:1080">
                      <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper thrill-breaker-bg">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                 <!--  <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/thrill-breaker/thrill-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/thrill-breaker/thrill-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/thrill-breaker/thrill-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name thrill-breaker-color">MILK CHOCOLATE</h3>
                  <p class="product-text db-p-color">Fascinating with the creamy and crunch hazelnut experience.</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#milk-ninfo" class="nutritional-info"><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> MINI MOMENTS</h3>
                  <div class="videoposter">
                    <a href="https://www.youtube.com/embed/qrBam8INpGQ?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video4 mm-vid-box-con" data-caption="New KitKat mini moments" data-type="iframe" data-options="width:1920, height:1080">
                      <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <div class="kitkat-description-wrapper love-breaker-bg">
        <div class="red-bg-bottom dreamy-breaker-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel mm-left">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                 <!--  <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div> -->
                  <div class="pd-down-arrow">
                    <a href="../two-finger/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
                <div class="clearfix main-product">
                    <div class="mm-title">
                        <img src="../../../assets/images/product-range/mini-moments/love-breaker/love-breaker-title.png" class="img-responsive" alt="" />
                    </div>
                    <div class="product-image text-center mm-image">
                        <img src="../../../assets/images/product-range/mini-moments/love-breaker/love-breaker-img.png" class="img-responsive">
                    </div>
                </div>

                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/mini-moments/love-breaker/love-breaker-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name love-breaker-color">CARAMEL</h3>
                  <p class="product-text db-p-color">Fascinating with the creamy and crunch hazelnut experience.</p>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="5096"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#caramel-ninfo" class="nutritional-info"><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                  <hr>
                  <h3 class="product-name"> MINI MOMENTS</h3>
                  <div class="videoposter">
                    <a href="https://www.youtube.com/embed/qrBam8INpGQ?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" class="minimoments-video5 mm-vid-box-con" data-caption="New KitKat mini moments" data-type="iframe" data-options="width:1920, height:1080">
                      <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-vid.jpg" alt="" />
                    </a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../../assets/images/product-range/mini-moments/dreamy-breaker/dreamy-maker-pack.jpg" class="img-responsive mm-pack">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->
    </main>

    <!-- Nutritional info modals-->
    <div class="nutri-popup-outer" id="macha-ninfo">
    		<div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
    		<h2>Mocha</h2>
            <div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
    				<table border="0" cellpadding="0" cellspacing="0">
					  <thead>
					    <tr>
					      <th>Nutrient</th>
					      <th>Per 100g</th>
					      <th>Per serving**</th>
					      <th>Reference Intake*</th>
					      <th>%RI*</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td rowspan="2">Energy</td>
					      <td>2135kJ</td>
					      <td>971kJ</td>
					      <td>8400kJ</td>
					      <td>12%</td>
					    </tr>
					    <tr>
					      <td>510kcal</td>
					      <td>232kcal</td>
					      <td>2000kcal</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td rowspan="2">Fat <br/>of which: saturates</td>
					      <td>24.8g</td>
					      <td>11.3g</td>
					      <td>70g</td>
					      <td>16%</td>
					    </tr>
					    <tr>
					      <td>14.1g</td>
					      <td>6.4g</td>
					      <td>20g</td>
					      <td>32%</td>
					    </tr>
					    <tr>
					      <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
					      <td>64.6g</td>
					      <td>29.4g</td>
					      <td>260g</td>
					      <td>11%</td>
					    </tr>
					    <tr>
					      <td>52.2g</td>
					      <td>23.8gl</td>
					      <td>90g</td>
					      <td>26%</td>
					    </tr>
					    <tr>
					      <td>Fibre</td>
					      <td>2.0g</td>
					      <td>0.9g</td>
					      <td>-</td>
					      <td>-</td>
					    </tr>
					    <tr>
					      <td>Protein</td>
					      <td>5.8g</td>
					      <td>2.6g</td>
					      <td>50g</td>
					      <td>5%</td>
					    </tr>
					    <tr>
					      <td>Salt</td>
					      <td>0.20g</td>
					      <td>0.10g</td>
					      <td>6g</td>
					      <td>2%</td>
					    </tr>

					  </tbody>
					</table>
					<p>*Reference Intake of an average adult (8400kJ/2000kcal).</p>
					<p>**Per bar.</p>
					<p>Portions should be adjusted for childern of different ages.</p>
    			</div>
    			
    		</div>
    	</div>

    	<div class="nutri-popup-outer" id="cream-ninfo">
    		<div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
    		<h2>Cookies and Cream</h2>
            <div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
    				<table border="0" cellpadding="0" cellspacing="0">
					  <thead>
					    <tr>
					      <th>Nutrient</th>
					      <th>Per 100g</th>
					      <th>Per serving**</th>
					      <th>Reference Intake*</th>
					      <th>%RI*</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td rowspan="2">Energy</td>
					      <td>2135kJ</td>
					      <td>971kJ</td>
					      <td>8400kJ</td>
					      <td>12%</td>
					    </tr>
					    <tr>
					      <td>510kcal</td>
					      <td>232kcal</td>
					      <td>2000kcal</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td rowspan="2">Fat <br/>of which: saturates</td>
					      <td>24.8g</td>
					      <td>11.3g</td>
					      <td>70g</td>
					      <td>16%</td>
					    </tr>
					    <tr>
					      <td>14.1g</td>
					      <td>6.4g</td>
					      <td>20g</td>
					      <td>32%</td>
					    </tr>
					    <tr>
					      <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
					      <td>64.6g</td>
					      <td>29.4g</td>
					      <td>260g</td>
					      <td>11%</td>
					    </tr>
					    <tr>
					      <td>52.2g</td>
					      <td>23.8gl</td>
					      <td>90g</td>
					      <td>26%</td>
					    </tr>
					    <tr>
					      <td>Fibre</td>
					      <td>2.0g</td>
					      <td>0.9g</td>
					      <td>-</td>
					      <td>-</td>
					    </tr>
					    <tr>
					      <td>Protein</td>
					      <td>5.8g</td>
					      <td>2.6g</td>
					      <td>50g</td>
					      <td>5%</td>
					    </tr>
					    <tr>
					      <td>Salt</td>
					      <td>0.20g</td>
					      <td>0.10g</td>
					      <td>6g</td>
					      <td>2%</td>
					    </tr>

					  </tbody>
					</table>
					<p>*Reference Intake of an average adult (8400kJ/2000kcal).</p>
					<p>**Per bar.</p>
					<p>Portions should be adjusted for childern of different ages.</p>
    			</div>
    			
    		</div>
    	</div>
    	<div class="nutri-popup-outer" id="milk-ninfo">
    		<div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
    		<h2>Milk Chocolate</h2>
            <div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
    				<table border="0" cellpadding="0" cellspacing="0">
					  <thead>
					    <tr>
					      <th>Nutrient</th>
					      <th>Per 100g</th>
					      <th>Per serving**</th>
					      <th>Reference Intake*</th>
					      <th>%RI*</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td rowspan="2">Energy</td>
					      <td>2135kJ</td>
					      <td>971kJ</td>
					      <td>8400kJ</td>
					      <td>12%</td>
					    </tr>
					    <tr>
					      <td>510kcal</td>
					      <td>232kcal</td>
					      <td>2000kcal</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td rowspan="2">Fat <br/>of which: saturates</td>
					      <td>24.8g</td>
					      <td>11.3g</td>
					      <td>70g</td>
					      <td>16%</td>
					    </tr>
					    <tr>
					      <td>14.1g</td>
					      <td>6.4g</td>
					      <td>20g</td>
					      <td>32%</td>
					    </tr>
					    <tr>
					      <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
					      <td>64.6g</td>
					      <td>29.4g</td>
					      <td>260g</td>
					      <td>11%</td>
					    </tr>
					    <tr>
					      <td>52.2g</td>
					      <td>23.8gl</td>
					      <td>90g</td>
					      <td>26%</td>
					    </tr>
					    <tr>
					      <td>Fibre</td>
					      <td>2.0g</td>
					      <td>0.9g</td>
					      <td>-</td>
					      <td>-</td>
					    </tr>
					    <tr>
					      <td>Protein</td>
					      <td>5.8g</td>
					      <td>2.6g</td>
					      <td>50g</td>
					      <td>5%</td>
					    </tr>
					    <tr>
					      <td>Salt</td>
					      <td>0.20g</td>
					      <td>0.10g</td>
					      <td>6g</td>
					      <td>2%</td>
					    </tr>

					  </tbody>
					</table>
					<p>*Reference Intake of an average adult (8400kJ/2000kcal).</p>
					<p>**Per bar.</p>
					<p>Portions should be adjusted for childern of different ages.</p>
    			</div>
    			
    		</div>
    	</div>
    	<div class="nutri-popup-outer" id="caramel-ninfo">
    		<div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
    		<h2>Caramel</h2>
            <div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
    				<table border="0" cellpadding="0" cellspacing="0">
					  <thead>
					    <tr>
					      <th>Nutrient</th>
					      <th>Per 100g</th>
					      <th>Per serving**</th>
					      <th>Reference Intake*</th>
					      <th>%RI*</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td rowspan="2">Energy</td>
					      <td>2135kJ</td>
					      <td>971kJ</td>
					      <td>8400kJ</td>
					      <td>12%</td>
					    </tr>
					    <tr>
					      <td>510kcal</td>
					      <td>232kcal</td>
					      <td>2000kcal</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td rowspan="2">Fat <br/>of which: saturates</td>
					      <td>24.8g</td>
					      <td>11.3g</td>
					      <td>70g</td>
					      <td>16%</td>
					    </tr>
					    <tr>
					      <td>14.1g</td>
					      <td>6.4g</td>
					      <td>20g</td>
					      <td>32%</td>
					    </tr>
					    <tr>
					      <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
					      <td>64.6g</td>
					      <td>29.4g</td>
					      <td>260g</td>
					      <td>11%</td>
					    </tr>
					    <tr>
					      <td>52.2g</td>
					      <td>23.8gl</td>
					      <td>90g</td>
					      <td>26%</td>
					    </tr>
					    <tr>
					      <td>Fibre</td>
					      <td>2.0g</td>
					      <td>0.9g</td>
					      <td>-</td>
					      <td>-</td>
					    </tr>
					    <tr>
					      <td>Protein</td>
					      <td>5.8g</td>
					      <td>2.6g</td>
					      <td>50g</td>
					      <td>5%</td>
					    </tr>
					    <tr>
					      <td>Salt</td>
					      <td>0.20g</td>
					      <td>0.10g</td>
					      <td>6g</td>
					      <td>2%</td>
					    </tr>

					  </tbody>
					</table>
					<p>*Reference Intake of an average adult (8400kJ/2000kcal).</p>
					<p>**Per bar.</p>
					<p>Portions should be adjusted for childern of different ages.</p>
    			</div>
    			
    		</div>
    	</div>
    	<div class="nutri-popup-outer" id="hazelnut-ninfo">
    		<div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
    		<h2>Hazelnut</h2>
            <div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
    				<table border="0" cellpadding="0" cellspacing="0">
					  <thead>
					    <tr>
					      <th>Nutrient</th>
					      <th>Per 100g</th>
					      <th>Per serving**</th>
					      <th>Reference Intake*</th>
					      <th>%RI*</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td rowspan="2">Energy</td>
					      <td>2135kJ</td>
					      <td>971kJ</td>
					      <td>8400kJ</td>
					      <td>12%</td>
					    </tr>
					    <tr>
					      <td>510kcal</td>
					      <td>232kcal</td>
					      <td>2000kcal</td>
					      <td></td>
					    </tr>
					    <tr>
					      <td rowspan="2">Fat <br/>of which: saturates</td>
					      <td>24.8g</td>
					      <td>11.3g</td>
					      <td>70g</td>
					      <td>16%</td>
					    </tr>
					    <tr>
					      <td>14.1g</td>
					      <td>6.4g</td>
					      <td>20g</td>
					      <td>32%</td>
					    </tr>
					    <tr>
					      <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
					      <td>64.6g</td>
					      <td>29.4g</td>
					      <td>260g</td>
					      <td>11%</td>
					    </tr>
					    <tr>
					      <td>52.2g</td>
					      <td>23.8gl</td>
					      <td>90g</td>
					      <td>26%</td>
					    </tr>
					    <tr>
					      <td>Fibre</td>
					      <td>2.0g</td>
					      <td>0.9g</td>
					      <td>-</td>
					      <td>-</td>
					    </tr>
					    <tr>
					      <td>Protein</td>
					      <td>5.8g</td>
					      <td>2.6g</td>
					      <td>50g</td>
					      <td>5%</td>
					    </tr>
					    <tr>
					      <td>Salt</td>
					      <td>0.20g</td>
					      <td>0.10g</td>
					      <td>6g</td>
					      <td>2%</td>
					    </tr>

					  </tbody>
					</table>
					<p>*Reference Intake of an average adult (8400kJ/2000kcal).</p>
					<p>**Per bar.</p>
					<p>Portions should be adjusted for childern of different ages.</p>
    			</div>
    			
    		</div>
    	</div>


    <?php include("../../../footer.php"); ?>
        <script src="../../../assets/js/custom.js" type="application/javascript"></script>
        <script src="../../../assets/js/owl.carousel.min.js" type="application/javascript"></script>
        <script src="../../../assets/js/ilightbox.min.js" type="application/javascript"></script>
        <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".mm-slider .owl-next").empty();
                $(".mm-slider .owl-prev").empty();
                $('.minimoments-video1').iLightBox();
                $('.minimoments-video2').iLightBox();
                $('.minimoments-video3').iLightBox();
                $('.minimoments-video4').iLightBox();
                $('.minimoments-video5').iLightBox();
              /*  if( currentIndex == 0 ) {
                  $(".owl-controls").addClass("mm-slide-one");
                } else if (currentIndex == 1) {
                  $(".owl-controls").addClass("mm-slide-two");
                } else if (currentIndex == 2) {
                }*/
                $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
				//if($(window).height() > 930){
					//$('.kitkat-description-wrapper').css('width','100%');
				//}
				//console.log("window height" + $(window).height());	
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                   // console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                    $(this).children('div').find('h1').html(main_title);
                    $(this).children('div').find('img').attr('src',main_img);
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
 
        var mmowl = $('.mm-slider');
        var mmowl_settings = {
          margin: 0,dots: true,nav:true,//loop: true,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 1
            },
            1000: {
              items: 1
            }
          }
        }
        mmowl.owlCarousel(mmowl_settings);

        mmowl.on('changed.owl.carousel', function(event) {
           var currentItemOwl = event.item.index + 1;
           //alert(currentItemOwl);
           if(currentItemOwl == 1) {
             $(".red-bg-bottom").css("background","#6fbae6");
           } else if (currentItemOwl == 2) {
             $(".red-bg-bottom").css("background","#cb8f5c");
           } else if (currentItemOwl == 3) {
             $(".red-bg-bottom").css("background","#4e9551");
           } else if (currentItemOwl == 4) {
             $(".red-bg-bottom").css("background","#fac062");
           } else if(currentItemOwl == 5) {
             $(".red-bg-bottom").css("background","#f03439");
           }
        });
		
		if( $(window).width() < 1200 )  {
          mmowl.on('changed.owl.carousel', function(event) {
           var currentItemOwl = event.item.index + 1;
           if(currentItemOwl == 1) {
             $(".mm-slider .owl-controls").css("background-color","#6fbae6");
           } else if (currentItemOwl == 2) {
             $(".mm-slider .owl-controls").css("background-color","#cb8f5c");
           } else if (currentItemOwl == 3) {
             $(".mm-slider .owl-controls").css("background-color","#4e9551");
           } else if (currentItemOwl == 4) {
             $(".mm-slider .owl-controls").css("background-color","#f03439");
           } else if(currentItemOwl == 5) {
             $(".mm-slider .owl-controls").css("background-color","#fac062");
           }
        });
        }

        $(window).resize(function(){
            if( $(window).width() < 1200 )  {
          mmowl.on('changed.owl.carousel', function(event) {
           var currentItemOwl = event.item.index + 1;
           if(currentItemOwl == 1) {
             $(".mm-slider .owl-controls").css("background-color","#6fbae6");
           } else if (currentItemOwl == 2) {
             $(".mm-slider .owl-controls").css("background-color","#cb8f5c");
           } else if (currentItemOwl == 3) {
             $(".mm-slider .owl-controls").css("background-color","#4e9551");
           } else if (currentItemOwl == 4) {
             $(".mm-slider .owl-controls").css("background-color","#f03439");
           } else if(currentItemOwl == 5) {
             $(".mm-slider .owl-controls").css("background-color","#fac062");
           }
        });
        }
        });
		
       </script>
</body>

</html>
