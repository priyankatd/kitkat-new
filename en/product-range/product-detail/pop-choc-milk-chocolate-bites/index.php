<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/css/product-detail.css">
    <script src="../../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->

</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../../config.php");
            include("../../../header.php");
        ?>
    </header>

    <main class="clearfix page-content">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper">
        <div class="red-bg-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="../../"><img src="../../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-up-arrow">
                    <a href="../chunky-peanut-butter/"><img src="../../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-down-arrow">
                    <a href="../kitkat-sense-hazelnut/"><img src="../../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
  
				<div class="main-product">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>POP CHOC MILK BITES BAG<br>36G</h1>
                    </div>
                    <div class="product-image text-cente v-center top-63">
                        <img src="../../../assets/images/product-range/product-detail/pop-choc/pop-choc-36g.png">
                    </div>
                </div>
				
				<!-- for mobile -->
        <div class="clearfix main-container">    
        <div class="main-product one">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>POP CHOC MILK BITES BAG<br>36G</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/pop-choc/thumbnails/pop-choc-36g.png">
                    </div>
                </div>
				<div class="main-product two">
                    <div>
                        <h1 class="main-title text-center">KITKAT<br>POP CHOC MILK BITES BAG<br>140G</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../../assets/images/product-range/product-detail/pop-choc/thumbnails/pop-choc-140g.png">
                    </div>
                </div>

				<!-- for mobile -->
				
			</div>		
                <div class="thumbnail-product" id="thumb-1">
                    <div>
                        <img src="../../../assets/images/product-range/product-detail/pop-choc/thumbnails/pop-choc-140g.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">KITKAT<br>POP CHOC MILK BITES BAG<br>140G</h1>
                    </div>
                </div>


            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../../assets/images/product-range/product-detail/right-panel-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name">KITKAT<br>POP CHOC MILK BITES BAG</h3>
                  <p class="product-text">A delicious chocolate bar, comprising a crispy wafer covered with a tasty layer of caramel and milk chocolate.</p>
                  <hr>
                  <div class="buttons">
                      <a href="#" class="p-buynow" data-buynowid="4515"><img src="../../../assets/images/product-range/buynow.png"></a>
                      <a href="#nutritionalpopup" class="nutritional-info" ><img src="../../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
				  <a id="" class="ipad-playvideo minimoments-video1" href="https://www.youtube.com/embed/I8NP67Cg-Yg?autoplay=1&amp;autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-type="iframe" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                        <img src="../../../assets/images/product-range/product-detail/pop-choc/video-kitkat_pop_choc.jpg" class="img-responsive img-center">
                      </a>
                </div>
                <div class="right-panel-img">
                    <div class="videoposter">
                      <a id="" class="ipad-hide minimoments-video" href="https://www.youtube.com/embed/I8NP67Cg-Yg?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1" data-type="iframe" data-caption="New KitKat mini moments" data-options="width:1920, height:1080">
                        <img src="../../../assets/images/product-range/product-detail/pop-choc/video-kitkat_pop_choc.jpg" class="img-responsive img-center">
                      </a>
                    </div>
                    &nbsp;<br>
                   <img class="img-responsive" src="../../../assets/images/product-range/product-detail/pop-choc/pop-choc-bolls-inner.png">
                </div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->

    </main>
    <div class="nutri-popup-outer" id="nutritionalpopup">
            <div class="nutri-closer pull-right"><img src="../../../assets/images/close-small.png" class="img-responsive"></div>
            <h2>Kitkat<br>Pop Choc Milk Bites Bag 36g</h2>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <thead>
                        <tr>      
                          <th>Nutrient</th>
                          <th>Per 100g</th>
                          <th>Per serving**</th>
                          <th>Reference Intake*</th>
                          <th>%RI*</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td rowspan="2">Energy</td>
                          <td>529 KJ</td>
                          <td>191 KJ</td>
                          <td>-</td>
                          <td>10%</td>
                        </tr>
                        <tr>
                          <td>126.434 kcal</td>
                          <td>45.6501 kcal</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td rowspan="2">Fat <br/>of which: saturates</td>
                          <td>29.71g</td>
                          <td>10.7g</td>
                          <td>-</td>
                          <td>15%</td>
                        </tr>
                        <tr>
                          <td>17.61g</td>
                          <td>6.3g</td>
                          <td>-</td>
                          <td>32%</td>
                        </tr>
                        <tr>
                          <td rowspan="2">Carbohydrates <br/>of which: sugars</td>
                          <td>58.25g</td>
                          <td>21g</td>
                          <td></td>
                          <td>8%</td>
                        </tr>
                        <tr>
                          <td>45.1g</td>
                          <td>16.5g</td>
                          <td></td>
                          <td>18%</td>
                        </tr>
                        <tr>
                          <td>Fibre</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Protein</td>
                          <td>5.99g</td>
                          <td>2.2g</td>
                          <td>-</td>
                          <td>4%</td>
                        </tr>
                        <tr>
                          <td>Salt</td>
                          <td>0.06g</td>
                          <td>0.02g</td>
                          <td>-</td>
                          <td>19%</td>
                        </tr>
                        
                      </tbody>
                    </table>
                    <p>*Reference Intake of an average adult (__ KJ/__ kcal).</p>
                    <p>**Per bar.</p>
                    <p>Portions should be adjusted for children of different ages</p>
                </div>
                
            </div>
        </div>
    <div class="nutri-popup-overlay"></div>
    <?php include("../../../footer.php"); ?>
        <!--<script src="../../assets/js/custom.js" type="application/javascript"></script>-->
        <script type="text/javascript" src="../../../assets/js/slick.min.js"></script>
         <script src="../../../assets/js/ilightbox.min.js" type="application/javascript"></script>
         <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
         <script src="../../../assets/js/product-detail.js" type="application/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
              $('.minimoments-video').iLightBox();
              $('.minimoments-video1').iLightBox();
				if($(window).width() < 768 ){
					
					$('.main-container').slick({
                    infinite: false,
                    nav: false,dots:true,
                    easing: 'linear',
                    draggable: true,
					slidesToShow: 1

                  });
				}
				
				//+++++++++++product page nutrient table popup++++++
    $(".nutritional-info").on("click", function () {
        var nutritionalPopup = $(this).attr("href");
        $(".nutri-popup-overlay").show();
        $(nutritionalPopup).show();
        $(nutritionalPopup).position({
            my: "center",
            at: "center",
            of: window
        });
        return false;
    });

    $(".nutri-popup-trigger").click(function (event) {
        $(this).toggleClass("active");
        $(this).parents(".mobile-nutri-pop-up").children(".nutri-info").slideToggle();
        var owlactiveHeight = $(this).parents(".owl-item.active").height();
    });
    $(".nutri-closer, .nutri-popup-overlay").click(function (event) {
        $(".nutri-popup-overlay, .nutri-popup-outer").hide();
    });
    
              
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                    //console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                   // var lastSlash = main_img.lastIndexOf("/");
                   var lastSlash = main_img.split("/").pop();
                   
                   var text = main_img.substring(0, main_img.lastIndexOf('/'));
                   
                   main_img = text+'/thumbnails/'+lastSlash; 
                   console.log(" last slash "+lastSlash);
                   $(this).children('div').find('h1').html(main_title);
                                  $(this).children('div').find('img').attr('src',main_img);
     
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
			
			  $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
        </script>
</body>

</html>
