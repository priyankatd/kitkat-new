<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/product-detail.css">
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>
    <![endif]-->

</head>

<body id="home">
    <!-- header section including main navigation -->
    <header class="kitkat-header">
        <?php
            include("../../config.php");
            include("../../header.php");
        ?>
    </header>

    <main class="clearfix page-content">
     <!-- Kitkat Range start-->
    <div class="kitkat-description-wrapper">
        <div class="red-bg-bottom"></div>
       <div id="kitkat-description" class="clearfix">
            <div class="left-panel">
                <div class="pr-banner-arrows">
                  <div class="pd-left-arrow">
                    <a href="javascript:void(0);"><img src="../../assets/images/product-range/product-detail/pd-left-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-up-arrow">
                    <a href="javascript:void(0);"><img src="../../assets/images/product-range/product-detail/pd-up-arrow.png" alt="" /></a>
                  </div>
                  <div class="pd-down-arrow">
                    <a href="javascript:void(0);"><img src="../../assets/images/product-range/product-detail/pd-down-arrow.png" alt="" /></a>
                  </div>
                </div> <!--pr-banner-arrows-->
            <div class="clearfix main-container">    
				<div class="main-product">
                    <div>
                        <h1 class="main-title text-center">PRODUCT TITLE 1</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../assets/images/product-range/product-detail/2fingers/2-finger-milk-box.png">
                    </div>
                </div>
				
				<!-- for mobile -->
				<div class="main-product two">
                    <div>
                        <h1 class="main-title text-center">PRODUCT TITLE 2</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../assets/images/product-range/product-detail/2fingers/2-finger-milkbox.png">
                    </div>
                </div>
				
				<div class="main-product three">
                    <div>
                        <h1 class="main-title text-center">PRODUCT TITLE 3</h1>
                    </div>
                    <div class="product-image text-center">
                        <img src="../../assets/images/product-range/product-detail/2fingers/2fingers-item-1.png">
                    </div>
                </div>
				
				<!-- for mobile -->
				
			</div>		
                <div class="thumbnail-product" id="thumb-1">
                    <div>
                        <img src="../../assets/images/product-range/product-detail/2fingers/thumbnails/2-finger-milk-box.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">PRODUCT TITLE 2</h1>
                    </div>
                </div>
                <div class="thumbnail-product" id="thumb-2">
                    <div>
                        <img src="../../assets/images/product-range/product-detail/2fingers/thumbnails/2fingers-item-1.png">
                    </div>
                    <div>
                        <h1 class="thumbnail-title">PRODUCT TITLE 3</h1>
                    </div>
                </div>
			
                <div class="pd-dots hidden-lg hidden-md hidden-sm">
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                  <span class="pd-dot-inner"></span>
                </div>


            </div>
            <div class="right-panel">
                <div class="right-panel-triangle">
                  <img src="../../assets/images/product-range/product-detail/right-panel-triangle.png" alt="" />
                </div>
                <div class="right-panel-content">
                  <h3 class="product-name">KITKAT 2 FINGER MILK CHOCOLATE BAR - 20.5G</h3>
                  <p class="product-text">NESTLÉ KITKAT 2 Finger is the perfect snack for any break time. It can be enjoyed with family and friends at home, or with colleagues during the day. It’s the ideal snack to enjoy as part of a balanced diet with only 105k calories per portion (1 portion = 1 individually wrapped bar). Just unwrap, break off a finger, snap it in two and savour the great tasting crispy wafer fingers covered with smooth milk chocolate.</p>
                  <hr>
                  <div class="buttons">
                      <a href="#"><img src="../../assets/images/product-range/buynow.png"></a>
                      <a href="" ><img src="../../assets/images/product-range/product-detail/nutritional-info.png"></a>
                  </div>
                </div>
                <div class="right-panel-img">
                  <img src="../../assets/images/product-range/product-detail/2fingers/2-finger-inner.png" class="img-responsive">
                </div>
            </div>
       </div>
    </div>
     <!-- Kitkat Range ends-->

    </main>
    <?php include("../../footer.php"); ?>
        <!--<script src="../../assets/js/custom.js" type="application/javascript"></script>-->
        <script type="text/javascript" src="../../assets/js/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
				if($(window).width() < 768 ){
					
					$('.main-container').slick({
                    infinite: false,
                    nav: false,dots:true,
                    easing: 'linear',
                    draggable: true,
					slidesToShow: 1

                  });
				}
				
				
              
                $('#kitkat-description').css('margin-top',$('.header-wrap').height() - 12);
                $('.thumbnail-product').click(function(){
                    //console.log(this.id);
                    //var id=this.id;
                    var thumb_title = $(this).children('div').find('h1').html();
                    var thumb_img = $(this).children('div').find('img').attr('src');
                    thumb_img = thumb_img.replace("thumbnails/", "");
                    console.log("thumb src "+thumb_img);

                    var main_title = $('.main-product').children('div').find('h1').html();
                    var main_img = $('.main-product').children('div').find('img').attr('src');

                    $(this).children('div').find('h1').html(main_title);
                    $(this).children('div').find('img').attr('src',main_img);
                    $('.main-product').children('div').find('h1').html(thumb_title);
                    $('.main-product').children('div').find('img').attr('src',thumb_img);
                })
            });
			
			  $(window).resize(function(){
                  $('#kitkat-description').css('margin-top',$('.header-wrap').height() -12 );
                });
        </script>
</body>

</html>
