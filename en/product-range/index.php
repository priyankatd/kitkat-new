<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Products - KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="../assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/products.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/ilightbox.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom-new.css">
	<style>.slick-next{width:63px;height:63px;right:50px;background:url('../assets/images/r-arrow.png') no-repeat;}.slick-prev{width:63px;height:63px;left:50px;background:url('../assets/images/l-arrow.png') no-repeat;    z-index: 9;}.slick-prev:before,.slick-next:before{content:''}body{overflow-x:hidden;}</style>
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui-1.10.4.js"></script>
    <script src="../assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="../assets/js/ie8.js"></script>

    <![endif]-->
<!--     <script>
        if (top != self) {
            top.location = self.location;
        }
        if (top.location != self.location) {
            top.location = self.location.href;
        }
        (function (window) {
            if (window.location !== window.top.location) {
                window.top.location = window.location;
            }
        })(this);
    </script> -->

</head>
<body id="products">
    <header class="kitkat-header">
        <?php 
            include("../config.php");
            include("../header.php");        
        ?>
    </header>
        <main class="clearfix page-content">
            <div class="products">
                <div class="container-fluid row">
                    <div class="product-wrap">
                        <div class="text-center">
                            <h1>KITKAT RANGE</h1>
                        </div>
                        <!-- <div class="left-custom-nav pull-left"></div>  -->
                        <div class="product-list">
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
										<img src="../assets/images/product-range/kitkat-mini-moments.png">
                                        <img src="../assets/images/product-range/inner/kitkat-mini-moments.png">
                                       
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                        <h4>KITKAT<br>MINI MOMENTS</h4>
                                            <a href="#" class="p-buynow buy-mini" data-buynowid="5096"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/min-moments" class="p-moreinfo info-mini"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center buy-mini">5 Unique Flavours in One Special Pouch.</p>
                                        </div>
                                    </div>
                                    <div class="tear-container tear-large"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
									<img src="../assets/images/product-range/kitkat-senses.png" >
                                    <img src="../assets/images/product-range/inner/kitkat-senses.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>SENSES HAZELNUT TWO FINGER</h4>
                                            <a href="#" class="p-buynow" data-buynowid="5097"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/kitkat-sense-hazelnut" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">Crispy wafer fingers with a layer of hazelnut pieces covered in milk chocolate.</p>
                                        </div>
                                    </div>
									<div class="tear-container tear-medium"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
										<img src="../assets/images/product-range/kitkat-2-fingers.png">
                                        <img src="../assets/images/product-range/inner/kitkat-2-finger.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>TWO FINGER</h4>
                                            <a href="#" class="p-buynow" data-buynowid="4506"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/two-finger" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">A delicious chocolate bar made of 2 fingers of crispy wafer covered with smooth milk chocolate.</p>
                                        </div>
                                    </div>   
										 <div class="tear-container tear-medium"></div>	
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">
										<img src="../assets/images/product-range/kitkat-4-fingers.png">
                                        <img src="../assets/images/product-range/inner/kitkat-4-finger.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>FOUR FINGER</h4>
                                            <a href="#" class="p-buynow" data-buynowid="4509"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/four-finger" class="p-moreinfo"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center">A delicious  chocolate bar, made of 4 fingers of crispy wafer covered with smooth milk chocolate.</p>
                                        </div>
                                    </div>  
										 <div class="tear-container tear-medium"></div>	
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">      
										<img src="../assets/images/product-range/kitakt-4-finger-dark-chocolate.png">      <img src="../assets/images/product-range/inner/kitkat-4-finger-dark-chocolate.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>FOUR FINGER DARK CHOCOLATE</h4>
                                            <a href="#" class="p-buynow buy-dark" data-buynowid="4508"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/four-finger-dark" class="p-moreinfo info-dark"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center buy-dark">A delicious  chocolate bar, made of 4 fingers of crispy wafer covered with dark chocolate.</p>
                                        </div>
                                    </div>
									 <div class="tear-container tear-medium"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical"> 
										<img src="../assets/images/product-range/chunky-mini.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-mini.png">   
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>CHUNKY MINI</h4>
                                            <a href="#" class="p-buynow buy-mini-chunky" data-buynowid="4514"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-mini" class="p-moreinfo info-mini-chunky"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center buy-mini-chunky ">A delicious chocolate bar, comprising a crispy wafer coated in smooth milk chocolate.</p>
                                        </div>
                                    </div>
									 <div class="tear-container tear-small"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">  
										<img src="../assets/images/product-range/kitkat-chunky.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky.png">             
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>CHUNKY</h4>
                                            <a href="#" class="p-buynow buy-mini" data-buynowid="4510"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky" class="p-moreinfo info-mini"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center kitkat-chunky">A delicious chocolate bar, comprising a crispy wafer coated in smooth milk chocolate.</p>
                                        </div>
                                    </div>
									 <div class="tear-container"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">  
										<img src="../assets/images/product-range/kitkat-chumky-mini-caramel.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-mini-caramel.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>CHUNKY MINI CARAMEL</h4>
                                            <a href="#" class="p-buynow buy-mini-chunky" data-buynowid="1279"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chuny-mini-caramel" class="p-moreinfo info-mini-chunky"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center info-mini-chunky">A delicious chocolate bar made of 2 fingers of crispy wafer covered with smooth milk chocolate.</p>
                                        </div>
                                    </div>
									 <div class="tear-container tear-small"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">   
										<img src="../assets/images/product-range/kitkat-chunky-caramel.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-caramel.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>CHUNKY CARAMEL</h4>
                                            <a href="#" class="p-buynow buy-mini" data-buynowid="4511"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-caramel" class="p-moreinfo info-mini"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center buy-mini">A delicious chocolate bar made of 2 fingers of crispy wafer covered with smooth milk chocolate.</p>
                                        </div>
                                    </div>  
										 <div class="tear-container"></div>
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" data-orientation="vertical">    
										<img src="../assets/images/product-range/kitkat-chunky-peanut-butter.png">
                                        <img src="../assets/images/product-range/inner/kitkat-chunky-caramel.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>CHUNKY PEANUT BUTTER</h4>
                                            <a href="#" class="p-buynow buy-mini" data-buynowid=""><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/chunky-peanut-butter" class="p-moreinfo info-mini"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center kitkat-chunky">A delicious chocolate bar, comprising a crispy wafer covered with a tasty layer of caramel and milk chocolate.</p>
                                        </div>
                                    </div>
										 <div class="tear-container"></div>	
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>    
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture">                                        
                                        <img src="../assets/images/product-range/kitkat-pop-choc-mini.png">
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>POP CHOC MILK BITES<br>36G</h4>
                                            <a href="#" class="p-buynow buy-pop" data-buynowid="1216"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/pop-choc-milk-chocolate-bites" class="p-moreinfo info-pop"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center info-pop">Milk Chocolate Bites Pouch contains small bite-sized pieces of wafer, making it the ideal treat for sharing a break with friends and family while watching a movie or during a break at home.</p>
                                        </div>
                                    </div>
									 <!--<div class="tear-container"></div>-->
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                            <div class="item">
                                <div class="product-slider-inner">
                                    <div class="text-center product-picture" >                                        
                                        <img src="../assets/images/product-range/kitkat-pop-choc-140.png">   
                                    </div>
                                    <div class="text-center kitkat-ranges">
                                        <div class="kitkat-ranges-content">
                                            <h4>KITKAT<br>POP CHOC MILK BITES<br>140G</h4>
                                            <a href="#" class="p-buynow buy-pop" data-buynowid="1217"><img src="../assets/images/product-range/buynow.png" class="img-responsive"></a>
                                            <a href="product-detail/pop-choc-milk-chocolate-bites" class="p-moreinfo info-pop"><img src="../assets/images/product-range/moreinfo.png"></a>
                                            <p class="text-center info-pop">Milk Chocolate Bites Pouch contains small bite-sized pieces of wafer, making it the ideal treat for sharing a break with friends and family while watching a movie or during a break at home.</p>
                                        </div>
                                    </div>
										<!-- <div class="tear-container"></div>-->
                                </div>
                                <div class="kr-inner-bottom-wrapper"></div>
                            </div>
                        </div> <!-- product list -->
                        <!-- <div class="right-custom-nav pull-right"></div>  -->
                        <div class="product-r-gradient"></div>
                    </div>
                </div>
           </div>
    </main>
     <?php include("../footer.php"); ?>
        <!--<script src="../assets/js/custom.js" type="text/javascript"></script>-->
        <script src="../assets/js/jquery.event.move.js" type="application/javascript"></script>
        <script src="../assets/js/jquery.twentytwenty.js" type="application/javascript"></script>
		<script src="../assets/js/ilightbox.min.js" type="application/javascript"></script>
        <script src="../assets/js/slick.min.js" type="text/javascript"></script>
        <script src="../assets/js/product-detail.js" type="text/javascript"></script>
        <script src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js" type="text/javascript"></script>
        <script>
			$(document).ready(function () {
				
				$('.products').css('margin-top',$('.header-wrap').height() - 30);
					
					function contactUs() {$('#iframe_1').iLightBox();}
					
					
			
				/*$('.tear-container').bind("touchstart", function(e){
					e.preventDefault();
					console.log("prevent swipe");
				});

				$('.tear-container').bind("touchmove", function(e){
					e.preventDefault();
					console.log("prevent swipe 1");
				});*/
				
            });
            $(window).resize(function(){
                $('.products').css('margin-top',$('.header-wrap').height() - 30);
            });
            $('.product-list').slick({
                    variableWidth: true,
                    infinite: false,
                    nav: false,
                    centerMode: false,
                    margin:0,
                    easing: 'linear',
                    draggable: true,
                    slidesToScroll: 5,
                    rows: 1,
                    /*,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    margin: 0*/
                   responsive: [
                        {
                          breakpoint: 1700,
                          settings: {
                            /*slidesToShow: 3,*/
                            slidesToScroll: 3,
                            margin: 0
                          }
                        },
                        {
                          breakpoint: 1390,
                          settings: {
                            /*slidesToShow: 3,*/
                            slidesToScroll: 3
                          }
                        },
                        {
                          breakpoint: 1070,
                          settings: {
                            /*slidesToShow: 2,*/
                            slidesToScroll: 2
                          }
                        },
                        {
                          breakpoint: 750,
                          settings: {
                           /* slidesToShow: 1,*/
                            slidesToScroll: 1
                          }
                        },
                        {
                          breakpoint: 430,
                          settings: {
                            /*slidesToShow: 1,*/
                            slidesToScroll: 1
                          }
                        }
                    ]
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                     
					 // nextArrow: '<img src="assets/images/l-arrow.png">',
					 // prevArrow: '<img src="assets/images/r-arrow.png">',
                  });
            // $('.right-arrow').click(function(){
                   // $('.product-list').slick('slickNext');
                // });
                // $('.left-arrow').click(function(){
                     // $('.product-list').slick('slickPrev');
                // });
        </script>
        <script type="text/javascript">
            $(window).load(function () {
                $(".product-picture[data-orientation='vertical']").twentytwenty({
                    default_offset_pct:1, orientation: 'vertical'
                });
            });
        </script>
</body>

</html>