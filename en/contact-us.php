<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KITKAT Arabia&reg;</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="assets/images/cropped-logo-icon-32x32.jpg" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/images/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive-tab.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.custom.css">

    <link rel="stylesheet" type="text/css" href="assets/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/common.css?v1.0">
    <link rel="stylesheet" type="text/css" href="assets/css/contact-us.css?v1.1">
	<style>body{background: transparent;}</style>
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/jquery-ui-1.10.4.js"></script>
    <script src="assets/js/modernizr.custom.min.js"></script>
    <!-- <meta http-equiv="X-Frame-Options" content="SAMEORIGIN"> -->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie8.css">
    <script src="assets/js/ie8.js"></script>

    <![endif]-->
</head>

    <body id="">

        <section class="contact-us">
            <div id="responsiveTabsDemo" class="left-panel">
                <ul class="tab-heading">
                    <li><a href="#tab-1">EMAIL US</a></li>
                    <li><a href="#tab-2">CALL US</a></li>
                    <!--<li><a href="#tab-3">CHAT WITH US</a></li>-->
                    <li><a href="#tab-4">FAQ</a></li>
                </ul>

                <div id="tab-1" class="item">
                    <div class="left-content" >
                        <p><span>Great to see you here!</span> Looks like you have something to share with us.Please choose an option below to reach us and we would be more than happy to help!</p>
                        <div class="email-section">
                            <h3>WHAT BRINGS YOU HERE?</h3>
                            <div class="comment">
                                <img src="assets/images/contact-assets/comment.png">
                            </div>
                            <div class="advice">
                                <img src="assets/images/contact-assets/advice.png">
                            </div>
                            <div class="question">
                                <img src="assets/images/contact-assets/question.png">
                            </div>
                        </div>
                    </div>
                    <div class="left-content-comment-container" style="display: none;">
                        <div class="">
                                        <p>Fill in your personal information and indicate your comment, advice needed or question and we will reply with the shortest possible time.</p>
                                        <div class="popup-tabs">
                                            <div class="form-closer">
                                                <img src="assets/images/close-red.png">
                                            </div>
                                            <h3>SHARE US YOUR COMMENT</h3>
                                            <div class="form-area">
                                                <form class="contact-us-form2" method="post">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="First name"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Second name"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Email"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Mobile"></input>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 ">
                                                            <select class="form-area-select">
                                                                <option class="form-area-option" selected>Choose Country</option>
                                                                <option value="uae">United Arab Emirates</option>
                                                                <option value="bahrain">Bahrain</option>
                                                                <option value="kuwait">Kuwait</option>
                                                                <option value="oman">Oman</option>
                                                                <option value="saudi">Saudi Arabia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="message-row-1 col-md-7">
                                                            <textarea placeholder="Your message here"></textarea>
                                                        </div>

                                                        <div class="col-md-5">
                                                         <p>HOW SHALL WE GET IN TOUCH WITH YOU?</p>
                                                            <div class="email-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>Email</p>
                                                            </div>
                                                            <div class="mobile-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>Mobile</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 attach-file-box">
                                                            <img src="assets/images/contact-assets/attach-file.png"><span class="attach-file">Attach File (2 Mb limit)</span>
                                                        </div>
                                                        <div class="offers col-md-12">
                                                            <div class="offer-statement">
                                                                <input class="checkboxes" type="checkbox" name="offers"></input>
                                                                <p> I would like to receive offers, and be contacted by, or on behalf of Nestlé, about Nestlé, its brands, special offers,consumer research and promotions.</p>
                                                            </div>
                                                        </div>
                                                        <div class="message-row-2 col-md-12">
                                                           <div class="privacy-statement">
                                                                <input class="checkboxes" type="checkbox" name="privacy-check"></input><p> I accept the use and privacy of my data, more information on <a href="#" class="privacy-detailed">privacy policies *</a></p>

                                                            </div>
                                                            <button class="contact-us-send" type="sbumit" name="submit" value="SEND">SEND</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                    <div class="left-content-advice-container" style="display: none;">
                                                <div class="">
                                        <p>Fill in your personal information and indicate your comment, advice needed or question and we will reply with the shortest possible time.</p>
                                        <div class="popup-tabs">
                                            <div class="form-closer">
                                                <img src="assets/images/close-red.png">
                                            </div>
                                            <h3>WHAT ADVICE WOULD YOU NEED FROM US?</h3>
                                            <div class="form-area">
                                                <form class="contact-us-form2" method="post">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="First name"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Second name"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Email"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Mobile"></input>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 ">
                                                            <select class="form-area-select">
                                                                <option class="form-area-option" selected>Choose Country</option>
                                                                <option value="uae">United Arab Emirates</option>
                                                                <option value="bahrain">Bahrain</option>
                                                                <option value="kuwait">Kuwait</option>
                                                                <option value="oman">Oman</option>
                                                                <option value="saudi">Saudi Arabia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="message-row-1 col-md-7">
                                                            <textarea placeholder="Your message here"></textarea>
                                                        </div>
                                                        <div class="col-md-5">
                                                         <p>HOW SHALL WE GET IN TOUCH WITH YOU?</p>
                                                            <div class="email-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>Email</p>
                                                            </div>
                                                            <div class="mobile-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>Mobile</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 attach-file-box">
                                                            <img src="assets/images/contact-assets/attach-file.png"><span class="attach-file">Attach File (2 Mb limit)</span>
                                                        </div>
                                                        <div class="offers col-md-12">
                                                            <div class="offer-statement">
                                                                <input class="checkboxes" type="checkbox" name="offers"></input>
                                                                <p> I would like to receive offers, and be contacted by, or on behalf of Nestlé, about Nestlé, its brands, special offers,consumer research and promotions.</p>
                                                            </div>
                                                        </div>
                                                        <div class="message-row-2 col-md-12">
                                                           <div class="privacy-statement">
                                                                <input class="checkboxes" type="checkbox" name="privacy-check"></input><p> I accept the use and privacy of my data, more information on <a href="#" class="privacy-detailed">privacy policies *</a></p>

                                                            </div>
                                                            <button class="contact-us-send" type="sbumit" name="submit" value="SEND">SEND</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                    <div class="left-content-question-container" style="display: none;">
                                                <div class="">
                                        <p>Fill in your personal information and indicate your comment, advice needed or question and we will reply with the shortest possible time.</p>
                                        <div class="popup-tabs">
                                            <div class="form-closer">
                                                <img src="assets/images/close-red.png">
                                            </div>
                                            <h3>TELL US ABOUT YOUR QUERY</h3>
                                            <div class="form-area">
                                                <form class="contact-us-form2" method="post">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="First name"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Second name"></input>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Email"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input name="" type="text" placeholder="Mobile"></input>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 ">
                                                            <select class="form-area-select">
                                                                <option class="form-area-option" selected>Choose Country</option>
                                                                <option value="uae">United Arab Emirates</option>
                                                                <option value="bahrain">Bahrain</option>
                                                                <option value="kuwait">Kuwait</option>
                                                                <option value="oman">Oman</option>
                                                                <option value="saudi">Saudi Arabia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="message-row-1 col-md-7">
                                                            <textarea placeholder="Your message here"></textarea>
                                                        </div>
                                                        
                                                        <div class="col-md-5">
                                                         <p>HOW SHALL WE GET IN TOUCH WITH YOU?</p>
                                                            <div class="email-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>Email</p>
                                                            </div>
                                                            <div class="mobile-statement">
                                                                <input class="checkboxes" type="checkbox" name=""></input>
                                                                <p>Mobile</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 attach-file-box">
                                                            <img src="assets/images/contact-assets/attach-file.png"><span class="attach-file">Attach File (2 Mb limit)</span>
                                                        </div>
                                                        <div class="offers col-md-12">
                                                            <div class="offer-statement">
                                                                <input class="checkboxes" type="checkbox" name="offers"></input>
                                                                <p> I would like to receive offers, and be contacted by, or on behalf of Nestlé, about Nestlé, its brands, special offers,consumer research and promotions.</p>
                                                            </div>
                                                        </div>
                                                        <div class="message-row-2 col-md-12">
                                                           <div class="privacy-statement">
                                                                <input class="checkboxes" type="checkbox" name="privacy-check"></input><p> I accept the use and privacy of my data, more information on <a href="#" class="privacy-detailed">privacy policies *</a></p>

                                                            </div>
                                                            <button class="contact-us-send" type="sbumit" name="submit" value="SEND">SEND</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
                <div id="tab-2" class="item">
                    <div class="tabs-common-inner-section">
                        <h3>NESTLE CARE CENTER NUMBER FOR ALL COUNTRIES</h3>
                        <div class="">
                            <ul class="contact-countries-list">
                                <li class="call-us-item">
                                    <p>KSA</p>
                                    <img src="assets/images/contact-us/ksa.png">
                                    <p class="calling-number">8008971971</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>UAE</p>
                                    <img src="assets/images/contact-us/uae.png">
                                    <p class="calling-number">8008971971</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>BAHRAIN</p>
                                    <img src="assets/images/contact-us/bahrain.png">
                                    <p class="calling-number">+97444587688</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>KUWAIT</p>
                                    <img src="assets/images/contact-us/kuwait.png">
                                    <p class="calling-number">+96522286847</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>OMAN</p>
                                    <img src="assets/images/contact-us/oman.png">
                                    <p class="calling-number">+96822033446</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>JORDAN</p>
                                    <img src="assets/images/contact-us/jordan.png">
                                    <p class="calling-number">+96265902998</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>LEBANON</p>
                                    <img src="assets/images/contact-us/lebanon.png">
                                    <p class="calling-number">+9614548595</p>
                                    <span>TOLL FREE</span>
                                </li>
                                <li class="call-us-item">
                                    <p>QATAR</p>
                                    <img src="assets/images/contact-us/qatar.png">
                                    <p class="calling-number">+97444587688</p>
                                    <span>TOLL FREE</span>
                                </li>
                            </ul>
                            <h3 class="text-center">OTHER COUNTRIES</h3>
                            <p class="text-center"><strong>0097148100000</strong></p>
                        </div>
                    </div>
                </div>
                <!--<div id="tab-3" class="item">
                    <div class="tabs-common-inner-section">
                        <h3>CHAT SUPPORT</h3>
                        <p>Start a chat session with Nestlé Chat Support now.</p>
                        <div class="form-area row">
                            <form class="contact-us-form1" method="post">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input name="" type="text" placeholder="First name"></input>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input name="" type="text" placeholder="Second name"></input>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input name="" type="text" placeholder="Email"></input>
                                </div>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <textarea placeholder="Your message here"></textarea>
                                </div>
                                <div class="col-md-5 col-sm-6  col-xs-12">
                                    <div class="chat-now-container">
                                        <div class="chat-now text-center">
                                            <img src="assets/images/chat.png" >
                                            <h4>CHAT NOW</h4>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->
                <div id="tab-4" class="item">
                    <div class="tabs-common-inner-section">
                        <h3>LOOKING FOR MORE?</h3>
                        <p>Here you'll find answers to our most frequently asked questions.</p>
                        <div class="faq-inner">
                            <div class="accordions">
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="accordion">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </h3>
                                    <div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                    </div>
                                </div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                <div class="">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-panel">
                <div class="right-panel-content">
                <div class="contact-us-popup-inner-right">
                    <div class="new-letter">
                        <input type="text" name="user" placeholder="SUBSCRIBE TO OUR NEWSLETTER"></input>
                        <button type="submit"><i class="fa fa-caret-right"></i></button>
                    </div>
                    <!--<div class="help-line">
                        <h3>HELP LINE</h3>
                        <img src="assets/images/contact-assets/call-icon.gif" alt="">
                        <p>Consumer Service representatives are available to help you directly,
                        <br>Monday-Friday from 9 a.m to 6 p.m.</p>
                        <h3 class="helpline-number">1 800 123-4567</h3>
                    </div>-->
                    <div class="address-box">
                        <p class="company-name">Nestlé Middle East FZE <br>
                        P.O. Box: 17327<br>
                        3rd Floor - Dubai World Central HQ Building<br>
                        Near New Al Maktoum International Airport<br>
                        Dubai Logistic City, Jebel Ali - Dubai</p>
                    </div>
                    <div class="social">
                        <h4>FOLLOW US</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/kitkat.arabia" target="_blank"><img src="assets/images/contact-assets/facebook.png"></a></li>
                            <li><a href="https://twitter.com/kitkatarabia" target="_blank"><img src="assets/images/contact-assets/twitter.png"></a></li>
                            <li><a href="https://www.youtube.com/watch?v=of4JWg_hf9s"><img src="assets/images/contact-assets/youtube.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </section>
        <script src="assets/js/jquery.responsiveTabs.js"></script>
        <script>
            $('#responsiveTabsDemo').responsiveTabs({
                startCollapsed: 'accordion'
            });
            $(function() {
                $( ".accordions" ).accordion();
                $(".accordion").accordion({ header: "h3", collapsible: true, active: false });
              });
             $('.comment').click(function(){
                $('.left-content-comment-container').fadeIn(500);
                $('.left-content').fadeOut();
            });
             $('.advice').click(function(){
                $('.left-content-advice-container').fadeIn(500);
                $('.left-content').fadeOut();
            });
             $('.question').click(function(){
                $('.left-content-question-container').fadeIn(500);
                $('.left-content').fadeOut();
            });
            $('.form-closer').click(function(){
                $('.left-content-comment-container').fadeOut();
                $('.left-content').fadeIn(500);
            });
            $('.form-closer').click(function(){
                $('.left-content-question-container').fadeOut();
                $('.left-content').fadeIn(500);
            });
            $('.form-closer').click(function(){
                $('.left-content-advice-container').fadeOut();
                $('.left-content').fadeIn(500);
            });
        </script>
    </body>
</html>