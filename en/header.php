<div class="header-wrap">
    <div class="nav-wrap">
        <div class="container">
            <div class="clearfix">
                <nav class="main-nav">
                    <div class="menu-left">
                        <ul class="left">
                            <li class="hidden-xs">
                                <a href="<?php echo $root_languge; ?>/cocoa-plan/"><img class="img-responsive" src="<?php echo $root_languge; ?>/assets/images/cocoa-logo.png" alt="cocoa"></img>
                                </a>
                            </li>
                            <li class="language">
                                <a href="<?php echo $root; ?>/ar">عربي</a>
                            </li>
                        </ul>
                    </div>
                    <div class="menu-center">
                        <ul class="center">
                            <li>
                                <a href="<?php echo $root_languge; ?>/"><img class="img-responsive shake" src="<?php echo $root_languge; ?>/assets/images/kitkat-main-logo.png" alt="KITKAT"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="menu-right">
                        <ul class="right">
                            <li>
                                <a href="<?php echo $root_languge; ?>/product-range/" class="buy-trolley"><img src="<?php echo $root_languge; ?>/assets/images/trolley.png" width="40"></a>
                            </li>
                            <li><a href="https://www.facebook.com/kitkat.arabia" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
                            <li><a href="https://twitter.com/kitkatarabia" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/user/KitKatArabia" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href="https://www.instagram.com/kitkatarabia/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="http://breakerpage.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a></li>
                            <li>
                                <a href="javascript:void(0)" class="menu-trigger"><img src="<?php echo $root_languge; ?>/assets/images/menu-icon.png" class="img-responsive" alt="menu"></a>
                            </li>
                        </ul>
                    </div>
                    <a href="<?php echo $root_languge; ?>/contact-us.php"  onclick="contactUs()" id="iframe_1" class="contact-button"><img src="<?php echo $root_languge; ?>/assets/images/contacts-icon.png" class="img-responsive" alt="Contact Us"></a>
                </nav>
            </div>
        </div>
    </div>
    <div class="menu-wrap">
        <div class="container">
            <div class="row">
                <nav class="main-menu">
                    <ul class="main-menu-innner">
                        <li class="hidden-lg hidden-md hidden-sm">
                            <a href="<?php echo $root_languge; ?>/cocoa-plan/"><img class="img-responsive" src="<?php echo $root_languge; ?>/assets/images/cocoa-logo.png" alt="cocoa"></img>
                            </a>
                            <!-- <a href="javascript:void(0);"><img class="img-responsive" src="assets/images/trolley_logo.png" alt="trolley"></img></a> --></li>
                        <li><a href="<?php echo $root_languge; ?>/">HOME</a></li>
                        <li><a href="<?php echo $root_languge; ?>/product-range/">PRODUCT RANGE</a></li>
                        <li><a class="push-down" href="<?php echo $root_languge; ?>/#journey">JOURNEY</a></li>
                        <!-- <li><a href="<?php echo $root_languge; ?>/contact-us.php"  onclick="contactUs()" id="iframe_1" >CONTACT US</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>